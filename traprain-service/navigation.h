/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_SERVICE_NAVIGATION_H__
#define __TRAPRAIN_SERVICE_NAVIGATION_H__

#include <gio/gio.h>
#include <glib-object.h>

#include "traprain-common/route.h"

G_BEGIN_DECLS

#define TRP_SERVICE_TYPE_NAVIGATION (trp_service_navigation_get_type ())
G_DECLARE_FINAL_TYPE (TrpServiceNavigation, trp_service_navigation, TRP_SERVICE, NAVIGATION, GObject)

TrpServiceNavigation *trp_service_navigation_new (GDBusConnection *connection);

TrpRoute *trp_service_navigation_create_route (TrpServiceNavigation *self,
                                               guint total_distance,
                                               guint total_time);

gint trp_service_navigation_add_route (TrpServiceNavigation *self,
                                       TrpRoute *route,
                                       gint index,
                                       GError **error);
gboolean trp_service_navigation_remove_route (TrpServiceNavigation *self,
                                              guint index,
                                              GError **error);
gboolean trp_service_navigation_set_routes (TrpServiceNavigation *self,
                                            GPtrArray *routes,
                                            GError **error);

gboolean trp_service_navigation_set_current_route (TrpServiceNavigation *self,
                                                   gint index,
                                                   GError **error);

GDBusConnection *trp_service_navigation_get_dbus_connection (TrpServiceNavigation *self);

guint trp_service_navigation_get_n_routes (TrpServiceNavigation *self);

G_END_DECLS

#endif /* __TRAPRAIN_SERVICE_NAVIGATION_H__ */
