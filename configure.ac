AC_PREREQ(2.65)

# Release version
m4_define([trp_version_major],[0])
m4_define([trp_version_minor],[2019pre])
m4_define([trp_version_micro],[0])

# API version
m4_define([trp_api_version],[0])

AC_INIT([traprain],[trp_version_major.trp_version_minor.trp_version_micro],
        [mailto:maintainers@lists.apertis.org],[traprain],
        [https://git.apertis.org/cgit/traprain.git/])

AX_CHECK_ENABLE_DEBUG
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_HEADERS([config.h])
AC_USE_SYSTEM_EXTENSIONS
AM_SILENT_RULES([yes])
AC_REQUIRE_AUX_FILE([tap-driver.sh])

AM_INIT_AUTOMAKE([1.11 -Wno-portability dist-xz no-dist-gzip check-news subdir-objects tar-ustar])

AC_PROG_CXX
AM_PROG_CC_C_O
LT_INIT
PKG_PROG_PKG_CONFIG
AC_PROG_LN_S

LT_LIB_M
AC_SUBST([LIBM])

# Before making a release, the TRP_LT_VERSION string should be modified. The
# string is of the form c:r:a. Follow these instructions sequentially:
#
#  1. If the library source code has changed at all since the last update, then
#     increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#  2. If any interfaces have been added, removed, or changed since the last
#     update, increment current, and set revision to 0.
#  3. If any interfaces have been added since the last public release, then
#     increment age.
#  4. If any interfaces have been removed or changed since the last public
#     release, then set age to 0.
AC_SUBST([TRP_LT_VERSION],[2:1:1])

AC_SUBST([TRP_VERSION_MAJOR],trp_version_major)
AC_SUBST([TRP_VERSION_MINOR],trp_version_minor)
AC_SUBST([TRP_VERSION_MICRO],trp_version_micro)
AC_SUBST([TRP_API_VERSION],trp_api_version)

# Dependencies
AX_PKG_CHECK_MODULES([TRAPRAIN],
                     [glib-2.0 >= 2.42.0 gio-2.0 gobject-2.0],
                     [gthread-2.0 gio-unix-2.0],
                     [],[],
                     [TRP_PACKAGE_REQUIRES],[TRP_PACKAGE_REQUIRES_PRIVATE])

AC_SUBST([TRP_COMMON_PACKAGE_REQUIRES],[$TRP_PACKAGE_REQUIRES])
AC_SUBST([TRP_COMMON_PACKAGE_REQUIRES_PRIVATE],[$TRP_PACKAGE_REQUIRES_PRIVATE])

AC_SUBST([TRP_SERVICE_PACKAGE_REQUIRES],["$TRP_PACKAGE_REQUIRES traprain-common-$TRP_API_VERSION"])
AC_SUBST([TRP_SERVICE_PACKAGE_REQUIRES_PRIVATE],[$TRP_PACKAGE_REQUIRES_PRIVATE])

AC_SUBST([TRP_CLIENT_PACKAGE_REQUIRES],["$TRP_PACKAGE_REQUIRES traprain-common-$TRP_API_VERSION"])
AC_SUBST([TRP_CLIENT_PACKAGE_REQUIRES_PRIVATE],[$TRP_PACKAGE_REQUIRES_PRIVATE])

AC_SUBST([TRP_GUIDANCE_PACKAGE_REQUIRES],[$TRP_PACKAGE_REQUIRES])
AC_SUBST([TRP_GUIDANCE_PACKAGE_REQUIRES_PRIVATE],[$TRP_PACKAGE_REQUIRES_PRIVATE])

AX_PKG_CHECK_MODULES([TRAPRAIN_MOCK_SERVICE],
                     [],
                     [libsystemd],
                     [], [],
                     [TRP_MOCK_SERVICE_PACKAGE_REQUIRES],[TRP_MOCK_SERVICE_PACKAGE_REQUIRES_PRIVATE])

AX_PKG_CHECK_MODULES([TRAPRAIN_GUIDANCE_UI],
                     [],
                     [libnotify libsystemd],
                     [], [],
                     [TRP_GUIDANCE_UI_PACKAGE_REQUIRES],[TRP_GUIDANCE_UI_PACKAGE_REQUIRES_PRIVATE])

# Code coverage
AX_CODE_COVERAGE

# General macros
AX_COMPILER_FLAGS
AX_VALGRIND_CHECK
AX_GENERATE_CHANGELOG

GOBJECT_INTROSPECTION_CHECK([0.9.7])

AC_ARG_VAR([GDBUS_CODEGEN], [The gdbus-codegen tool])
AC_PATH_PROG([GDBUS_CODEGEN], [gdbus-codegen], [])
AS_IF([test "x$GDBUS_CODEGEN" = "x"],
  [AC_MSG_ERROR([gdbus-codegen (part of GLib) is required])])

AC_ARG_VAR([GLIB_MKENUMS], [the glib-mkenums tool])
AC_PATH_PROG([GLIB_MKENUMS], [glib-mkenums])
AS_IF([test -z "$GLIB_MKENUMS"],
  [AC_MSG_ERROR([glib-mkenums not found])])

AC_PATH_PROG([GLIB_COMPILE_RESOURCES],[glib-compile-resources])

AC_SUBST([AM_CPPFLAGS])
AC_SUBST([AM_CFLAGS])
AC_SUBST([AM_CXXFLAGS])
AC_SUBST([AM_LDFLAGS])

# Documentation
HOTDOC_CHECK([0.8], [c])

# installed-tests
AC_ARG_ENABLE([always_build_tests],
              AS_HELP_STRING([--enable-always-build-tests],
                             [Enable always building tests (default: yes)]),,
              [enable_always_build_tests=yes])
AC_ARG_ENABLE([installed_tests],
              AS_HELP_STRING([--enable-installed-tests],
                             [Install test programs (default: no)]),,
              [enable_installed_tests=no])

AM_CONDITIONAL([ENABLE_ALWAYS_BUILD_TESTS],
               [test "$enable_always_build_tests" = "yes"])
AC_SUBST([ENABLE_ALWAYS_BUILD_TESTS],[$enable_always_build_tests])

AM_CONDITIONAL([ENABLE_INSTALLED_TESTS],
               [test "$enable_installed_tests" = "yes"])
AC_SUBST([ENABLE_INSTALLED_TESTS],[$enable_installed_tests])

# Mock service
AC_ARG_WITH([systemdsystemunitdir],
            AS_HELP_STRING([--with-systemdsystemunitdir=DIR],
                           [Directory for systemd service files]),
            [],[with_systemdsystemunitdir='${prefix}/lib/systemd/system'])
AC_SUBST([systemdsystemunitdir],[$with_systemdsystemunitdir])

AC_ARG_WITH([systemduserunitdir],
            AS_HELP_STRING([--with-systemduserunitdir=DIR],
                           [Directory for systemd user service files]),
            [],[with_systemduserunitdir='${prefix}/lib/systemd/user'])
AC_SUBST([systemduserunitdir],[$with_systemduserunitdir])

AC_ARG_WITH([systemdsysusersdir],
            AS_HELP_STRING([--with-systemdsysusersdir=DIR],
                           [Directory for systemd sysusers files]),
            [],[with_systemdsysusersdir='${prefix}/lib/sysusers.d'])
AC_SUBST([systemdsysusersdir],[$with_systemdsysusersdir])

AC_ARG_WITH([mock-user],
            [AS_HELP_STRING([--with-mock-user=<user>],
                            [User for running the mock Traprain service (default: traprain-mock)])],
            [],[with_mock_user=traprain-mock])

AC_SUBST([MOCK_USER],[$with_mock_user])
AC_DEFINE_UNQUOTED([MOCK_USER],"$with_mock_user",
                   [User for running the Traprain mock service])

AC_CONFIG_FILES([
Makefile
traprain-common/libtraprain-common-$TRP_API_VERSION.pc:traprain-common/libtraprain-common.pc.in
traprain-service/libtraprain-service-$TRP_API_VERSION.pc:traprain-service/libtraprain-service.pc.in
traprain-client/libtraprain-client-$TRP_API_VERSION.pc:traprain-client/libtraprain-client.pc.in
traprain-guidance/libtraprain-guidance-$TRP_API_VERSION.pc:traprain-guidance/libtraprain-guidance.pc.in
tests/services/org.apertis.Navigation1.service
tests/services/org.apertis.Traprain1.Mock.service
tests/services/org.apertis.NavigationGuidance1.Progress.service
],[],
[TRP_API_VERSION='$TRP_API_VERSION'])
AC_OUTPUT
