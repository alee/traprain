/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "navigation-guidance-progress.h"

#include "dbus/org.apertis.NavigationGuidance1.Progress.h"

#define PROGRESS_PATH "/org/apertis/NavigationGuidance1/Progress"
#define PROGRESS_BUS_NAME "org.apertis.NavigationGuidance1.Progress"

/**
 * SECTION: traprain-client/navigation-guidance-progress.h
 * @title: TrpClientNavigationGuidanceProgress
 * @short_description: Retrieve information about journey progress
 * published by the navigation system.
 *
 * #TrpClientNavigationGuidanceProgress is used to retrieve progress information
 * about the current journey published by the navigation system.
 *
 * Once you have created a #TrpClientNavigationGuidanceProgress, g_async_initable_init_async()
 * should be called so it can start fetching data from the navigation service.
 *
 * Use trp_client_navigation_guidance_progress_get_start_time() and
 * trp_client_navigation_guidance_progress_get_estimated_end_time()
 * to retrieve the information. Changes are notified using the
 * #GObject::notify signal.
 *
 * See also #TrpClientNavigation to retrieve information about the navigation routes.
 *
 * Since: 0.1612.0
 */

/**
 * TrpClientNavigationGuidanceProgress:
 *
 * #TrpClientNavigationGuidanceProgress is an object to retrieve published information regarding the
 * current journey from the navigation system.
 *
 * Since: 0.1612.0
 */

struct _TrpClientNavigationGuidanceProgress
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  TrpNavigationGuidance1Progress *proxy;
  GList /*<owned GTask>*/ *init_tasks; /* owned */
  GCancellable *cancellable;           /* owned */

  GDateTime *start_time;         /* owned */
  GDateTime *estimated_end_time; /* owned */
};

typedef enum {
  PROP_CONNECTION = 1,
  PROP_START_TIME,
  PROP_ESTIMATED_END_TIME,
  /*< private >*/
  PROP_LAST = PROP_ESTIMATED_END_TIME
} TrpClientNavigationGuidanceProgressProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (TrpClientNavigationGuidanceProgress, trp_client_navigation_guidance_progress, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init))

static void
trp_client_navigation_guidance_progress_get_property (GObject *object,
                                                      guint prop_id,
                                                      GValue *value,
                                                      GParamSpec *pspec)
{
  TrpClientNavigationGuidanceProgress *self = TRP_CLIENT_NAVIGATION_GUIDANCE_PROGRESS (object);

  switch ((TrpClientNavigationGuidanceProgressProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    case PROP_START_TIME:
      g_value_set_boxed (value, self->start_time);
      break;
    case PROP_ESTIMATED_END_TIME:
      g_value_set_boxed (value, self->estimated_end_time);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_client_navigation_guidance_progress_set_property (GObject *object,
                                                      guint prop_id,
                                                      const GValue *value,
                                                      GParamSpec *pspec)
{
  TrpClientNavigationGuidanceProgress *self = TRP_CLIENT_NAVIGATION_GUIDANCE_PROGRESS (object);

  switch ((TrpClientNavigationGuidanceProgressProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    /* read only */
    case PROP_START_TIME:
    case PROP_ESTIMATED_END_TIME:
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_client_navigation_guidance_progress_dispose (GObject *object)
{
  TrpClientNavigationGuidanceProgress *self = (TrpClientNavigationGuidanceProgress *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_clear_object (&self->conn);
  g_clear_object (&self->proxy);

  g_clear_pointer (&self->start_time, g_date_time_unref);
  g_clear_pointer (&self->estimated_end_time, g_date_time_unref);

  g_assert (self->init_tasks == NULL);

  G_OBJECT_CLASS (trp_client_navigation_guidance_progress_parent_class)
      ->dispose (object);
}

static void
trp_client_navigation_guidance_progress_class_init (TrpClientNavigationGuidanceProgressClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_client_navigation_guidance_progress_get_property;
  object_class->set_property = trp_client_navigation_guidance_progress_set_property;
  object_class->dispose = trp_client_navigation_guidance_progress_dispose;

  /**
   * TrpClientNavigationGuidanceProgress:connection:
   *
   * The #GDBusConnection used by the client.
   *
   * Since: 0.1612.0
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientNavigationGuidanceProgress:start-time:
   *
   * A #GDateTime representing the start time of the current journey,
   * as reported by the navigation system, or %NULL.
   *
   * Since: 0.1612.0
   */
  properties[PROP_START_TIME] = g_param_spec_boxed (
      "start-time", "Start time", "GDateTime representing the start time",
      G_TYPE_DATE_TIME, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientNavigationGuidanceProgress:estimated-end-time:
   *
   * A #GDateTime representing the estimated end time of the current journey,
   * as reported by the navigation system, or %NULL.
   *
   * Since: 0.1612.0
   */
  properties[PROP_ESTIMATED_END_TIME] = g_param_spec_boxed (
      "estimated-end-time", "Estimated end time", "GDateTime representing the estimated end time",
      G_TYPE_DATE_TIME, G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientNavigationGuidanceProgress::invalidated:
   * @self: a #TrpClientNavigationGuidanceProgress
   * @error: error which caused @self to be invalidated
   *
   * Emitted when the backing object underlying this #TrpClientNavigationGuidanceProgress
   * disappears, or it is otherwise disconnected.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus object for an #TrpClientNavigationGuidanceProgress disappears.
   *
   * Once @self has been invalidated it can no longer be used and should be
   * destroyed.
   *
   * Since: 0.1612.0
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", TRP_CLIENT_TYPE_NAVIGATION_GUIDANCE_PROGRESS,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
trp_client_navigation_guidance_progress_init (TrpClientNavigationGuidanceProgress *self)
{
  self->cancellable = g_cancellable_new ();
}

/**
 * trp_client_navigation_guidance_progress_new:
 * @connection: the #GDBusConnection to use to communicate with the navigation service
 *
 * Create a new #TrpClientNavigationGuidanceProgress.
 *
 * Returns: (transfer full): a new #TrpClientNavigationGuidanceProgress
 * Since: 0.1612.0
 */
TrpClientNavigationGuidanceProgress *
trp_client_navigation_guidance_progress_new (GDBusConnection *connection)
{
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);

  return g_object_new (TRP_CLIENT_TYPE_NAVIGATION_GUIDANCE_PROGRESS, "connection", connection, NULL);
}

static void
fire_invalidated (TrpClientNavigationGuidanceProgress *self,
                  GError *error)
{
  g_clear_object (&self->proxy);

  g_debug ("TrpClientNavigationGuidanceProgress invalidated: %s", error->message);

  g_signal_emit (self, signals[SIG_INVALIDATED], 0, error);
}

static void
fail_init_tasks (TrpClientNavigationGuidanceProgress *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (TrpClientNavigationGuidanceProgress *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
proxy_notify_name_owner_cb (GObject *obj,
                            GParamSpec *pspec,
                            gpointer user_data)
{
  TrpClientNavigationGuidanceProgress *self = user_data;
  g_autoptr (GError) error = NULL;

  g_set_error_literal (&error, G_DBUS_ERROR, G_DBUS_ERROR_DISCONNECTED,
                       "Progress proxy has disconnected");

  fire_invalidated (self, error);
}

static void
update_start_time (TrpClientNavigationGuidanceProgress *self)
{
  guint64 ts, old_ts = 0;

  if (self->start_time != NULL)
    {
      old_ts = g_date_time_to_unix (self->start_time);
      g_clear_pointer (&self->start_time, g_date_time_unref);
    }

  ts = trp_navigation_guidance1_progress_get_start_time (self->proxy);
  if (ts != 0)
    self->start_time = g_date_time_new_from_unix_utc (ts);

  if (ts != old_ts)
    g_object_notify (G_OBJECT (self), "start-time");
}

static void
update_estimated_end_time (TrpClientNavigationGuidanceProgress *self)
{
  guint64 ts, old_ts = 0;

  if (self->estimated_end_time != NULL)
    {
      old_ts = g_date_time_to_unix (self->estimated_end_time);
      g_clear_pointer (&self->estimated_end_time, g_date_time_unref);
    }

  ts = trp_navigation_guidance1_progress_get_estimated_end_time (self->proxy);
  if (ts != 0)
    self->estimated_end_time = g_date_time_new_from_unix_utc (ts);

  if (ts != old_ts)
    g_object_notify (G_OBJECT (self), "estimated-end-time");
}

static void
proxy_notify_start_time_cb (GObject *obj,
                            GParamSpec *pspec,
                            gpointer user_data)
{
  TrpClientNavigationGuidanceProgress *self = user_data;

  update_start_time (self);
}

static void
proxy_notify_estimated_end_time_cb (GObject *obj,
                                    GParamSpec *pspec,
                                    gpointer user_data)
{
  TrpClientNavigationGuidanceProgress *self = user_data;

  update_estimated_end_time (self);
}

static void
proxy_cb (GObject *source,
          GAsyncResult *res,
          gpointer user_data)
{
  TrpClientNavigationGuidanceProgress *self = user_data;
  g_autoptr (GError) error = NULL;
  g_autofree gchar *owner = NULL;

  self->proxy = trp_navigation_guidance1_progress_proxy_new_finish (res, &error);
  if (self->proxy == NULL)
    {
      fail_init_tasks (self, error);
      return;
    }

  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (self->proxy));
  if (owner == NULL)
    {
      error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_SERVICE_UNKNOWN,
                           "No navigation service running");
      fail_init_tasks (self, error);
      return;
    }

  g_signal_connect (self->proxy, "notify::g-name-owner",
                    G_CALLBACK (proxy_notify_name_owner_cb), self);
  g_signal_connect (self->proxy, "notify::start-time",
                    G_CALLBACK (proxy_notify_start_time_cb), self);
  g_signal_connect (self->proxy, "notify::estimated-end-time",
                    G_CALLBACK (proxy_notify_estimated_end_time_cb), self);

  g_object_freeze_notify (G_OBJECT (self));
  update_start_time (self);
  update_estimated_end_time (self);
  g_object_thaw_notify (G_OBJECT (self));

  complete_init_tasks (self);
}

static void
trp_client_navigation_guidance_progress_init_async (GAsyncInitable *initable,
                                                    int io_priority,
                                                    GCancellable *cancellable,
                                                    GAsyncReadyCallback callback,
                                                    gpointer user_data)
{
  TrpClientNavigationGuidanceProgress *self = TRP_CLIENT_NAVIGATION_GUIDANCE_PROGRESS (initable);
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->proxy != NULL)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  self->init_tasks = g_list_append (self->init_tasks, g_steal_pointer (&task));

  trp_navigation_guidance1_progress_proxy_new (self->conn, G_DBUS_PROXY_FLAGS_NONE,
                                               PROGRESS_BUS_NAME, PROGRESS_PATH,
                                               self->cancellable, proxy_cb, self);
}

static gboolean
trp_client_navigation_guidance_progress_init_finish (GAsyncInitable *initable,
                                                     GAsyncResult *result,
                                                     GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = trp_client_navigation_guidance_progress_init_async;
  iface->init_finish = trp_client_navigation_guidance_progress_init_finish;
}

/**
 * trp_client_navigation_guidance_progress_get_start_time:
 * @self: a #TrpClientNavigationGuidanceProgress
 *
 * Retrieve the start time of the current journey as exposed by the navigation service,
 * or %NULL if none is published.
 *
 * Returns: (transfer none) (nullable): a #GDateTime, or %NULL
 * Since: 0.1612.0
 */
GDateTime *
trp_client_navigation_guidance_progress_get_start_time (TrpClientNavigationGuidanceProgress *self)
{
  g_return_val_if_fail (TRP_CLIENT_IS_NAVIGATION_GUIDANCE_PROGRESS (self), NULL);

  return self->start_time;
}

/**
 * trp_client_navigation_guidance_progress_get_estimated_end_time:
 * @self: a #TrpClientNavigationGuidanceProgress
 *
 * Retrieve the estimated end time of the current journey as exposed by the navigation service,
 * or %NULL if none is published.
 *
 * Returns: (transfer none) (nullable): a #GDateTime, or %NULL
 * Since: 0.1612.0
 */
GDateTime *
trp_client_navigation_guidance_progress_get_estimated_end_time (TrpClientNavigationGuidanceProgress *self)
{
  g_return_val_if_fail (TRP_CLIENT_IS_NAVIGATION_GUIDANCE_PROGRESS (self), NULL);

  return self->estimated_end_time;
}
