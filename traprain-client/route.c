/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "route-internal.h"
#include "route.h"

/**
 * SECTION: traprain-client/route.h
 * @title: TrpClientRoute
 * @short_description: Potential route returned by #TrpClientNavigation
 * @see_also: #TrpClientNavigation
 *
 * #TrpRoute represents a potential navigation route returned by #TrpClientNavigation.
 *
 * Routes are composed of indexed segments containing a latitude and longitude.
 * The first one, at index 0, is the starting point of the route and the last one
 * its destination. All the segments between them are way point through the route.
 *
 * Use trp_client_route_get_n_segments() and trp_client_route_get_segment() to
 * iterate over segments.
 *
 * Since: 0.1.0
 */

/**
 * TrpClientRoute:
 *
 * #TrpClientRoute is an object representing a potential route.
 *
 * Since: 0.1.0
 */

G_DEFINE_TYPE (TrpClientRoute, trp_client_route, G_TYPE_OBJECT)

static void
trp_client_route_dispose (GObject *object)
{
  TrpClientRoute *self = (TrpClientRoute *) object;

  g_clear_object (&self->proxy);
  g_clear_pointer (&self->geo_desc, g_hash_table_unref);
  g_clear_pointer (&self->title, _trp_languages_map_unref);

  G_OBJECT_CLASS (trp_client_route_parent_class)
      ->dispose (object);
}

static void
trp_client_route_class_init (TrpClientRouteClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->dispose = trp_client_route_dispose;
}

static void
trp_client_route_init (TrpClientRoute *self)
{
}

/**
 * trp_client_route_get_total_distance:
 * @self: a #TrpClientRoute
 *
 * Return the estimated distance covered by @self, in metres.
 *
 * Returns: the route estimated total distance
 * Since: 0.1.0
 */
guint
trp_client_route_get_total_distance (TrpClientRoute *self)
{
  g_return_val_if_fail (TRP_CLIENT_IS_ROUTE (self), 0);

  return trp_navigation1_route_get_total_distance (self->proxy);
}

/**
 * trp_client_route_get_total_time:
 * @self: a #TrpClientRoute
 *
 * Return the estimated total time needed to travel @self, in seconds.
 *
 * Returns: the route estimated total time
 * Since: 0.1.0
 */
guint
trp_client_route_get_total_time (TrpClientRoute *self)
{
  g_return_val_if_fail (TRP_CLIENT_IS_ROUTE (self), 0);

  return trp_navigation1_route_get_total_time (self->proxy);
}

/**
 * trp_client_route_get_titles:
 * @self: a #TrpClientRoute
 *
 * Return the human-readable titles of @self in all available languages.
 *
 * Returns: (transfer none) (nullable): a #TrpLanguagesMap containing the title of @self in all available languages
 * Since: 0.1.0
 */
TrpLanguagesMap *
trp_client_route_get_titles (TrpClientRoute *self)
{
  g_return_val_if_fail (TRP_CLIENT_IS_ROUTE (self), NULL);

  return self->title;
}

/**
 * trp_client_route_get_title:
 * @self: a #TrpClientRoute
 * @language: a language code, see trp_client_route_get_titles()
 *
 * Return the human-readable title of @self in @language.
 *
 * Returns: the title of @self in @language
 * Since: 0.1.0
 */
const gchar *
trp_client_route_get_title (TrpClientRoute *self, const gchar *language)
{
  g_return_val_if_fail (TRP_CLIENT_IS_ROUTE (self), NULL);
  g_return_val_if_fail (language != NULL, NULL);

  return trp_languages_map_lookup (self->title, language);
}

/**
 * trp_client_route_get_n_segments:
 * @self: a #TrpClientRoute
 *
 * Return the number of segments composing @self.
 * Use trp_client_route_get_segment() to iterate over them.
 *
 * Returns: the number of segments
 * Since: 0.1.0
 */
guint
trp_client_route_get_n_segments (TrpClientRoute *self)
{
  GVariant *v;

  g_return_val_if_fail (TRP_CLIENT_IS_ROUTE (self), 0);

  v = trp_navigation1_route_get_geometry (self->proxy);
  return g_variant_n_children (v);
}

static gboolean
get_route_info (TrpClientRoute *self, guint index, gdouble *lat, gdouble *lon)
{
  g_autoptr (GVariant) v = NULL;
  gdouble _lat, _lon;

  g_return_val_if_fail (TRP_CLIENT_IS_ROUTE (self), FALSE);

  v = trp_navigation1_route_get_geometry (self->proxy);
  g_return_val_if_fail (index < g_variant_n_children (v), FALSE);

  v = g_variant_get_child_value (v, index);
  g_variant_get (v, "(dd)", &_lat, &_lon);

  if (lat != NULL)
    *lat = _lat;
  if (lon != NULL)
    *lon = _lon;

  return TRUE;
}

/**
 * trp_client_route_get_segment:
 * @self: a #TrpClientRoute
 * @index: the index of the segment
 * @lat: (out) (optional): pointer used to return the latitude of the segment
 * @lon: (out) (optional): pointer used to return the longitude of the segment
 * @descriptions: (out) (optional) (transfer none): pointer used to return the descriptions of the segment
 *
 * Return the latitude, longitude and human-readable descriptions of the segment at position @index.
 *
 * Since: 0.1.0
 */
void
trp_client_route_get_segment (TrpClientRoute *self, guint index, gdouble *lat, gdouble *lon, TrpLanguagesMap **descriptions)
{
  if (!get_route_info (self, index, lat, lon))
    return;

  if (descriptions)
    {
      *descriptions = g_hash_table_lookup (self->geo_desc, GUINT_TO_POINTER (index));

      if (*descriptions == NULL)
        {
          *descriptions = _trp_languages_map_new ();

          /* transfer ownership */
          g_hash_table_insert (self->geo_desc, GUINT_TO_POINTER (index), *descriptions);
        }
    }
}

/**
 * trp_client_route_get_segment_in_language:
 * @self: a #TrpClientRoute
 * @index: the index of the segment
 * @language: a language code, as defined in trp_client_route_get_titles()
 * @lat: (out) (optional): pointer used to return the latitude of the segment
 * @lon: (out) (optional): pointer used to return the longitude of the segment
 * @description: (out) (optional) (transfer none): pointer used to return the descriptions of the segment in @language
 *
 * Return the latitude, longitude and human-readable descriptions in @language of the segment at position @index.
 *
 * Since: 0.1.0
 */
void
trp_client_route_get_segment_in_language (TrpClientRoute *self, guint index, const gchar *language, gdouble *lat, gdouble *lon, const gchar **description)
{
  if (!get_route_info (self, index, lat, lon))
    return;

  if (description)
    {
      TrpLanguagesMap *map;

      g_return_if_fail (language != NULL);

      map = g_hash_table_lookup (self->geo_desc, GUINT_TO_POINTER (index));
      if (map != NULL)
        *description = trp_languages_map_lookup (map, language);
      else
        *description = NULL;
    }
}

static GHashTable *
build_geometry_descriptions (TrpClientRoute *self)
{
  GVariant *v, *desc;
  g_autoptr (GVariantIter) iter = NULL;
  guint i;
  GHashTable *result;

  result = g_hash_table_new_full (NULL, NULL, NULL, (GDestroyNotify) _trp_languages_map_unref);

  v = trp_navigation1_route_get_geometry_descriptions (self->proxy);

  g_variant_get (v, "a{ua{ss}}", &iter);
  while (g_variant_iter_next (iter, "{u@a{ss}}", &i, &desc))
    {
      g_hash_table_insert (result, GUINT_TO_POINTER (i),
                           _trp_languages_map_new_from_variant (desc));
    }

  return result;
}

static TrpLanguagesMap *
build_title (TrpClientRoute *self)
{
  GVariant *v;

  v = trp_navigation1_route_get_title (self->proxy);
  return _trp_languages_map_new_from_variant (v);
}

TrpClientRoute *
_trp_client_route_new (TrpNavigation1Route *proxy)
{
  TrpClientRoute *self;

  self = g_object_new (TRP_CLIENT_TYPE_ROUTE, NULL);

  /* Don't use a property here as TrpNavigation1Route is not part of
   * the API */
  self->proxy = g_object_ref (proxy);

  self->title = build_title (self);
  /* Convert all the geometry descriptions variants to array to save us from
   * iterating on all of them in trp_client_route_get_segment(). */
  self->geo_desc = build_geometry_descriptions (self);

  return self;
}
