/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "mock.h"

#include "dbus/org.apertis.Traprain1.Mock.h"
#include "traprain-common/route-internal.h"

#define MOCK_BUS_NAME "org.apertis.Traprain1.Mock"
#define NAVIGATION_ROUTES_PATH "/org/apertis/Navigation1/Routes"

/**
 * SECTION: traprain-client/mock.h
 * @title: TrpClientMock
 * @short_description: API to easily control Traprain's mock navigation service
 *
 * #TrpClientMock is an object to control the routes published by Traprain's mock
 * navigation service.
 *
 * Since: 0.1.0
 */

/**
 * TrpClientMock:
 *
 * #TrpClientMock is an object to control Traprain's mock navigation service.
 *
 * Since: 0.1.0
 */

struct _TrpClientMock
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  Trp1Mock *proxy; /* owned */

  GCancellable *cancellable;           /* owned */
  GList /* owned GTask */ *init_tasks; /* owned */
  gboolean initialized;
  GError *error; /* nullable; owned */
};

typedef enum {
  PROP_CONNECTION = 1,
  /*< private >*/
  PROP_LAST = PROP_CONNECTION
} TrpClientMockProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (TrpClientMock, trp_client_mock, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init))

static void
trp_client_mock_get_property (GObject *object,
                              guint prop_id,
                              GValue *value,
                              GParamSpec *pspec)
{
  TrpClientMock *self = TRP_CLIENT_MOCK (object);

  switch ((TrpClientMockProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_client_mock_set_property (GObject *object,
                              guint prop_id,
                              const GValue *value,
                              GParamSpec *pspec)
{
  TrpClientMock *self = TRP_CLIENT_MOCK (object);

  switch ((TrpClientMockProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_client_mock_dispose (GObject *object)
{
  TrpClientMock *self = (TrpClientMock *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->init_tasks != NULL)
    {
      g_list_free_full (self->init_tasks, g_object_unref);
      self->init_tasks = NULL;
    }
  g_clear_error (&self->error);

  g_clear_object (&self->proxy);
  g_clear_object (&self->conn);

  G_OBJECT_CLASS (trp_client_mock_parent_class)
      ->dispose (object);
}

static void
trp_client_mock_class_init (TrpClientMockClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_client_mock_get_property;
  object_class->set_property = trp_client_mock_set_property;
  object_class->dispose = trp_client_mock_dispose;

  /**
   * TrpClientMock:connection:
   *
   * The #GDBusConnection used by the client to communicate with the mock
   * service.
   *
   * Since: 0.1.0
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientMock::invalidated:
   * @self: a #TrpClientMock
   * @error: error which caused @self to be invalidated
   *
   * Emitted when the backing object underlying this #TrpClientMock
   * disappears, or it is otherwise disconnected.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus object for an #TrpClientMock disappears.
   *
   * Once @self has been invalidated it can no longer be used and should be
   * destroyed.
   *
   * Since: 0.1.0
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", TRP_CLIENT_TYPE_MOCK,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
trp_client_mock_init (TrpClientMock *self)
{
  self->cancellable = g_cancellable_new ();
}

/**
 * trp_client_mock_new_async:
 * @connection: the #GDBusConnection to use to communicate with the mock service
 * @cancellable: (nullable): a #GCancellable, or %NULL
 * @callback: callback to invoke on completion
 * @user_data: user data to pass to @callback
 *
 * Create a new #TrpClientMock and asynchronously
 * set it up. This is an asynchronous process which might fail; object
 * instantiation must be finished (or the error returned) by calling
 * trp_client_mock_new_finish().
 *
 * Since: 0.1.0
 */
void
trp_client_mock_new_async (GDBusConnection *connection,
                           GCancellable *cancellable,
                           GAsyncReadyCallback callback,
                           gpointer user_data)
{
  g_return_if_fail (G_IS_DBUS_CONNECTION (connection));
  g_return_if_fail (cancellable == NULL || G_IS_CANCELLABLE (cancellable));

  g_async_initable_new_async (TRP_CLIENT_TYPE_MOCK, G_PRIORITY_DEFAULT,
                              cancellable, callback, user_data,
                              "connection", connection, NULL);
}

/**
 * trp_client_mock_new_finish:
 * @result: asynchronous operation result
 * @error: return location for a #GError
 *
 * Finish initialising a #TrpClientMock. See trp_client_mock_new_async().
 *
 * Returns: (transfer full): initialised #TrpClientMock, or %NULL on error
 * Since: 0.1.0
 */
TrpClientMock *
trp_client_mock_new_finish (GAsyncResult *result,
                            GError **error)
{
  g_autoptr (GObject) source_object = NULL;

  g_return_val_if_fail (G_IS_ASYNC_RESULT (result), NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  source_object = g_async_result_get_source_object (result);
  return TRP_CLIENT_MOCK (g_async_initable_new_finish (G_ASYNC_INITABLE (source_object),
                                                       result, error));
}

static void
fail_init_tasks (TrpClientMock *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (self->error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (TrpClientMock *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
proxy_notify_name_owner_cb (GObject *obj,
                            GParamSpec *pspec,
                            gpointer user_data)
{
  TrpClientMock *self = user_data;

  if (self->error != NULL)
    return;

  self->error = g_error_new_literal (G_DBUS_ERROR, G_DBUS_ERROR_DISCONNECTED,
                                     "Proxy has disconnected");

  g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->error);
}

static void
proxy_cb (GObject *source,
          GAsyncResult *res,
          gpointer user_data)
{
  /* While creating the object, GTask are the ones keeping, potentially, the
   * only reference to @self. Keep a ref to make sure it stays alive during
   * the whole function. */
  g_autoptr (TrpClientMock) self = g_object_ref (user_data);
  g_autofree gchar *owner = NULL;

  self->proxy = trp_1_mock_proxy_new_for_bus_finish (res, &self->error);

  if (self->proxy == NULL)
    {
      fail_init_tasks (self);
      return;
    }

  g_signal_connect (self->proxy, "notify::g-name-owner",
                    G_CALLBACK (proxy_notify_name_owner_cb), self);

  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (self->proxy));
  if (owner == NULL)
    {
      self->error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_SERVICE_UNKNOWN,
                                 "no mock service running");
      fail_init_tasks (self);
      return;
    }

  complete_init_tasks (self);
}

static void
trp_client_mock_init_async (GAsyncInitable *initable,
                            int io_priority,
                            GCancellable *cancellable,
                            GAsyncReadyCallback callback,
                            gpointer user_data)
{
  TrpClientMock *self = TRP_CLIENT_MOCK (initable);
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);
  g_task_set_source_tag (task, trp_client_mock_new_finish);

  if (self->error)
    g_task_return_error (task, g_error_copy (self->error));
  else if (self->initialized)
    g_task_return_boolean (task, TRUE);
  else
    {
      gboolean start_init;

      start_init = (self->init_tasks == NULL);
      self->init_tasks = g_list_append (self->init_tasks, g_steal_pointer (&task));

      if (start_init)
        /* Use the Mock bus name as we want this specific implementation of the
         * Navigation service */
        trp_1_mock_proxy_new (self->conn, G_DBUS_PROXY_FLAGS_NONE,
                              MOCK_BUS_NAME, NAVIGATION_ROUTES_PATH,
                              self->cancellable, proxy_cb, self);
    }
}

static gboolean
trp_client_mock_init_finish (GAsyncInitable *initable,
                             GAsyncResult *result,
                             GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

/**
 * trp_client_mock_create_route:
 * @total_distance: the total distance covered by this route, in metres
 * @total_time: an estimation of the time needed to travel this route, in seconds
 *
 * Create a new TrpRoute representing a potential navigation route.
 *
 * One should generally create a route, set its title using trp_route_add_title()
 * and add the geometry of the route using trp_route_add_segment().
 * Once the route has been fully defined it should be passed to the #TrpClientMock
 * for publishing using trp_client_mock_add_route_async().
 *
 * Returns: (transfer full): a new #TrpRoute
 * Since: 0.1.0
 */
TrpRoute *
trp_client_mock_create_route (TrpClientMock *self,
                              guint total_distance,
                              guint total_time)
{
  return _trp_route_new (total_distance, total_time);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = trp_client_mock_init_async;
  iface->init_finish = trp_client_mock_init_finish;
}

static gboolean
check_invalidated (TrpClientMock *self,
                   GError **error)
{
  if (self->error == NULL)
    return TRUE;

  if (error != NULL)
    *error = g_error_copy (self->error);

  return FALSE;
}

static void
add_route_cb (GObject *source,
              GAsyncResult *result,
              gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  guint index;
  GError *error = NULL;

  if (!trp_1_mock_call_add_route_finish ((Trp1Mock *) source, &index, result, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_int (task, index);
}

/**
 * trp_client_mock_add_route_async:
 * @self: a #TrpClientMock
 * @route: a #TrpRoute
 * @callback: callback to call when the request is satisfied.
 * @user_data: the data to pass to @callback function.
 *
 * Add @route to the list of routes exposed by the mock service.
 * @route should contain at least two segments: the start and destination of
 * the route.
 *
 * When the operation is finished, @callback will be called.
 * You can then call trp_client_mock_add_route_finish() to get the result of
 * the operation.
 *
 * Since: 0.1.0
 */
void
trp_client_mock_add_route_async (TrpClientMock *self,
                                 TrpRoute *route,
                                 GAsyncReadyCallback callback,
                                 gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  g_autoptr (TrpRoute) _route = route; /* transfer */
  GError *error = NULL;
  GVariant *title, *geometry, *geometry_desc;

  g_return_if_fail (TRP_CLIENT_IS_MOCK (self));
  g_return_if_fail (TRP_IS_ROUTE (route));
  g_return_if_fail (route->geometry->len >= 2);

  task = g_task_new (self, self->cancellable, callback, user_data);

  if (!check_invalidated (self, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_assert (self->proxy != NULL);

  _trp_route_to_variants (_route, &title, &geometry, &geometry_desc);

  trp_1_mock_call_add_route (self->proxy, title, route->total_distance,
                             route->total_time, geometry, geometry_desc,
                             self->cancellable, add_route_cb, g_steal_pointer (&task));
}

/**
 * trp_client_mock_add_route_finish:
 * @self: a #TrpClientMock
 * @result: a #GAsyncResult
 * @error: a #GError, or %NULL
 *
 * Finishes an operation started with trp_client_mock_add_route_async().
 *
 * Returns: the index of the newly added route, or -1 on error
 * Since: 0.1.0
 */
gint
trp_client_mock_add_route_finish (TrpClientMock *self,
                                  GAsyncResult *result,
                                  GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), -1);
  g_return_val_if_fail (error == NULL || *error == NULL, -1);

  return g_task_propagate_int (G_TASK (result), error);
}

static void
remove_route_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  GError *error = NULL;

  if (!trp_1_mock_call_remove_route_finish ((Trp1Mock *) source, result, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * trp_client_mock_remove_route_async:
 * @self: a #TrpClientMock
 * @index: the index of the route to remove
 * @callback: callback to call when the request is satisfied.
 * @user_data: the data to pass to @callback function.
 *
 * Remove the route at index @index from the list of routes exposed by the
 * mock service.
 *
 * When the operation is finished, @callback will be called.
 * You can then call trp_client_mock_remove_route_finish() to get the result of
 * the operation.
 *
 * Since: 0.1.0
 */
void
trp_client_mock_remove_route_async (TrpClientMock *self,
                                    guint index,
                                    GAsyncReadyCallback callback,
                                    gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GError *error = NULL;

  g_return_if_fail (TRP_CLIENT_IS_MOCK (self));

  task = g_task_new (self, self->cancellable, callback, user_data);

  if (!check_invalidated (self, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_assert (self->proxy != NULL);

  trp_1_mock_call_remove_route (self->proxy, index, self->cancellable,
                                remove_route_cb, g_steal_pointer (&task));
}

/**
 * trp_client_mock_remove_route_finish:
 * @self: a #TrpClientMock
 * @result: a #GAsyncResult
 * @error: a #GError, or %NULL
 *
 * Finishes an operation started with trp_client_mock_remove_route_async().
 *
 * Returns: %TRUE if the operation succeeded, %FALSE on error
 * Since: 0.1.0
 */
gboolean
trp_client_mock_remove_route_finish (TrpClientMock *self,
                                     GAsyncResult *result,
                                     GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_assert_no_error (*error);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
set_current_route_cb (GObject *source,
                      GAsyncResult *result,
                      gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  GError *error = NULL;

  if (!trp_1_mock_call_set_current_route_finish ((Trp1Mock *) source, result, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * trp_client_mock_set_current_route_async:
 * @self: a #TrpClientMock
 * @index: the index of the route to set as current
 * @callback: callback to call when the request is satisfied.
 * @user_data: the data to pass to @callback function.
 *
 * Set the route at index @index as the current one.
 *
 * When the operation is finished, @callback will be called.
 * You can then call trp_client_mock_set_current_route_finish() to get the result of
 * the operation.
 *
 * Since: 0.1.0
 */
void
trp_client_mock_set_current_route_async (TrpClientMock *self,
                                         guint index,
                                         GAsyncReadyCallback callback,
                                         gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GError *error = NULL;

  g_return_if_fail (TRP_CLIENT_IS_MOCK (self));

  task = g_task_new (self, self->cancellable, callback, user_data);

  if (!check_invalidated (self, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_assert (self->proxy != NULL);

  trp_1_mock_call_set_current_route (self->proxy, index, self->cancellable,
                                     set_current_route_cb, g_steal_pointer (&task));
}

/**
 * trp_client_mock_set_current_route_finish:
 * @self: a #TrpClientMock
 * @result: a #GAsyncResult
 * @error: a #GError, or %NULL
 *
 * Finishes an operation started with trp_client_mock_set_current_route_async().
 *
 * Returns: %TRUE if the operation succeeded, %FALSE on error
 * Since: 0.1.0
 */
gboolean
trp_client_mock_set_current_route_finish (TrpClientMock *self,
                                          GAsyncResult *result,
                                          GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_assert_no_error (*error);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
clear_routes_cb (GObject *source,
                 GAsyncResult *result,
                 gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  GError *error = NULL;

  if (!trp_1_mock_call_clear_routes_finish ((Trp1Mock *) source, result, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * trp_client_mock_clear_routes_async:
 * @self: a #TrpClientMock
 * @callback: callback to call when the request is satisfied.
 * @user_data: the data to pass to @callback function.
 *
 * Remove all the exposed routes and reset the current one.
 *
 * When the operation is finished, @callback will be called.
 * You can then call trp_client_mock_clear_routes_finish() to get the result of
 * the operation.
 *
 * Since: 0.1.0
 */
void
trp_client_mock_clear_routes_async (TrpClientMock *self,
                                    GAsyncReadyCallback callback,
                                    gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GError *error = NULL;

  g_return_if_fail (TRP_CLIENT_IS_MOCK (self));

  task = g_task_new (self, self->cancellable, callback, user_data);

  if (!check_invalidated (self, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_assert (self->proxy != NULL);

  trp_1_mock_call_clear_routes (self->proxy, self->cancellable,
                                clear_routes_cb, g_steal_pointer (&task));
}

/**
 * trp_client_mock_clear_routes_finish:
 * @self: a #TrpClientMock
 * @result: a #GAsyncResult
 * @error: a #GError, or %NULL
 *
 * Finishes an operation started with trp_client_mock_clear_routes_async().
 *
 * Returns: %TRUE if the operation succeeded, %FALSE on error
 * Since: 0.1.0
 */
gboolean
trp_client_mock_clear_routes_finish (TrpClientMock *self,
                                     GAsyncResult *result,
                                     GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_assert_no_error (*error);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
set_start_time_cb (GObject *source,
                   GAsyncResult *result,
                   gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  GError *error = NULL;

  if (!trp_1_mock_call_set_start_time_finish ((Trp1Mock *) source, result, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * trp_client_mock_set_start_time_async:
 * @self: a #TrpClientMock
 * @start_time: (nullable): a #GDateTime, or %NULL
 * @callback: callback to call when the request is satisfied.
 * @user_data: the data to pass to @callback function.
 *
 * Set the start time of the current journey to @start_time, or unset
 * it if @start_time is %NULL.
 *
 * When the operation is finished, @callback will be called.
 * You can then call trp_client_mock_set_start_time_finish() to get the result of
 * the operation.
 *
 * Since: 0.1703.1
 */
void
trp_client_mock_set_start_time_async (TrpClientMock *self,
                                      GDateTime *start_time,
                                      GAsyncReadyCallback callback,
                                      gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GError *error = NULL;
  guint64 ts = 0;

  g_return_if_fail (TRP_CLIENT_IS_MOCK (self));

  task = g_task_new (self, self->cancellable, callback, user_data);
  g_task_set_source_tag (task, trp_client_mock_set_start_time_async);

  if (!check_invalidated (self, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_assert (self->proxy != NULL);

  if (start_time != NULL)
    ts = g_date_time_to_unix (start_time);

  trp_1_mock_call_set_start_time (self->proxy, ts, self->cancellable,
                                  set_start_time_cb, g_steal_pointer (&task));
}

/**
 * trp_client_mock_set_start_time_finish:
 * @self: a #TrpClientMock
 * @result: a #GAsyncResult
 * @error: a #GError, or %NULL
 *
 * Finishes an operation started with trp_client_mock_set_start_time_async().
 *
 * Returns: %TRUE if the operation succeeded, %FALSE on error
 * Since: 0.1703.1
 */
gboolean
trp_client_mock_set_start_time_finish (TrpClientMock *self,
                                       GAsyncResult *result,
                                       GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
set_estimated_end_time_cb (GObject *source,
                           GAsyncResult *result,
                           gpointer user_data)
{
  g_autoptr (GTask) task = user_data;
  GError *error = NULL;

  if (!trp_1_mock_call_set_estimated_end_time_finish ((Trp1Mock *) source, result, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_task_return_boolean (task, TRUE);
}

/**
 * trp_client_mock_set_estimated_end_time_async:
 * @self: a #TrpClientMock
 * @estimated_end_time: (nullable): a #GDateTime, or %NULL
 * @callback: callback to call when the request is satisfied.
 * @user_data: the data to pass to @callback function.
 *
 * Set the estimated end time of the current journey to @estimated, or unset
 * it if @estimated_end_time is %NULL.
 *
 * When the operation is finished, @callback will be called.
 * You can then call trp_client_mock_set_estimated_end_time_finish() to get the result of
 * the operation.
 *
 * Since: 0.1703.1
 */
void
trp_client_mock_set_estimated_end_time_async (TrpClientMock *self,
                                              GDateTime *estimated_end_time,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data)
{
  g_autoptr (GTask) task = NULL;
  GError *error = NULL;
  guint64 ts = 0;

  g_return_if_fail (TRP_CLIENT_IS_MOCK (self));

  task = g_task_new (self, self->cancellable, callback, user_data);
  g_task_set_source_tag (task, trp_client_mock_set_estimated_end_time_async);

  if (!check_invalidated (self, &error))
    {
      g_task_return_error (task, error);
      return;
    }

  g_assert (self->proxy != NULL);

  if (estimated_end_time != NULL)
    ts = g_date_time_to_unix (estimated_end_time);

  trp_1_mock_call_set_estimated_end_time (self->proxy, ts, self->cancellable,
                                          set_estimated_end_time_cb, g_steal_pointer (&task));
}

/**
 * trp_client_mock_set_estimated_end_time_finish:
 * @self: a #TrpClientMock
 * @result: a #GAsyncResult
 * @error: a #GError, or %NULL
 *
 * Finishes an operation started with trp_client_mock_set_estimated_end_time_async().
 *
 * Returns: %TRUE if the operation succeeded, %FALSE on error
 * Since: 0.1703.1
 */
gboolean
trp_client_mock_set_estimated_end_time_finish (TrpClientMock *self,
                                               GAsyncResult *result,
                                               GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, self), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}
