/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_CLIENT_NAVIGATION_GUIDANCE_PROGRESS_H__
#define __TRAPRAIN_CLIENT_NAVIGATION_GUIDANCE_PROGRESS_H__

#include <gio/gio.h>
#include <glib-object.h>

G_BEGIN_DECLS

#define TRP_CLIENT_TYPE_NAVIGATION_GUIDANCE_PROGRESS (trp_client_navigation_guidance_progress_get_type ())
G_DECLARE_FINAL_TYPE (TrpClientNavigationGuidanceProgress, trp_client_navigation_guidance_progress, TRP_CLIENT, NAVIGATION_GUIDANCE_PROGRESS, GObject)

TrpClientNavigationGuidanceProgress *trp_client_navigation_guidance_progress_new (GDBusConnection *connection);

GDateTime *trp_client_navigation_guidance_progress_get_start_time (TrpClientNavigationGuidanceProgress *self);
GDateTime *trp_client_navigation_guidance_progress_get_estimated_end_time (TrpClientNavigationGuidanceProgress *self);

G_END_DECLS

#endif /* __TRAPRAIN_CLIENT_NAVIGATION_GUIDANCE_PROGRESS_H__ */
