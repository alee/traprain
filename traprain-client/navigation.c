/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "navigation.h"
#include "route-internal.h"

#include "dbus/org.apertis.Navigation1.h"

#define NAVIGATION_ROUTES_PATH "/org/apertis/Navigation1/Routes"
#define NAVIGATION_BUS_NAME "org.apertis.Navigation1"

#define NAVIGATION_ROUTE_IFACE "org.apertis.Navigation1.Route"

/**
 * SECTION: traprain-client/navigation.h
 * @title: TrpClientNavigation
 * @short_description: Retrieve information about navigation routes
 * published by the navigation system.
 *
 * #TrpClientNavigation is used to retrieve information about navigation routes which are
 * published by the navigation system.
 *
 * Once you have created a #TrpClientNavigation, g_async_initable_init_async() should be called so it can start
 * fetching data from the navigation service.
 *
 * Use trp_client_navigation_get_routes() and trp_client_navigation_get_current_route()
 * to retrieve the potential navigation routes. Changes are notified using the
 * #GObject::notify signal.
 *
 * Since: 0.1.0
 */

/**
 * TrpClientNavigation:
 *
 * #TrpClientNavigation is an object to retrieve information regarding the routes
 * published by the navigation system.
 *
 * Since: 0.1.0
 */

struct _TrpClientNavigation
{
  GObject parent;

  GDBusConnection *conn; /* owned */

  TrpNavigation1Routes *routes_proxy;  /* owned */
  GList /*<owned GTask>*/ *init_tasks; /* owned */
  GCancellable *cancellable;           /* owned */
  GDBusObjectManager *object_mgr;      /* owned */

  GPtrArray /* <owned TrpClientRoute *> */ *routes; /* owned */
  /* Contains the same object as in routes, used to easily re-use existing objects
   * when updating routes. */
  GHashTable /* <owned gchar*> -> <owned TrpClientRoute *> */ *routes_by_path; /* owned */
  guint update_id;
  TrpClientRoute *current_route; /* owned */
};

typedef enum {
  PROP_CONNECTION = 1,
  PROP_ROUTES,
  PROP_CURRENT_ROUTE,
  /*< private >*/
  PROP_LAST = PROP_CURRENT_ROUTE
} TrpClientNavigationProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (TrpClientNavigation, trp_client_navigation, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init))

static void
trp_client_navigation_get_property (GObject *object,
                                    guint prop_id,
                                    GValue *value,
                                    GParamSpec *pspec)
{
  TrpClientNavigation *self = TRP_CLIENT_NAVIGATION (object);

  switch ((TrpClientNavigationProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    case PROP_ROUTES:
      g_value_set_boxed (value, trp_client_navigation_get_routes (self));
      break;
    case PROP_CURRENT_ROUTE:
      g_value_set_object (value, trp_client_navigation_get_current_route (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_client_navigation_set_property (GObject *object,
                                    guint prop_id,
                                    const GValue *value,
                                    GParamSpec *pspec)
{
  TrpClientNavigation *self = TRP_CLIENT_NAVIGATION (object);

  switch ((TrpClientNavigationProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    case PROP_ROUTES:
    case PROP_CURRENT_ROUTE:
    /* read only */
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_client_navigation_dispose (GObject *object)
{
  TrpClientNavigation *self = (TrpClientNavigation *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  g_clear_object (&self->routes_proxy);

  if (self->init_tasks != NULL)
    {
      g_list_free_full (self->init_tasks, g_object_unref);
      self->init_tasks = NULL;
    }

  g_clear_object (&self->object_mgr);
  g_clear_pointer (&self->routes, g_ptr_array_unref);
  g_clear_pointer (&self->routes_by_path, g_hash_table_unref);
  g_clear_object (&self->current_route);
  g_clear_object (&self->conn);

  G_OBJECT_CLASS (trp_client_navigation_parent_class)
      ->dispose (object);
}

static void
trp_client_navigation_class_init (TrpClientNavigationClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_client_navigation_get_property;
  object_class->set_property = trp_client_navigation_set_property;
  object_class->dispose = trp_client_navigation_dispose;

  /**
   * TrpClientNavigation:connection:
   *
   * The #GDBusConnection used by the client.
   *
   * Since: 0.1.0
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientNavigation:routes:
   *
   * #GPtrArray containing the #TrpClientRoute currently exposed by the navigation service.
   * Each one represents a potential navigation route, with the most highly recommended
   * ones first.
   *
   * Since: 0.1.0
   */
  properties[PROP_ROUTES] = g_param_spec_boxed (
      "routes", "Array of TrpClientRoute",
      "Routes currently exposed by the navigation service.", G_TYPE_PTR_ARRAY,
      G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientNavigation:current-route:
   *
   * The #TrpClientRoute currently used by the navigation service.
   * ones first.
   *
   * Since: 0.1.0
   */
  properties[PROP_CURRENT_ROUTE] = g_param_spec_object (
      "current-route", "Current Route",
      "Current active routes", TRP_CLIENT_TYPE_ROUTE,
      G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

  /**
   * TrpClientNavigation::invalidated:
   * @self: a #TrpClientNavigation
   * @error: error which caused @self to be invalidated
   *
   * Emitted when the backing object underlying this #TrpClientNavigation
   * disappears, or it is otherwise disconnected.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus object for an #TrpClientNavigation disappears.
   *
   * Once @self has been invalidated it can no longer be used and should be
   * destroyed.
   *
   * Since: 0.1.0
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", TRP_CLIENT_TYPE_NAVIGATION,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
trp_client_navigation_init (TrpClientNavigation *self)
{
  self->cancellable = g_cancellable_new ();

  self->routes = g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);
  self->routes_by_path = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_object_unref);
}

/**
 * trp_client_navigation_new:
 * @connection: the #GDBusConnection to use to communicate with the navigation service
 *
 * Create a new #TrpClientNavigation.
 *
 * Returns: (transfer full): a new #TrpClientNavigation
 * Since: 0.1.0
 */
TrpClientNavigation *
trp_client_navigation_new (GDBusConnection *connection)
{
  g_return_val_if_fail (G_IS_DBUS_CONNECTION (connection), NULL);

  return g_object_new (TRP_CLIENT_TYPE_NAVIGATION, "connection", connection, NULL);
}

static gboolean
purge_old_routes (gpointer k,
                  gpointer v,
                  gpointer user_data)
{
  TrpClientNavigation *self = user_data;
  TrpClientRoute *route = v;

  return route->update_id != self->update_id;
}

static void
fire_invalidated (TrpClientNavigation *self,
                  GError *error)
{
  g_clear_object (&self->routes_proxy);
  g_clear_object (&self->object_mgr);

  g_debug ("TrpClientNavigation invalidated: %s", error->message);

  g_signal_emit (self, signals[SIG_INVALIDATED], 0, error);
}

/* Update our current routes by fetching the paths of the routes exposed
 * by the service, creating TrpClientRoute wrapper for the new ones and
 * destroying the ones which have been removed.
 */
static void
update_routes (TrpClientNavigation *self)
{
  g_auto (GStrv) routes = NULL;
  guint i;

  /* trp_navigation1_routes_get_routes() is leaking the array
   * (see bgo#770335) so use _dup_ */
  routes = trp_navigation1_routes_dup_routes (self->routes_proxy);

  g_ptr_array_unref (self->routes);
  self->routes = g_ptr_array_sized_new (g_strv_length ((GStrv) routes));
  g_ptr_array_set_free_func (self->routes, g_object_unref);

  /* Don't care about overflows as this ID is just used to compare one
   * epoch and a subsequent one. */
  self->update_id++;

  for (i = 0; routes[i] != NULL; i++)
    {
      const gchar *path = routes[i];
      TrpClientRoute *route;

      route = g_hash_table_lookup (self->routes_by_path, path);
      if (route != NULL)
        {
          /* re-use existing TrpClientRoute */
          route = g_object_ref (route);
        }
      else
        {
          g_autoptr (GDBusObject) object = NULL;
          g_autoptr (GDBusInterface) iface = NULL;

          object = g_dbus_object_manager_get_object (self->object_mgr, path);
          if (object == NULL)
            {
              g_autoptr (GError) error = NULL;

              g_set_error (&error, G_DBUS_ERROR, G_DBUS_ERROR_UNKNOWN_OBJECT,
                           "ObjectManager doesn't know about this route %s", path);

              fire_invalidated (self, error);
              return;
            }

          iface = g_dbus_object_get_interface (object, NAVIGATION_ROUTE_IFACE);
          if (iface == NULL)
            {
              g_autoptr (GError) error = NULL;

              g_set_error (&error, G_DBUS_ERROR, G_DBUS_ERROR_UNKNOWN_INTERFACE,
                           "Object %s does not implement the Route interface",
                           g_dbus_object_get_object_path (object));

              fire_invalidated (self, error);
              return;
            }

          route = _trp_client_route_new (TRP_NAVIGATION1_ROUTE (iface));

          g_hash_table_insert (self->routes_by_path, g_strdup (path), g_object_ref (route));
        }

      route->update_id = self->update_id;
      g_ptr_array_add (self->routes, route);
    }

  /* Destroy routes which are no longer exposed by the service */
  g_hash_table_foreach_remove (self->routes_by_path, purge_old_routes, self);

  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ROUTES]);
}

static void
routes_changed_cb (GObject *object,
                   GParamSpec *spec,
                   TrpClientNavigation *self)
{
  update_routes (self);
}

static void
update_current_route (TrpClientNavigation *self)
{
  gint index;

  g_clear_object (&self->current_route);

  index = trp_navigation1_routes_get_current_route (self->routes_proxy);
  if (index < 0)
    /* No current route */
    goto done;

  if ((guint) index >= self->routes->len)
    {
      g_warning ("Route at index %u has not been exposed; can't be set as current", index);
      return;
    }

  self->current_route = g_object_ref (g_ptr_array_index (self->routes, index));

done:
  g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_CURRENT_ROUTE]);
}

static void
current_route_changed_cb (GObject *object,
                          GParamSpec *spec,
                          TrpClientNavigation *self)
{
  update_current_route (self);
}

static void
fail_init_tasks (TrpClientNavigation *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (TrpClientNavigation *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
manager_new_cb (GObject *source,
                GAsyncResult *res,
                gpointer user_data)
{
  TrpClientNavigation *self = user_data;
  g_autoptr (GError) error = NULL;

  self->object_mgr = trp_object_manager_client_new_finish (res, &error);
  if (self->object_mgr == NULL)
    {
      fail_init_tasks (self, error);
    }
  else
    {
      g_signal_connect (self->routes_proxy, "notify::routes", G_CALLBACK (routes_changed_cb), self);
      g_signal_connect (self->routes_proxy, "notify::current-route", G_CALLBACK (current_route_changed_cb), self);

      update_routes (self);
      update_current_route (self);

      complete_init_tasks (self);
    }
}

static void
proxy_notify_name_owner_cb (GObject *obj,
                            GParamSpec *pspec,
                            gpointer user_data)
{
  TrpClientNavigation *self = user_data;
  g_autoptr (GError) error = NULL;

  g_set_error_literal (&error, G_DBUS_ERROR, G_DBUS_ERROR_DISCONNECTED,
                       "Routes proxy has disconnected");

  fire_invalidated (self, error);
}

static void
proxy_cb (GObject *source,
          GAsyncResult *res,
          gpointer user_data)
{
  TrpClientNavigation *self = user_data;
  g_autoptr (GError) error = NULL;
  GDBusConnection *conn;
  g_autofree gchar *owner = NULL;

  self->routes_proxy = trp_navigation1_routes_proxy_new_for_bus_finish (res, &error);
  if (self->routes_proxy == NULL)
    {
      fail_init_tasks (self, error);
      return;
    }

  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (self->routes_proxy));
  if (owner == NULL)
    {
      error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_SERVICE_UNKNOWN,
                           "No navigation service running");
      fail_init_tasks (self, error);
      return;
    }

  g_signal_connect (self->routes_proxy, "notify::g-name-owner",
                    G_CALLBACK (proxy_notify_name_owner_cb), self);

  conn = g_dbus_proxy_get_connection (G_DBUS_PROXY (self->routes_proxy));

  trp_object_manager_client_new (conn, G_DBUS_OBJECT_MANAGER_CLIENT_FLAGS_NONE, NAVIGATION_BUS_NAME,
                                 NAVIGATION_ROUTES_PATH, self->cancellable, manager_new_cb, self);
}

static void
trp_client_navigation_init_async (GAsyncInitable *initable,
                                  int io_priority,
                                  GCancellable *cancellable,
                                  GAsyncReadyCallback callback,
                                  gpointer user_data)
{
  TrpClientNavigation *self = TRP_CLIENT_NAVIGATION (initable);
  g_autoptr (GTask) task = NULL;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->routes_proxy != NULL)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  self->init_tasks = g_list_append (self->init_tasks, g_steal_pointer (&task));

  trp_navigation1_routes_proxy_new (self->conn, G_DBUS_PROXY_FLAGS_NONE,
                                    NAVIGATION_BUS_NAME, NAVIGATION_ROUTES_PATH,
                                    self->cancellable, proxy_cb, self);
}

static gboolean
trp_client_navigation_init_finish (GAsyncInitable *initable,
                                   GAsyncResult *result,
                                   GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = trp_client_navigation_init_async;
  iface->init_finish = trp_client_navigation_init_finish;
}

/**
 * trp_client_navigation_get_routes:
 * @self: a #TrpClientNavigation
 *
 * Retrieve all the #TrpClientRoute currently exposed by the navigation service.
 *
 * Returns: (element-type TrpClientRoute) (transfer none): array of #TrpClientRoute
 * Since: 0.1.0
 */
GPtrArray *
trp_client_navigation_get_routes (TrpClientNavigation *self)
{
  return self->routes;
}

/**
 * trp_client_navigation_get_current_route:
 * @self: a #TrpClientNavigation
 *
 * Return the currently active #TrpClientRoute.
 *
 * Returns: (transfer none): the active #TrpClientRoute
 * Since: 0.1.0
 */
TrpClientRoute *
trp_client_navigation_get_current_route (TrpClientNavigation *self)
{
  return self->current_route;
}
