/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "turn-by-turn-service.h"

#include <string.h>

#include "dbus/org.apertis.NavigationGuidance1.TurnByTurn.h"
#include "turn-by-turn-notification.h"

#define TBT_BUS_NAME "org.apertis.NavigationGuidance1.TurnByTurn"
#define TBT_PATH "/org/apertis/NavigationGuidance1/TurnByTurn"

/**
 * SECTION: traprain-guidance/turn-by-turn-service.h
 * @title: TrpGuidanceTurnByTurnService
 * @short_description: Receive turn-by-turn guidance notifications
 *
 * #TrpGuidanceTurnByTurnService is meant to be used by the service responsible
 * for displaying turn-by-turn guidance notifications.
 *
 * Once created, this UI should connect the #TrpGuidanceTurnByTurnService::new-notification
 * signal before initializing it with g_async_initable_init_async().
 * Each of those signals will represent a new notification from the navigation
 * system that should be presented to the user.
 *
 * Since: 0.1612.0
 */

/**
 * TrpGuidanceTurnByTurnService:
 *
 * Object used to receive turn-by-turn guidance notifications from the
 * navigation system.
 *
 * Since: 0.1612.0
 */

struct _TrpGuidanceTurnByTurnService
{
  GObject parent;

  TrpNavigationGuidance1TurnByTurn *service; /* owned */
  GDBusConnection *conn;                     /* owned */
  GCancellable *cancellable;                 /* owned */
  GList *init_tasks;                         /* owned */
  gboolean registered;
  guint own_name_id;         /* 0 if the server is not registered yet */
  GError *invalidated_error; /* owned, NULL until the service is invalidated */

  guint next_notification_id;
  GHashTable /* <guint> -> <weak reffed TrpGuidanceTurnByTurnNotification *> */ *notifications; /* owned */
};

typedef enum {
  PROP_CONNECTION = 1,
  /*< private >*/
  PROP_LAST = PROP_CONNECTION
} TrpGuidanceTurnByTurnServiceProperty;

static GParamSpec *properties[PROP_LAST + 1];

enum
{
  SIG_INVALIDATED,
  SIG_NEW_NOTIFICATION,
  LAST_SIGNAL
};

static guint signals[LAST_SIGNAL] = { 0 };

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data);

G_DEFINE_TYPE_WITH_CODE (TrpGuidanceTurnByTurnService, trp_guidance_turn_by_turn_service, G_TYPE_OBJECT, G_IMPLEMENT_INTERFACE (G_TYPE_ASYNC_INITABLE, async_initable_iface_init));

static void
trp_guidance_turn_by_turn_service_get_property (GObject *object,
                                                guint prop_id,
                                                GValue *value,
                                                GParamSpec *pspec)
{
  TrpGuidanceTurnByTurnService *self = TRP_GUIDANCE_TURN_BY_TURN_SERVICE (object);

  switch ((TrpGuidanceTurnByTurnServiceProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_value_set_object (value, self->conn);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_guidance_turn_by_turn_service_set_property (GObject *object,
                                                guint prop_id,
                                                const GValue *value,
                                                GParamSpec *pspec)
{
  TrpGuidanceTurnByTurnService *self = TRP_GUIDANCE_TURN_BY_TURN_SERVICE (object);

  switch ((TrpGuidanceTurnByTurnServiceProperty) prop_id)
    {
    case PROP_CONNECTION:
      g_assert (self->conn == NULL); /* construct only */
      self->conn = g_value_dup_object (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
notification_destroyed_cb (gpointer user_data,
                           GObject *notif)
{
  TrpGuidanceTurnByTurnService *self = user_data;
  GHashTableIter iter;
  gpointer v;

  g_hash_table_iter_init (&iter, self->notifications);
  while (g_hash_table_iter_next (&iter, NULL, &v))
    {
      if (v == notif)
        {
          g_hash_table_iter_remove (&iter);
          return;
        }
    }
}

static void
trp_guidance_turn_by_turn_service_dispose (GObject *object)
{
  TrpGuidanceTurnByTurnService *self = (TrpGuidanceTurnByTurnService *) object;

  g_cancellable_cancel (self->cancellable);
  g_clear_object (&self->cancellable);

  if (self->own_name_id != 0)
    {
      g_bus_unown_name (self->own_name_id);
      self->own_name_id = 0;
    }

  if (self->registered)
    {
      g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (self->service));
      self->registered = FALSE;
    }

  if (self->notifications != NULL)
    {
      GHashTableIter iter;
      gpointer v;

      g_hash_table_iter_init (&iter, self->notifications);
      while (g_hash_table_iter_next (&iter, NULL, &v))
        g_object_weak_unref (G_OBJECT (v), notification_destroyed_cb, self);

      g_clear_pointer (&self->notifications, g_hash_table_unref);
    }

  g_clear_object (&self->service);
  g_clear_object (&self->conn);

  g_assert (self->init_tasks == NULL);

  g_clear_error (&self->invalidated_error);

  G_OBJECT_CLASS (trp_guidance_turn_by_turn_service_parent_class)
      ->dispose (object);
}

static void
trp_guidance_turn_by_turn_service_class_init (TrpGuidanceTurnByTurnServiceClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = trp_guidance_turn_by_turn_service_get_property;
  object_class->set_property = trp_guidance_turn_by_turn_service_set_property;
  object_class->dispose = trp_guidance_turn_by_turn_service_dispose;

  /**
   * TrpGuidanceTurnByTurnService:connection:
   *
   * The #GDBusConnection used by the service to receive turn-by-turn notification.
   *
   * Since: 0.1612.0
   */
  properties[PROP_CONNECTION] = g_param_spec_object (
      "connection", "Connection", "GDBusConnection", G_TYPE_DBUS_CONNECTION,
      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);

  /**
   * TrpGuidanceTurnByTurnService::invalidated:
   * @self: a #TrpGuidanceTurnByTurnService
   * @error: error which caused @self to be invalidated
   *
   * Emitted when the backing object underlying this #TrpGuidanceTurnByTurnService
   * disappears, or it is otherwise disconnected.
   * The most common reason for this signal to be emitted is if the
   * underlying D-Bus service name is lost.
   *
   * After this signal is emitted, all method calls to #TrpGuidanceTurnByTurnService methods will
   * fail with the same error as the one reported in the signal (usually
   * #G_DBUS_ERROR_NAME_HAS_NO_OWNER).
   *
   * Since: 0.1612.0
   */
  signals[SIG_INVALIDATED] = g_signal_new ("invalidated", TRP_GUIDANCE_TYPE_TURN_BY_TURN_SERVICE,
                                           G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                           G_TYPE_NONE, 1, G_TYPE_ERROR);

  /**
   * TrpGuidanceTurnByTurnService::new-notification:
   * @self: a #TrpGuidanceTurnByTurnService
   * @notification: a #TrpGuidanceTurnByTurnNotification
   *
   * Emitted when a new notification is received from the navigation system to
   * be displayed. The guidance UI should keep a reference on @notification
   * while it's being handled.
   *
   * Since: 0.1612.0
   */
  signals[SIG_NEW_NOTIFICATION] = g_signal_new ("new-notification", TRP_GUIDANCE_TYPE_TURN_BY_TURN_SERVICE,
                                                G_SIGNAL_RUN_LAST, 0, NULL, NULL, NULL,
                                                G_TYPE_NONE, 1, TRP_GUIDANCE_TYPE_TURN_BY_TURN_NOTIFICATION);

  g_object_class_install_properties (object_class, G_N_ELEMENTS (properties), properties);
}

static void
update_next_notification_id (TrpGuidanceTurnByTurnService *self)
{
  guint current;

  current = self->next_notification_id;

  do
    {
      self->next_notification_id++;

      if (self->next_notification_id == 0)
        /* Not a valid ID */
        continue;

      if (g_hash_table_lookup (self->notifications,
                               GUINT_TO_POINTER (self->next_notification_id)) == NULL)
        return;
    }
  while (current != self->next_notification_id);

  g_critical ("All notification IDs are used. Service is probably leaking them.");
}

static gboolean
on_handle_notify (TrpNavigationGuidance1TurnByTurn *service,
                  GDBusMethodInvocation *invocation,
                  guint replace_id,
                  const gchar *_icon_name,
                  const gchar *summary,
                  const gchar *_body,
                  GVariant *hints,
                  gint expire_timeout,
                  TrpGuidanceTurnByTurnService *self)
{
  TrpGuidanceTurnByTurnNotification *notif;
  guint notification_id;
  const gchar *icon_name = NULL, *body = NULL;

  if (strlen (_body) > 0)
    body = _body;
  if (strlen (_icon_name) > 0)
    icon_name = _icon_name;

  notif = g_hash_table_lookup (self->notifications, GUINT_TO_POINTER (replace_id));
  if (notif != NULL)
    {
      /* Existing notification is being updated */
      notification_id = replace_id;

      g_object_set (notif,
                    "summary", summary,
                    "body", body,
                    "icon", icon_name,
                    "expire-timeout", expire_timeout,
                    NULL);

      g_signal_emit_by_name (notif, "updated");
    }
  else
    {
      notification_id = self->next_notification_id;
      update_next_notification_id (self);

      notif = g_object_new (TRP_GUIDANCE_TYPE_TURN_BY_TURN_NOTIFICATION,
                            "summary", summary,
                            "body", body,
                            "icon", icon_name,
                            "expire-timeout", expire_timeout,
                            NULL);

      g_object_weak_ref (G_OBJECT (notif), notification_destroyed_cb, self);

      g_hash_table_insert (self->notifications, GUINT_TO_POINTER (notification_id), notif);

      g_signal_emit (self, signals[SIG_NEW_NOTIFICATION], 0, notif);

      g_object_unref (notif);
    }

  trp_navigation_guidance1_turn_by_turn_complete_notify (service, invocation, notification_id);

  return TRUE;
}

static gboolean
on_handle_close_notification (TrpNavigationGuidance1TurnByTurn *service,
                              GDBusMethodInvocation *invocation,
                              guint id,
                              TrpGuidanceTurnByTurnService *self)
{
  TrpGuidanceTurnByTurnNotification *notif;

  notif = g_hash_table_lookup (self->notifications, GUINT_TO_POINTER (id));
  if (notif == NULL)
    {
      g_dbus_method_invocation_return_error (invocation, G_DBUS_ERROR, G_DBUS_ERROR_INVALID_ARGS,
                                             "Unknown notification %u", id);
      return TRUE;
    }

  g_signal_emit_by_name (notif, "close");

  trp_navigation_guidance1_turn_by_turn_complete_close_notification (service, invocation);

  return TRUE;
}

static void
trp_guidance_turn_by_turn_service_init (TrpGuidanceTurnByTurnService *self)
{
  self->service = trp_navigation_guidance1_turn_by_turn_skeleton_new ();

  self->next_notification_id = 1;

  self->notifications = g_hash_table_new (NULL, NULL);

  g_signal_connect (self->service, "handle-notify",
                    G_CALLBACK (on_handle_notify), self);
  g_signal_connect (self->service, "handle-close-notification",
                    G_CALLBACK (on_handle_close_notification), self);

  self->cancellable = g_cancellable_new ();
}

/**
 * trp_guidance_turn_by_turn_service_new:
 * @connection: the #GDBusConnection to use to publish routes
 *
 * Create a new #TrpGuidanceTurnByTurnService. This service won't expose anything
 * on D-Bus until g_async_initable_init_async() has been called.
 *
 * Returns: (transfer full): a new #TrpGuidanceTurnByTurnService
 * Since: 0.1612.0
 */
TrpGuidanceTurnByTurnService *
trp_guidance_turn_by_turn_service_new (GDBusConnection *connection)
{
  return g_object_new (TRP_GUIDANCE_TYPE_TURN_BY_TURN_SERVICE, "connection", connection, NULL);
}

static void
fail_init_tasks (TrpGuidanceTurnByTurnService *self,
                 GError *error)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_error (task, g_error_copy (error));
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
complete_init_tasks (TrpGuidanceTurnByTurnService *self)
{
  GList *l;

  for (l = self->init_tasks; l != NULL; l = g_list_next (l))
    {
      GTask *task = l->data;

      g_task_return_boolean (task, TRUE);
    }

  g_list_free_full (self->init_tasks, g_object_unref);
  self->init_tasks = NULL;
}

static void
bus_name_acquired_cb (GDBusConnection *conn,
                      const gchar *name,
                      gpointer user_data)
{
  TrpGuidanceTurnByTurnService *self = user_data;

  self->registered = TRUE;
  complete_init_tasks (self);
}

static void
bus_name_lost_cb (GDBusConnection *conn,
                  const gchar *name,
                  gpointer user_data)
{
  TrpGuidanceTurnByTurnService *self = user_data;

  g_clear_error (&self->invalidated_error);
  self->invalidated_error = g_error_new (G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER, "Lost bus name '%s'", name);

  fail_init_tasks (self, self->invalidated_error);
  g_signal_emit (self, signals[SIG_INVALIDATED], 0, self->invalidated_error);
}

static void
trp_guidance_turn_by_turn_service_init_async (GAsyncInitable *initable,
                                              int io_priority,
                                              GCancellable *cancellable,
                                              GAsyncReadyCallback callback,
                                              gpointer user_data)
{
  TrpGuidanceTurnByTurnService *self = TRP_GUIDANCE_TURN_BY_TURN_SERVICE (initable);
  g_autoptr (GTask) task = NULL;
  gboolean start_init;
  GError *error = NULL;

  g_return_if_fail (self->conn != NULL);

  task = g_task_new (initable, cancellable, callback, user_data);

  if (self->registered)
    {
      g_task_return_boolean (task, TRUE);
      return;
    }

  start_init = (self->init_tasks == NULL);
  self->init_tasks = g_list_append (self->init_tasks, g_object_ref (task));

  if (!start_init)
    return;

  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (self->service),
                                         self->conn, TBT_PATH, &error))
    {
      fail_init_tasks (self, error);
      return;
    }

  self->own_name_id = g_bus_own_name_on_connection (self->conn,
                                                    TBT_BUS_NAME,
                                                    G_BUS_NAME_OWNER_FLAGS_NONE,
                                                    bus_name_acquired_cb,
                                                    bus_name_lost_cb, self,
                                                    NULL);
}

static gboolean
trp_guidance_turn_by_turn_service_init_finish (GAsyncInitable *initable,
                                               GAsyncResult *result,
                                               GError **error)
{
  g_return_val_if_fail (g_task_is_valid (result, initable), FALSE);
  g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

  return g_task_propagate_boolean (G_TASK (result), error);
}

static void
async_initable_iface_init (gpointer g_iface,
                           gpointer data)
{
  GAsyncInitableIface *iface = g_iface;

  iface->init_async = trp_guidance_turn_by_turn_service_init_async;
  iface->init_finish = trp_guidance_turn_by_turn_service_init_finish;
}
