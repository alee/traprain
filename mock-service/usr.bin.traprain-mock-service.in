# vim:syntax=apparmor
# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include <tunables/global>

# (attach_disconnected) is needed to resolve paths from within the mount
# namespace caused by the ProtectSystem (and other) .service keys.
@bindir@/traprain-mock-service (attach_disconnected) {
  #include <abstractions/chaiwala-base>
  #include <abstractions/dbus-strict>

  # Well-known name
  dbus send
        bus=system
        path=/org/freedesktop/DBus
        interface=org.freedesktop.DBus
        member={RequestName,ReleaseName}
        peer=(name=org.freedesktop.DBus),
  dbus bind bus=system name="org.apertis.Traprain1.Mock",

  # Navigation well-known name
  dbus send
        bus=system
        path=/org/freedesktop/DBus
        interface=org.freedesktop.DBus
        member={RequestName,ReleaseName}
        peer=(name=org.freedesktop.DBus),
  dbus bind bus=system name="org.apertis.Navigation1",

  # Navigation Progress well-known name
  dbus send
        bus=system
        path=/org/freedesktop/DBus
        interface=org.freedesktop.DBus
        member={RequestName,ReleaseName}
        peer=(name=org.freedesktop.DBus),
  dbus bind bus=system name="org.apertis.NavigationGuidance1.Progress",

  @bindir@/traprain-mock-service mr,

  # Getting credentials from the daemon for authorisation checks on its AppArmor label
  dbus send
       bus=system
       path=/org/freedesktop/DBus
       interface=org.freedesktop.DBus
       member="GetConnectionCredentials"
       peer=(name=org.freedesktop.DBus),

  # systemd startup notification
  /run/systemd/notify w,

  # Approved clients
  dbus (send, receive) bus=system peer=(label=@installed_testdir@/test-mock-service),
  dbus (send, receive) bus=system peer=(label=@bindir@/traprain-guidance-ui),
  dbus (send, receive) bus=system peer=(label=@bindir@/traprain-update-progress),

  # Allow tests to ping us
  dbus (send, receive) bus=system interface=org.freedesktop.DBus.Peer member=Ping peer=(label=*),
}
