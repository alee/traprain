/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <gio/gio.h>
#include <glib.h>

#include "traprain-client/navigation-guidance-progress.h"
#include "traprain-service/navigation-guidance-progress.h"

#include "dbus/org.apertis.NavigationGuidance1.Progress.h"

#define PROGRESS_PATH "/org/apertis/NavigationGuidance1/Progress"
#define PROGRESS_BUS_NAME "org.apertis.NavigationGuidance1.Progress"

typedef struct
{
  GTestDBus *dbus;       /* owned */
  GDBusConnection *conn; /* owned */
  GMainLoop *loop;       /* owned */
  GError *error;         /* owned */
  gboolean init_result;

  TrpServiceNavigationGuidanceProgress *service; /* owned */
  TrpClientNavigationGuidanceProgress *client;   /* owned */
  TrpNavigationGuidance1Progress *proxy;         /* owned */
} Test;

static void
setup (Test *test,
       gconstpointer unused)
{
  const gchar *addr;

  test->dbus = g_test_dbus_new (G_TEST_DBUS_NONE);
  g_test_dbus_up (test->dbus);

  addr = g_test_dbus_get_bus_address (test->dbus);
  g_assert (addr != NULL);

  test->conn = g_dbus_connection_new_for_address_sync (addr,
                                                       G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT |
                                                           G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
                                                       NULL, NULL, &test->error);

  test->service = trp_service_navigation_guidance_progress_new (test->conn);
  g_assert_true (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (test->service));

  test->loop = g_main_loop_new (NULL, FALSE);
  test->error = NULL;
}

static void
teardown (Test *test,
          gconstpointer unused)
{
  g_clear_pointer (&test->loop, g_main_loop_unref);
  g_clear_pointer (&test->error, g_error_free);

  g_clear_object (&test->service);
  g_clear_object (&test->client);
  g_clear_object (&test->proxy);

  g_test_dbus_down (test->dbus);
  g_clear_object (&test->dbus);
  g_clear_object (&test->conn);
}

static void
init_cb (GObject *source,
         GAsyncResult *res,
         gpointer user_data)
{
  Test *test = user_data;

  test->init_result = g_async_initable_init_finish (G_ASYNC_INITABLE (source), res, &test->error);
  g_main_loop_quit (test->loop);
}

static void
init_service (Test *test)
{
  g_async_initable_init_async (G_ASYNC_INITABLE (test->service), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_true (test->init_result);
  g_assert_no_error (test->error);
}

static void
proxy_cb (GObject *source,
          GAsyncResult *res,
          gpointer user_data)
{
  Test *test = user_data;

  test->proxy = trp_navigation_guidance1_progress_proxy_new_finish (res, &test->error);

  g_main_loop_quit (test->loop);
}

static void
create_proxy (Test *test)
{
  g_clear_object (&test->proxy);

  trp_navigation_guidance1_progress_proxy_new (test->conn, G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START,
                                               PROGRESS_BUS_NAME, PROGRESS_PATH, NULL, proxy_cb, test);

  g_main_loop_run (test->loop);
}

/* Init a TrpServiceNavigationGuidanceProgress */
static void
test_service_init (Test *test,
                   gconstpointer unused)
{
  g_autofree gchar *owner = NULL;

  /* Service is not yet initialized, so not yet exposed on the bus */
  create_proxy (test);
  g_assert_no_error (test->error);
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (test->proxy));
  g_assert (owner == NULL);

  init_service (test);

  /* Check that the proxy is owned now that the service is initialized */
  create_proxy (test);
  g_assert_no_error (test->error);
  owner = g_dbus_proxy_get_name_owner (G_DBUS_PROXY (test->proxy));
  g_assert (owner != NULL);
}

static void
invalidated_cb (TrpServiceNavigationGuidanceProgress *service,
                const GError *error,
                Test *test)
{
  g_assert_true (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (service));
  g_assert (error != NULL);

  g_clear_error (&test->error);
  test->error = g_error_copy (error);
  g_main_loop_quit (test->loop);
}

/* Invalidate a TrpServiceNavigationGuidanceProgress */
static void
test_service_invalidated (Test *test,
                          gconstpointer unused)
{
  init_service (test);

  g_assert_no_error (test->error);
  g_signal_connect (test->service, "invalidated", G_CALLBACK (invalidated_cb),
                    test);

  g_dbus_connection_close (test->conn, NULL, NULL, NULL);
  g_main_loop_run (test->loop);

  g_assert_error (test->error, G_DBUS_ERROR, G_DBUS_ERROR_NAME_HAS_NO_OWNER);
}

static void
notify_cb (GObject *object,
           GParamSpec *spec,
           Test *test)
{
  g_main_loop_quit (test->loop);
}

/* Test trp_service_navigation_set_start_time(_now) API */
static void
test_service_start_time (Test *test,
                         gconstpointer unused)
{
  GDateTime *dt, *now;

  init_service (test);
  create_proxy (test);
  g_assert_no_error (test->error);

  /* 0 is the default start time */
  g_assert_cmpuint (trp_navigation_guidance1_progress_get_start_time (test->proxy), ==, 0);

  g_signal_connect (test->proxy, "notify::start-time", G_CALLBACK (notify_cb), test);

  /* Set an arbitrary start time */
  dt = g_date_time_new_from_unix_utc (123456789);
  g_assert_true (trp_service_navigation_guidance_progress_set_start_time (test->service, dt, &test->error));
  g_date_time_unref (dt);
  g_assert_no_error (test->error);
  g_main_loop_run (test->loop);
  g_assert_cmpuint (trp_navigation_guidance1_progress_get_start_time (test->proxy), ==, 123456789);

  /* Unset the start time */
  g_assert_true (trp_service_navigation_guidance_progress_set_start_time (test->service, NULL, &test->error));
  g_assert_no_error (test->error);
  g_main_loop_run (test->loop);
  g_assert_cmpuint (trp_navigation_guidance1_progress_get_start_time (test->proxy), ==, 0);

  /* Set the start time to now */
  g_assert_true (trp_service_navigation_guidance_progress_set_start_time_now (test->service, &test->error));
  g_assert_no_error (test->error);
  g_main_loop_run (test->loop);
  dt = g_date_time_new_from_unix_utc (trp_navigation_guidance1_progress_get_start_time (test->proxy));
  now = g_date_time_new_now_utc ();
  /* assuming it didn't take more than one minute between the call to set_start_time_now() and this check */
  g_assert_cmpint (g_date_time_difference (now, dt), <, G_TIME_SPAN_MINUTE);
  g_date_time_unref (dt);
  g_date_time_unref (now);

  /* Create a new TrpServiceNavigationGuidanceProgress and set the start time BEFORE initializing it */
  g_clear_object (&test->service);
  g_clear_object (&test->proxy);

  test->service = trp_service_navigation_guidance_progress_new (test->conn);
  g_assert_true (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (test->service));

  dt = g_date_time_new_from_unix_utc (987654321);
  g_assert_true (trp_service_navigation_guidance_progress_set_start_time (test->service, dt, &test->error));
  g_date_time_unref (dt);

  init_service (test);
  create_proxy (test);
  g_assert_no_error (test->error);

  g_assert_cmpuint (trp_navigation_guidance1_progress_get_start_time (test->proxy), ==, 987654321);
}

/* Test trp_service_navigation_guidance_progress_set_estimated_end_time API */
static void
test_service_estimated_end_time (Test *test,
                                 gconstpointer unused)
{
  GDateTime *dt;

  init_service (test);

  create_proxy (test);
  g_assert_no_error (test->error);

  /* 0 is the default estimated end time */
  g_assert_cmpuint (trp_navigation_guidance1_progress_get_estimated_end_time (test->proxy), ==, 0);

  g_signal_connect (test->proxy, "notify::estimated-end-time", G_CALLBACK (notify_cb), test);

  /* Set an arbitrary estimated end time */
  dt = g_date_time_new_from_unix_utc (123456789);
  g_assert_true (trp_service_navigation_guidance_progress_set_estimated_end_time (test->service, dt, &test->error));
  g_date_time_unref (dt);
  g_assert_no_error (test->error);
  g_main_loop_run (test->loop);
  g_assert_cmpuint (trp_navigation_guidance1_progress_get_estimated_end_time (test->proxy), ==, 123456789);

  /* Unset the start time */
  g_assert_true (trp_service_navigation_guidance_progress_set_estimated_end_time (test->service, NULL, &test->error));
  g_assert_no_error (test->error);
  g_main_loop_run (test->loop);
  g_assert_cmpuint (trp_navigation_guidance1_progress_get_estimated_end_time (test->proxy), ==, 0);

  /* Create a new TrpServiceNavigationGuidanceProgress and set the end time BEFORE initializing it */
  g_clear_object (&test->service);
  g_clear_object (&test->proxy);

  test->service = trp_service_navigation_guidance_progress_new (test->conn);
  g_assert_true (TRP_SERVICE_IS_NAVIGATION_GUIDANCE_PROGRESS (test->service));

  dt = g_date_time_new_from_unix_utc (987654321);
  g_assert_true (trp_service_navigation_guidance_progress_set_estimated_end_time (test->service, dt, &test->error));
  g_date_time_unref (dt);

  init_service (test);
  create_proxy (test);
  g_assert_no_error (test->error);

  g_assert_cmpuint (trp_navigation_guidance1_progress_get_estimated_end_time (test->proxy), ==, 987654321);
}

static void
create_and_init_client (Test *test)
{
  g_clear_object (&test->client);

  test->client = trp_client_navigation_guidance_progress_new (test->conn);

  g_async_initable_init_async (G_ASYNC_INITABLE (test->client), G_PRIORITY_DEFAULT, NULL, init_cb, test);
  g_main_loop_run (test->loop);
  g_assert_true (test->init_result);
  g_assert_no_error (test->error);
}

/* Test retrieving TrpClientNavigationGuidanceProgress properties */
static void
test_client_properties (Test *test,
                        gconstpointer unused)
{
  GDateTime *dt;

  /* Set start-time and estimated-end-time on the service */
  dt = g_date_time_new_from_unix_utc (123456789);
  g_assert_true (trp_service_navigation_guidance_progress_set_start_time (test->service, dt, &test->error));
  g_date_time_unref (dt);

  dt = g_date_time_new_from_unix_utc (987654321);
  g_assert_true (trp_service_navigation_guidance_progress_set_estimated_end_time (test->service, dt, &test->error));
  g_date_time_unref (dt);

  /* Publish info on the bus and create a client */
  init_service (test);
  create_and_init_client (test);

  dt = trp_client_navigation_guidance_progress_get_start_time (test->client);
  g_assert (dt != NULL);
  g_assert_cmpuint (g_date_time_to_unix (dt), ==, 123456789);

  g_object_get (test->client, "start-time", &dt, NULL);
  g_assert (dt != NULL);
  g_assert_cmpuint (g_date_time_to_unix (dt), ==, 123456789);
  g_date_time_unref (dt);

  dt = trp_client_navigation_guidance_progress_get_estimated_end_time (test->client);
  g_assert (dt != NULL);
  g_assert_cmpuint (g_date_time_to_unix (dt), ==, 987654321);

  g_object_get (test->client, "estimated-end-time", &dt, NULL);
  g_assert (dt != NULL);
  g_assert_cmpuint (g_date_time_to_unix (dt), ==, 987654321);
  g_date_time_unref (dt);

  /* Update the start time */
  g_signal_connect (test->client, "notify::start-time", G_CALLBACK (notify_cb), test);

  dt = g_date_time_new_from_unix_utc (666);
  g_assert_true (trp_service_navigation_guidance_progress_set_start_time (test->service, dt, &test->error));
  g_date_time_unref (dt);

  g_main_loop_run (test->loop);
  dt = trp_client_navigation_guidance_progress_get_start_time (test->client);
  g_assert (dt != NULL);
  g_assert_cmpuint (g_date_time_to_unix (dt), ==, 666);

  /* Update the estimated end time */
  g_signal_connect (test->client, "notify::estimated-end-time", G_CALLBACK (notify_cb), test);

  dt = g_date_time_new_from_unix_utc (777);
  g_assert_true (trp_service_navigation_guidance_progress_set_estimated_end_time (test->service, dt, &test->error));
  g_date_time_unref (dt);

  g_main_loop_run (test->loop);
  dt = trp_client_navigation_guidance_progress_get_estimated_end_time (test->client);
  g_assert (dt != NULL);
  g_assert_cmpuint (g_date_time_to_unix (dt), ==, 777);
}

int
main (int argc,
      char **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add ("/navigation-guidance-progress/service/init", Test, NULL,
              setup, test_service_init, teardown);
  g_test_add ("/navigation-guidance-progress/service/invalidated", Test, NULL,
              setup, test_service_invalidated, teardown);
  g_test_add ("/navigation-guidance-progress/service/start-time", Test, NULL,
              setup, test_service_start_time, teardown);
  g_test_add ("/navigation-guidance-progress/service/estimated-end-time", Test, NULL,
              setup, test_service_estimated_end_time, teardown);
  g_test_add ("/navigation-guidance-progress/client/properties", Test, NULL,
              setup, test_client_properties, teardown);

  return g_test_run ();
}
