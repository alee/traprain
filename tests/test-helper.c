/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "test-helper.h"

#include "traprain-common/languages-map-internal.h"

void
build_first_route (TrpRoute *route)
{
  guint segment;

  trp_route_add_title (route, "fr_FR", FST_ROUTE_TITLE_FR);
  trp_route_add_title (route, "en_US", FST_ROUTE_TITLE_EN);
  segment = trp_route_add_segment (route, FST_ROUTE_FST_SEG_LAT, FST_ROUTE_FST_SEG_LON);
  trp_route_add_segment_description (route, segment, "en_US", FST_ROUTE_FST_SEG_DESC_EN);
  trp_route_add_segment_description (route, segment, "fr_FR", FST_ROUTE_FST_SEG_DESC_FR);
  segment = trp_route_add_segment (route, FST_ROUTE_SND_SEG_LAT, FST_ROUTE_SND_SEG_LON);
  trp_route_add_segment_description (route, segment, "en_US", FST_ROUTE_SND_SEG_DESC_EN);
}

TrpRoute *
create_first_service_route (TrpServiceNavigation *nav)
{
  TrpRoute *route;

  route = trp_service_navigation_create_route (nav, FST_ROUTE_DISTANCE, FST_ROUTE_TIME);
  build_first_route (route);

  return route;
}

void
build_second_route (TrpRoute *route)
{
  guint segment;

  trp_route_add_title (route, "fr_FR", SND_ROUTE_TITLE_FR);
  trp_route_add_title (route, "en_US", SND_ROUTE_TITLE_EN);
  segment = trp_route_add_segment (route, SND_ROUTE_FST_SEG_LAT, SND_ROUTE_FST_SEG_LON);
  trp_route_add_segment_description (route, segment, "fr_FR", SND_ROUTE_FST_SEG_DESC_FR);
  trp_route_add_segment (route, SND_ROUTE_SND_SEG_LAT, SND_ROUTE_SND_SEG_LON);
}

TrpRoute *
create_second_service_route (TrpServiceNavigation *nav)
{
  TrpRoute *route;

  route = trp_service_navigation_create_route (nav, SND_ROUTE_DISTANCE, SND_ROUTE_TIME);
  build_second_route (route);

  return route;
}

void
build_third_route (TrpRoute *route)
{
  guint segment;

  trp_route_add_title (route, "en_US", THD_ROUTE_TITLE_EN);
  segment = trp_route_add_segment (route, THD_ROUTE_FST_SEG_LAT, THD_ROUTE_FST_SEG_LON);
  trp_route_add_segment_description (route, segment, "fr_FR", THD_ROUTE_FST_SEG_DESC_FR);
  trp_route_add_segment (route, THD_ROUTE_SND_SEG_LAT, THD_ROUTE_SND_SEG_LON);
}

TrpRoute *
create_third_service_route (TrpServiceNavigation *nav)
{
  TrpRoute *route;

  route = trp_service_navigation_create_route (nav, THD_ROUTE_DISTANCE, THD_ROUTE_TIME);
  build_third_route (route);

  return route;
}

static void
check_client_route (TrpClientRoute *route,
                    guint total_distance,
                    guint total_time,
                    const gchar *title_en,
                    const gchar *title_fr,
                    guint n_segments)
{
  TrpLanguagesMap *titles;
  guint nb_title = 0;

  g_assert (TRP_CLIENT_IS_ROUTE (route));

  g_assert_cmpuint (trp_client_route_get_total_distance (route), ==, total_distance);
  g_assert_cmpuint (trp_client_route_get_total_time (route), ==, total_time);
  if (title_en != NULL)
    {
      g_assert_cmpstr (trp_client_route_get_title (route, "en_US"), ==, title_en);
      nb_title++;
    }
  if (title_fr != NULL)
    {
      g_assert_cmpstr (trp_client_route_get_title (route, "fr_FR"), ==, title_fr);
      nb_title++;
    }

  titles = trp_client_route_get_titles (route);
  g_assert_cmpuint (titles->len, ==, nb_title);
  if (title_en != NULL)
    g_assert_cmpstr (trp_languages_map_lookup (titles, "en_US"), ==, title_en);
  if (title_fr != NULL)
    g_assert_cmpstr (trp_languages_map_lookup (titles, "fr_FR"), ==, title_fr);

  g_assert_cmpuint (trp_client_route_get_n_segments (route), ==, n_segments);
}

void
check_first_client_route (TrpClientRoute *route)
{
  gdouble lat, lon;
  TrpLanguagesMap *desc;
  const gchar *d;
  const gchar *lang, *str;

  check_client_route (route, FST_ROUTE_DISTANCE, FST_ROUTE_TIME, FST_ROUTE_TITLE_EN, FST_ROUTE_TITLE_FR, 2);

  trp_client_route_get_segment (route, 0, &lat, &lon, &desc);
  g_assert_cmpfloat (lat, ==, FST_ROUTE_FST_SEG_LAT);
  g_assert_cmpfloat (lon, ==, FST_ROUTE_FST_SEG_LON);
  g_assert_cmpuint (desc->len, ==, 2);
  g_assert_cmpstr (trp_languages_map_lookup (desc, "en_US"), ==, FST_ROUTE_FST_SEG_DESC_EN);
  g_assert_cmpstr (trp_languages_map_lookup (desc, "fr_FR"), ==, FST_ROUTE_FST_SEG_DESC_FR);
  trp_client_route_get_segment_in_language (route, 0, "en_US", NULL, NULL, &d);
  g_assert_cmpstr (d, ==, FST_ROUTE_FST_SEG_DESC_EN);
  trp_client_route_get_segment_in_language (route, 0, "fr_FR", NULL, NULL, &d);
  g_assert_cmpstr (d, ==, FST_ROUTE_FST_SEG_DESC_FR);

  g_assert (trp_languages_map_get (desc, 0, &lang, &str));
  g_assert_cmpstr (lang, ==, "en_US");
  g_assert_cmpstr (str, ==, FST_ROUTE_FST_SEG_DESC_EN);
  g_assert (trp_languages_map_get (desc, 1, &lang, &str));
  g_assert_cmpstr (lang, ==, "fr_FR");
  g_assert_cmpstr (str, ==, FST_ROUTE_FST_SEG_DESC_FR);
  g_assert (!trp_languages_map_get (desc, 2, &lang, &str));
  g_assert (lang == NULL);
  g_assert (str == NULL);

  trp_client_route_get_segment (route, 1, &lat, &lon, &desc);
  g_assert_cmpfloat (lat, ==, FST_ROUTE_SND_SEG_LAT);
  g_assert_cmpfloat (lon, ==, FST_ROUTE_SND_SEG_LON);
  g_assert_cmpuint (desc->len, ==, 1);
  g_assert_cmpstr (trp_languages_map_lookup (desc, "en_US"), ==, FST_ROUTE_SND_SEG_DESC_EN);
  trp_client_route_get_segment_in_language (route, 1, "en_US", NULL, NULL, &d);
  g_assert_cmpstr (d, ==, FST_ROUTE_SND_SEG_DESC_EN);
}

void
check_second_client_route (TrpClientRoute *route)
{
  gdouble lat, lon;
  TrpLanguagesMap *desc;
  const gchar *d;

  check_client_route (route, SND_ROUTE_DISTANCE, SND_ROUTE_TIME, SND_ROUTE_TITLE_EN, SND_ROUTE_TITLE_FR, 2);

  trp_client_route_get_segment (route, 0, &lat, &lon, &desc);
  g_assert_cmpfloat (lat, ==, SND_ROUTE_FST_SEG_LAT);
  g_assert_cmpfloat (lon, ==, SND_ROUTE_FST_SEG_LON);
  g_assert_cmpuint (desc->len, ==, 1);
  g_assert_cmpstr (trp_languages_map_lookup (desc, "fr_FR"), ==, SND_ROUTE_FST_SEG_DESC_FR);
  trp_client_route_get_segment_in_language (route, 0, "fr_FR", NULL, NULL, &d);
  g_assert_cmpstr (d, ==, SND_ROUTE_FST_SEG_DESC_FR);

  trp_client_route_get_segment (route, 1, &lat, &lon, &desc);
  g_assert_cmpfloat (lat, ==, SND_ROUTE_SND_SEG_LAT);
  g_assert_cmpfloat (lon, ==, SND_ROUTE_SND_SEG_LON);
  g_assert_cmpuint (desc->len, ==, 0);
  g_assert (trp_languages_map_lookup (desc, "en_US") == NULL);
  trp_client_route_get_segment_in_language (route, 1, "en_US", NULL, NULL, &d);
  g_assert (d == NULL);
}

void
check_third_client_route (TrpClientRoute *route)
{
  gdouble lat, lon;
  TrpLanguagesMap *desc;
  const gchar *d;

  check_client_route (route, THD_ROUTE_DISTANCE, THD_ROUTE_TIME, THD_ROUTE_TITLE_EN, NULL, 2);

  trp_client_route_get_segment (route, 0, &lat, &lon, &desc);
  g_assert_cmpfloat (lat, ==, THD_ROUTE_FST_SEG_LAT);
  g_assert_cmpfloat (lon, ==, THD_ROUTE_FST_SEG_LON);
  g_assert_cmpuint (desc->len, ==, 1);
  g_assert_cmpstr (trp_languages_map_lookup (desc, "fr_FR"), ==, THD_ROUTE_FST_SEG_DESC_FR);
  trp_client_route_get_segment_in_language (route, 0, "fr_FR", NULL, NULL, &d);
  g_assert_cmpstr (d, ==, THD_ROUTE_FST_SEG_DESC_FR);

  trp_client_route_get_segment (route, 1, &lat, &lon, &desc);
  g_assert_cmpfloat (lat, ==, THD_ROUTE_SND_SEG_LAT);
  g_assert_cmpfloat (lon, ==, THD_ROUTE_SND_SEG_LON);
  g_assert_cmpuint (desc->len, ==, 0);
  g_assert (trp_languages_map_lookup (desc, "en_US") == NULL);
  trp_client_route_get_segment_in_language (route, 1, "en_US", NULL, NULL, &d);
  g_assert (d == NULL);
}
