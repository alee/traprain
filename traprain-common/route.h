/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_COMMON_ROUTE_H__
#define __TRAPRAIN_COMMON_ROUTE_H__

#include <glib-object.h>

G_BEGIN_DECLS

#define TRP_TYPE_ROUTE (trp_route_get_type ())
/* TrpRoute can only be subclassed internally so don't use
 * G_DECLARE_DERIVABLE_TYPE() */
G_DECLARE_FINAL_TYPE (TrpRoute, trp_route, TRP, ROUTE, GObject)

void trp_route_add_title (TrpRoute *route,
                          const gchar *language,
                          const gchar *title);

guint trp_route_add_segment (TrpRoute *route,
                             gdouble latitude,
                             gdouble longitude);
void trp_route_add_segment_description (TrpRoute *route,
                                        guint index,
                                        const gchar *language,
                                        const gchar *description);

G_END_DECLS

#endif /* __TRAPRAIN_COMMON_ROUTE_H__ */
