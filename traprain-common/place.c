/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "traprain-common/place.h"

#include <math.h>
#include <string.h>

#include <gio/gio.h>

#include "traprain-common/enumtypes.h"
#include "traprain-common/messages-internal.h"
#include "traprain-common/uri-internal.h"

/**
 * SECTION: traprain-common/place.h
 * @title: TrpPlace
 * @short_description: Object representing a place
 *
 * #TrpPlace represents a place, either in terms of a physical
 * geographic location (latitude, longitude and optionally altitude),
 * a textual description such as an address, or a combination of the two.
 *
 * Since: 0.1612.0
 */

/* A non-inline version of the function-like macro, for introspection */
gboolean (trp_place_is_latitude) (gdouble d)
{
  return trp_place_is_latitude (d);
}

/* A non-inline version of the function-like macro, for introspection */
gboolean (trp_place_is_longitude) (gdouble d)
{
  return trp_place_is_longitude (d);
}

/* A non-inline version of the function-like macro, for introspection */
gboolean (trp_place_is_altitude) (gdouble d)
{
  return trp_place_is_altitude (d);
}

/* A non-inline version of the function-like macro, for introspection */
gboolean (trp_place_is_uncertainty) (gdouble d)
{
  return trp_place_is_uncertainty (d);
}

/**
 * trp_place_is_country_code:
 * @country: a potential country code
 *
 * Return whether @country has the syntax of an ISO 3166-1 alpha-2 country
 * code. This function does not check whether @country is currently assigned
 * to a real country; reserved and special-purpose codes such as `ZZ`
 * are also accepted.
 *
 * Since: 0.1612.0
 */
gboolean
trp_place_is_country_code (const gchar *country)
{
  return (country != NULL &&
          g_ascii_isalpha (country[0]) &&
          g_ascii_isalpha (country[1]) &&
          country[2] == '\0');
}

GQuark
trp_place_error_quark (void)
{
  static gsize id = 0;

  if (g_once_init_enter (&id))
    {
      g_once_init_leave (&id,
                         g_quark_from_static_string ("trp-place-error-quark"));
    }

  return (GQuark) id;
}

/*
 * TrpPlaceDetails:
 *
 * Details used internally by both #TrpPlace and #TrpPlaceBuilder.
 *
 * In a #TrpPlaceBuilder, @longitude is not necessarily normalized (for example
 * it can differ from zero even if @latitude is at a pole). In a #TrpPlace,
 * it is normalized.
 *
 * Since: 0.1612.0
 */
typedef struct
{
  /*<private>*/
  gchar *location_string;
  gchar *postal_code;
  gchar *country;
  gchar *region;
  gchar *locality;
  gchar *area;
  gchar *street;
  gchar *building;
  gchar *formatted_address;
  gdouble latitude;
  gdouble longitude;
  gdouble altitude;
  gdouble uncertainty;
} TrpPlaceDetails;

/*
 * no_details:
 *
 * An empty set of details.
 *
 * Since: 0.1612.0
 */
static const TrpPlaceDetails no_details =
    {
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      NULL,
      TRP_PLACE_LATITUDE_UNKNOWN,
      TRP_PLACE_LONGITUDE_UNKNOWN,
      TRP_PLACE_ALTITUDE_UNKNOWN,
      TRP_PLACE_UNCERTAINTY_UNKNOWN,
    };

/**
 * TrpPlace:
 *
 * An object representing a place, either in terms of a physical
 * geographic location (latitude, longitude and optionally altitude),
 * a textual description such as an address, or a combination of the two.
 *
 * This is an immutable "value object". Use a #TrpPlaceBuilder to
 * build up a place from parameters.
 *
 * It is considered to be an error to construct a place object that does
 * not have at least either a #TrpPlace:location-string,
 * or a #TrpPlace:latitude and #TrpPlace:longitude pair.
 *
 * Since: 0.1612.0
 */
struct _TrpPlace
{
  /*<private>*/
  GObject parent;
  TrpPlaceDetails d;
};

/**
 * TrpPlaceClass:
 *
 * #TrpPlace cannot be subclassed.
 *
 * Since: 0.1612.0
 */
struct _TrpPlaceClass
{
  /*<private>*/
  GObjectClass parent;
};

/**
 * TrpPlaceBuilder:
 *
 * Mutable object used to build up a #TrpPlace from parameters.
 *
 * Since: 0.1612.0
 */
struct _TrpPlaceBuilder
{
  /*<private>*/
  gsize refcount;
  TrpPlaceDetails d;
};

G_DEFINE_BOXED_TYPE (TrpPlaceBuilder, trp_place_builder, trp_place_builder_ref, trp_place_builder_unref)

typedef enum {
  PROP_LOCATION_STRING = 1,
  PROP_LATITUDE,
  PROP_LONGITUDE,
  PROP_ALTITUDE,
  PROP_UNCERTAINTY,
  PROP_POSTAL_CODE,
  PROP_COUNTRY,
  PROP_REGION,
  PROP_LOCALITY,
  PROP_AREA,
  PROP_STREET,
  PROP_BUILDING,
  PROP_FORMATTED_ADDRESS,
} Property;

static GParamSpec *property_specs[PROP_FORMATTED_ADDRESS + 1] = { NULL };

G_DEFINE_TYPE (TrpPlace, trp_place, G_TYPE_OBJECT)

/*
 * trp_place_details_normalize:
 *
 * Remove spurious variation from the place details. For example, for
 * coordinates at a pole, the longitude is irrelevant and can be set to 0.
 *
 * Since: 0.1612.0
 */
static void
trp_place_details_normalize (TrpPlaceDetails *self)
{
  /* Deliberately avoiding use of == to compare doubles to avoid compiler
   * warnings. We know, and assume here, that TRP_PLACE_*_UNKNOWN are all
   * (very) negative. */

  /* If we are at the North Pole, longitude doesn't matter. */
  if (self->latitude >= 90.0)
    self->longitude = 0;

  /* If we are at the South Pole, longitude doesn't matter either. */
  if (self->latitude <= -90.0 && self->latitude > TRP_PLACE_LATITUDE_UNKNOWN)
    self->longitude = 0;

  /* -180 degree longitude wraps around to +180. */
  if (self->longitude <= -180.0 &&
      self->longitude > TRP_PLACE_LONGITUDE_UNKNOWN)
    self->longitude = 180.0;
}

static void
trp_place_details_clear (TrpPlaceDetails *self)
{
  g_free (self->location_string);
  g_free (self->postal_code);
  g_free (self->country);
  g_free (self->region);
  g_free (self->locality);
  g_free (self->area);
  g_free (self->street);
  g_free (self->building);
  g_free (self->formatted_address);
  *self = no_details;
}

static void
trp_place_details_copy_from (TrpPlaceDetails *dest,
                             TrpPlaceDetails *source)
{
  trp_place_details_normalize (source);
  *dest = *source;
  dest->location_string = g_strdup (source->location_string);
  dest->postal_code = g_strdup (source->postal_code);
  dest->country = g_strdup (source->country);
  dest->region = g_strdup (source->region);
  dest->locality = g_strdup (source->locality);
  dest->area = g_strdup (source->area);
  dest->street = g_strdup (source->street);
  dest->building = g_strdup (source->building);
  dest->formatted_address = g_strdup (source->formatted_address);
}

/**
 * trp_place_builder_new:
 * @place: (nullable): if not %NULL, initialize fields from this place
 *
 * Create a new builder object to build up a place from parameters.
 *
 * The new builder object has coordinate reference system
 * %TRP_COORDINATE_REFERENCE_SYSTEM_WGS84 and no other details.
 *
 * Returns: (transfer full): a new builder object
 *
 * Since: 0.1612.0
 */
TrpPlaceBuilder *
trp_place_builder_new (TrpPlace *place)
{
  g_autoptr (TrpPlaceBuilder) self = g_slice_new0 (TrpPlaceBuilder);

  g_return_val_if_fail (place == NULL || TRP_IS_PLACE (place), NULL);

  self->refcount = 1;
  self->d = no_details;

  if (place != NULL)
    trp_place_details_copy_from (&self->d, &place->d);

  return g_steal_pointer (&self);
}

/**
 * trp_place_builder_ref:
 * @self: a place builder
 *
 * Increase the reference count of @self and return it.
 *
 * Returns: (transfer full): @self
 *
 * Since: 0.1612.0
 */
TrpPlaceBuilder *
trp_place_builder_ref (TrpPlaceBuilder *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);

  self->refcount++;
  return self;
}

/**
 * trp_place_builder_unref:
 * @self: (transfer full): a place builder
 *
 * Decrease the reference count of @self, and free it if there are no more
 * references.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_unref (TrpPlaceBuilder *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);

  if (--self->refcount == 0)
    {
      trp_place_details_clear (&self->d);
      g_slice_free (TrpPlaceBuilder, self);
    }
}

/**
 * trp_place_builder_set_latitude:
 * @self: a place builder
 * @latitude: a latitude, or %TRP_PLACE_LATITUDE_UNKNOWN
 *
 * Set or unset the #TrpPlace:latitude of the place that will be returned by
 * trp_place_builder_end().
 *
 * If @latitude is not %TRP_PLACE_LATITUDE_UNKNOWN, then
 * trp_place_is_latitude() must return %TRUE for it.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_latitude (TrpPlaceBuilder *self,
                                gdouble latitude)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_place_is_latitude (latitude) ||
                    latitude <= TRP_PLACE_LATITUDE_UNKNOWN);

  self->d.latitude = latitude;
}

/**
 * trp_place_builder_set_longitude:
 * @self: a place builder
 * @longitude: a longitude, or %TRP_PLACE_LONGITUDE_UNKNOWN
 *
 * Set or unset the #TrpPlace:longitude of the place that will be returned by
 * trp_place_builder_end().
 *
 * If @longitude is not %TRP_PLACE_LONGITUDE_UNKNOWN, then
 * trp_place_is_longitude() must return %TRUE for it.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_longitude (TrpPlaceBuilder *self,
                                 gdouble longitude)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_place_is_longitude (longitude) ||
                    longitude <= TRP_PLACE_LONGITUDE_UNKNOWN);

  self->d.longitude = longitude;
}

/**
 * trp_place_builder_set_altitude:
 * @self: a place builder
 * @altitude: an altitude, or %TRP_PLACE_ALTITUDE_UNKNOWN
 *
 * Set or unset the #TrpPlace:altitude of the place that will be returned by
 * trp_place_builder_end().
 *
 * If @altitude is not %TRP_PLACE_ALTITUDE_UNKNOWN, then
 * trp_place_is_altitude() must return %TRUE for it.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_altitude (TrpPlaceBuilder *self,
                                gdouble altitude)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_place_is_altitude (altitude) ||
                    altitude <= TRP_PLACE_ALTITUDE_UNKNOWN);

  self->d.altitude = altitude;
}

/**
 * trp_place_builder_set_uncertainty:
 * @self: a place builder
 * @uncertainty: an uncertainty, or %TRP_PLACE_UNCERTAINTY_UNKNOWN
 *
 * Set or unset the #TrpPlace:uncertainty of the place that will be returned
 * by trp_place_builder_end().
 *
 * If @uncertainty is not %TRP_PLACE_UNCERTAINTY_UNKNOWN, then
 * trp_place_is_uncertainty() must return %TRUE for it.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_uncertainty (TrpPlaceBuilder *self,
                                   gdouble uncertainty)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_place_is_uncertainty (uncertainty) ||
                    uncertainty <= TRP_PLACE_UNCERTAINTY_UNKNOWN);

  self->d.uncertainty = uncertainty;
}

/**
 * trp_place_builder_set_location_string:
 * @self: a place builder
 * @location_string: (nullable): the human-readable location
 *
 * Set or unset the #TrpPlace:location-string of the place that will be
 * returned by trp_place_builder_end().
 *
 * @location_string must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_location_string (TrpPlaceBuilder *self,
                                       const gchar *location_string)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (location_string == NULL ||
                    g_utf8_validate (location_string, -1, NULL));

  if (g_strcmp0 (location_string, self->d.location_string) == 0)
    return;

  g_clear_pointer (&self->d.location_string, g_free);

  if (location_string != NULL && location_string[0] != '\0')
    self->d.location_string = g_strdup (location_string);
}

/**
 * trp_place_builder_set_postal_code:
 * @self: a place builder
 * @postal_code: (nullable): a postal code (postcode, zip code)
 *
 * Set or unset the #TrpPlace:postal-code of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_postal_code (TrpPlaceBuilder *self,
                                   const gchar *postal_code)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (postal_code == NULL ||
                    g_utf8_validate (postal_code, -1, NULL));

  if (g_strcmp0 (postal_code, self->d.postal_code) == 0)
    return;

  g_clear_pointer (&self->d.postal_code, g_free);

  if (postal_code != NULL && postal_code[0] != '\0')
    self->d.postal_code = g_strdup (postal_code);
}

/**
 * trp_place_builder_set_country_code:
 * @self: a place builder
 * @country: (nullable): an ISO 3166-1 alpha-2 country code
 *
 * Set or unset the #TrpPlace:country. If not %NULL, it must be
 * exactly 2 ASCII letters. It will be normalized to upper-case.
 *
 * This function does not check whether @country is currently assigned to
 * a real country; reserved and special-purpose codes such as `ZZ`
 * are also accepted.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_country_code (TrpPlaceBuilder *self,
                                    const gchar *country)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (country == NULL || trp_place_is_country_code (country));

  if (g_strcmp0 (country, self->d.country) == 0)
    return;

  g_clear_pointer (&self->d.country, g_free);

  if (country != NULL)
    self->d.country = g_ascii_strup (country, 2);
}

/**
 * trp_place_builder_set_region:
 * @self: a place builder
 * @region: (nullable): a province, state, county or other large region
 *  within a country
 *
 * Set or unset the #TrpPlace:region of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_region (TrpPlaceBuilder *self,
                              const gchar *region)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (region == NULL ||
                    g_utf8_validate (region, -1, NULL));

  if (g_strcmp0 (region, self->d.region) == 0)
    return;

  g_clear_pointer (&self->d.region, g_free);

  if (region != NULL && region[0] != '\0')
    self->d.region = g_strdup (region);
}

/**
 * trp_place_builder_set_locality:
 * @self: a place builder
 * @locality: (nullable): a town, city or other locality name
 *
 * Set or unset the #TrpPlace:locality of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_locality (TrpPlaceBuilder *self,
                                const gchar *locality)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (locality == NULL ||
                    g_utf8_validate (locality, -1, NULL));

  if (g_strcmp0 (locality, self->d.locality) == 0)
    return;

  g_clear_pointer (&self->d.locality, g_free);

  if (locality != NULL && locality[0] != '\0')
    self->d.locality = g_strdup (locality);
}

/**
 * trp_place_builder_set_area:
 * @self: a place builder
 * @area: (nullable): an area smaller than the #TrpPlace:locality
 *
 * Set or unset the #TrpPlace:area of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_area (TrpPlaceBuilder *self,
                            const gchar *area)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (area == NULL ||
                    g_utf8_validate (area, -1, NULL));

  if (g_strcmp0 (area, self->d.area) == 0)
    return;

  g_clear_pointer (&self->d.area, g_free);

  if (area != NULL && area[0] != '\0')
    self->d.area = g_strdup (area);
}

/**
 * trp_place_builder_set_street:
 * @self: a place builder
 * @street: (nullable): a street name
 *
 * Set or unset the #TrpPlace:street of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_street (TrpPlaceBuilder *self,
                              const gchar *street)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (street == NULL ||
                    g_utf8_validate (street, -1, NULL));

  if (g_strcmp0 (street, self->d.street) == 0)
    return;

  g_clear_pointer (&self->d.street, g_free);

  if (street != NULL && street[0] != '\0')
    self->d.street = g_strdup (street);
}

/**
 * trp_place_builder_set_building:
 * @self: a place builder
 * @building: (nullable): a house or building number or name
 *
 * Set or unset the #TrpPlace:building of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_building (TrpPlaceBuilder *self,
                                const gchar *building)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (building == NULL ||
                    g_utf8_validate (building, -1, NULL));

  if (g_strcmp0 (building, self->d.building) == 0)
    return;

  g_clear_pointer (&self->d.building, g_free);

  if (building != NULL && building[0] != '\0')
    self->d.building = g_strdup (building);
}

/**
 * trp_place_builder_set_formatted_address:
 * @self: a place builder
 * @address: (nullable): a complete, formatted version of the entire address
 *
 * Set or unset the #TrpPlace:formatted-address of the place that will be
 * returned by trp_place_builder_end(). It must be valid UTF-8.
 *
 * Since: 0.1612.0
 */
void
trp_place_builder_set_formatted_address (TrpPlaceBuilder *self,
                                         const gchar *address)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (address == NULL ||
                    g_utf8_validate (address, -1, NULL));

  if (g_strcmp0 (address, self->d.formatted_address) == 0)
    return;

  g_clear_pointer (&self->d.formatted_address, g_free);

  if (address != NULL && address[0] != '\0')
    self->d.formatted_address = g_strdup (address);
}

static gboolean
trp_place_builder_parse_geo_uri (TrpPlaceBuilder *self,
                                 const gchar *uri,
                                 TrpUriFlags flags,
                                 GError **error)
{
  const gchar *p;
  gboolean strict = ((flags & TRP_URI_FLAGS_STRICT) ? TRUE : FALSE);

  if (g_ascii_strncasecmp (uri, "geo:", 4) != 0)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "URI does not start with geo:");
      goto error;
    }

  p = uri + 4;

  if (*p == '/')
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "geo: URIs are not hierarchical");
      goto error;
    }

  if (!_trp_uri_consume_double (&p, &self->d.latitude, error))
    goto error;

  if (!trp_place_is_latitude (self->d.latitude))
    {
      if (strict)
        {
          g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_OUT_OF_RANGE,
                       "Latitude %.2f out of range -90.0 to +90.0",
                       self->d.latitude);
          goto error;
        }
      else if (isfinite (self->d.latitude))
        {
          self->d.latitude = CLAMP (self->d.latitude, -90.0, +90.0);
        }
      else
        {
          self->d.latitude = TRP_PLACE_LATITUDE_UNKNOWN;
        }
    }

  if (*p != ',')
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "Expected a comma before longitude at: %s", p);
      goto error;
    }

  p++;

  if (!_trp_uri_consume_double (&p, &self->d.longitude, error))
    goto error;

  if (!trp_place_is_longitude (self->d.longitude))
    {
      if (strict)
        {
          g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_OUT_OF_RANGE,
                       "Longitude %.2f out of range -180.0 to +180.0",
                       self->d.longitude);
          goto error;
        }
      else if (isfinite (self->d.longitude))
        {
          self->d.longitude = CLAMP (self->d.longitude, -180.0, +180.0);
        }
      else
        {
          self->d.longitude = TRP_PLACE_LONGITUDE_UNKNOWN;
        }
    }

  if (*p == ',')
    {
      p++;

      if (!_trp_uri_consume_double (&p, &self->d.altitude, error))
        goto error;

      if (!trp_place_is_altitude (self->d.altitude))
        {
          if (strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_OUT_OF_RANGE,
                           "Altitude %.2f not valid", self->d.altitude);
              goto error;
            }

          self->d.altitude = TRP_PLACE_ALTITUDE_UNKNOWN;
        }
    }

  while (*p == ';')
    {
      g_autofree gchar *key = NULL;
      g_autofree gchar *value = NULL;

      p++;

      /* Tolerate multiple ';' */
      if (!strict && *p == ';')
        continue;

      /* Tolerate trailing ';' */
      if (!strict && *p == '\0')
        return TRUE;

      if (!_trp_uri_consume_label_text (&p, &key, error))
        goto error;

      if (*p == '=')
        {
          p++;

          if (!_trp_uri_consume_parameter_value (&p, flags, &value, error))
            goto error;
        }
      /* else leave the value set to NULL */

      if (g_ascii_strcasecmp (key, "crs") == 0)
        {
          if (value == NULL)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for crs parameter");
              goto error;
            }

          if (g_ascii_strcasecmp (value, "wgs84") != 0)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_UNKNOWN_CRS,
                           "Unknown coordinate reference system \"%s\"",
                           value);
              goto error;
            }
        }
      else if (g_ascii_strcasecmp (key, "u") == 0)
        {
          gchar *endptr;

          if (value == NULL || *value == '\0')
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for uncertainty parameter");
              goto error;
            }

          self->d.uncertainty = g_ascii_strtod (value, &endptr);

          if (endptr == NULL || *endptr != '\0' ||
              !trp_place_is_uncertainty (self->d.uncertainty))
            {
              if (strict)
                {
                  g_set_error (error, TRP_PLACE_ERROR,
                               TRP_PLACE_ERROR_INVALID_URI,
                               "Unable to parse uncertainty \"%s\"", value);
                  goto error;
                }

              self->d.uncertainty = TRP_PLACE_UNCERTAINTY_UNKNOWN;
            }
        }
    }

  if (*p == '\0')
    {
      return TRUE;
    }

  if (!strict && (*p == '#' || *p == '?'))
    {
      /* As a non-RFC5870 extension, allow and ignore a query-string
       * (as used by Google Maps) or a fragment */
      return TRUE;
    }

  g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
               "Expected a semicolon or end of string at: %s", p);
error:
  trp_place_details_clear (&self->d);
  return FALSE;
}

static gboolean
trp_place_builder_parse_place_uri (TrpPlaceBuilder *self,
                                   const gchar *uri,
                                   TrpUriFlags flags,
                                   GError **error)
{
  const gchar *p;
  g_autofree gchar *location_string = NULL;
  gboolean strict = ((flags & TRP_URI_FLAGS_STRICT) ? TRUE : FALSE);

  if (g_ascii_strncasecmp (uri, "place:", 6) != 0)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "URI does not start with place:");
      goto error;
    }

  p = uri + 6;

  if (*p == '/')
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "place: URIs are not hierarchical");
      goto error;
    }

  /* The location string is syntactically the same as a parameter value */
  if (!_trp_uri_consume_parameter_value (&p, flags, &location_string, error))
    goto error;

  /* We don't tolerate this even in tolerant mode. */
  if (location_string == NULL || location_string[0] == '\0')
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "place: URIs require a non-empty location string");
      goto error;
    }

  trp_place_builder_set_location_string (self, location_string);

  while (*p == ';')
    {
      g_autofree gchar *key = NULL;
      g_autofree gchar *value = NULL;

      p++;

      /* Tolerate multiple ';' */
      if (!strict && *p == ';')
        continue;

      /* Tolerate trailing ';' */
      if (!strict && *p == '\0')
        return TRUE;

      if (!_trp_uri_consume_label_text (&p, &key, error))
        goto error;

      if (*p == '=')
        {
          p++;

          if (!_trp_uri_consume_parameter_value (&p, flags, &value, error))
            goto error;
        }
      /* else leave the value set to NULL */

      if (g_ascii_strcasecmp (key, "location") == 0)
        {
          if (value == NULL)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for location parameter");
              goto error;
            }

          if (!trp_place_builder_parse_geo_uri (self, value, flags, error))
            goto error;
        }
      else if (g_ascii_strcasecmp (key, "postal-code") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for postal-code parameter");
              goto error;
            }

          trp_place_builder_set_postal_code (self, value);
        }
      else if (g_ascii_strcasecmp (key, "country") == 0)
        {
          gsize i;

          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for country parameter");
              goto error;
            }

          if (value == NULL || value[0] == '\0')
            {
              trp_place_builder_set_country_code (self, NULL);
              continue;
            }

          for (i = 0; i < 2; i++)
            {
              if (!g_ascii_isalpha (value[i]))
                {
                  g_set_error (error, TRP_PLACE_ERROR,
                               TRP_PLACE_ERROR_INVALID_COUNTRY_CODE,
                               "Expected an ISO 3166-1 alpha-2 value for "
                               "country parameter, not \"%s\"",
                               value);
                  goto error;
                }
            }

          if (value[2] != '\0')
            {
              g_set_error (error, TRP_PLACE_ERROR,
                           TRP_PLACE_ERROR_INVALID_COUNTRY_CODE,
                           "Expected an ISO 3166-1 alpha-2 value for "
                           "country parameter, not \"%s\"",
                           value);
              goto error;
            }

          trp_place_builder_set_country_code (self, value);
        }
      else if (g_ascii_strcasecmp (key, "region") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for region parameter");
              goto error;
            }

          trp_place_builder_set_region (self, value);
        }
      else if (g_ascii_strcasecmp (key, "locality") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for locality parameter");
              goto error;
            }

          trp_place_builder_set_locality (self, value);
        }
      else if (g_ascii_strcasecmp (key, "area") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for area parameter");
              goto error;
            }

          trp_place_builder_set_area (self, value);
        }
      else if (g_ascii_strcasecmp (key, "street") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for street parameter");
              goto error;
            }

          trp_place_builder_set_street (self, value);
        }
      else if (g_ascii_strcasecmp (key, "building") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for building parameter");
              goto error;
            }

          trp_place_builder_set_building (self, value);
        }
      else if (g_ascii_strcasecmp (key, "formatted-address") == 0)
        {
          if (value == NULL && strict)
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for formatted-address parameter");
              goto error;
            }

          trp_place_builder_set_formatted_address (self, value);
        }
    }

  if (*p == '\0')
    {
      return TRUE;
    }

  g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
               "Expected a semicolon or end of string at: %s", p);
error:
  trp_place_details_clear (&self->d);
  return FALSE;
}

static gboolean
_trp_place_builder_parse_uri (TrpPlaceBuilder *self,
                              const gchar *uri,
                              TrpUriFlags flags,
                              GError **error)
{
  g_autofree gchar *scheme = g_uri_parse_scheme (uri);

  if (scheme == NULL)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "URI syntax error");
      goto error;
    }

  if (g_ascii_strcasecmp (scheme, "geo") == 0)
    return trp_place_builder_parse_geo_uri (self, uri, flags, error);

  if (g_ascii_strcasecmp (scheme, "place") == 0)
    return trp_place_builder_parse_place_uri (self, uri, flags, error);

  g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_UNKNOWN_SCHEME,
               "Unsupported URI scheme");
error:
  trp_place_details_clear (&self->d);
  return FALSE;
}

/**
 * trp_place_builder_copy:
 * @self: a place builder
 *
 * Copy the current place parameters into a new immutable #TrpPlace object,
 * without clearing the builder object.
 *
 * It is an error to call this method without first populating either the
 * location string, or the formatted address, or both the latitude and
 * the longitude.
 *
 * Returns: (transfer full): a new #TrpPlace
 *
 * Since: 0.1612.0
 */
TrpPlace *
trp_place_builder_copy (TrpPlaceBuilder *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);

  g_return_val_if_fail (((trp_place_is_latitude (self->d.latitude) &&
                          trp_place_is_longitude (self->d.longitude)) ||
                         self->d.location_string != NULL ||
                         self->d.formatted_address != NULL),
                        NULL);

  return g_object_new (TRP_TYPE_PLACE,
                       "altitude", self->d.altitude,
                       "area", self->d.area,
                       "building", self->d.building,
                       "country", self->d.country,
                       "formatted-address", self->d.formatted_address,
                       "latitude", self->d.latitude,
                       "locality", self->d.locality,
                       "location-string", self->d.location_string,
                       "longitude", self->d.longitude,
                       "postal-code", self->d.postal_code,
                       "region", self->d.region,
                       "street", self->d.street,
                       "uncertainty", self->d.uncertainty,
                       NULL);
}

/**
 * trp_place_builder_end:
 * @self: a place builder
 *
 * Convert the current place parameters into a new immutable #TrpPlace object,
 * and reset the place builder to an empty state.
 *
 * It is an error to call this method without first populating either the
 * location string, or the formatted address, or both the latitude and
 * the longitude.
 *
 * Returns: (transfer full): a new #TrpPlace
 *
 * Since: 0.1612.0
 */
TrpPlace *
trp_place_builder_end (TrpPlaceBuilder *self)
{
  g_autoptr (TrpPlace) place = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);

  place = trp_place_builder_copy (self);
  trp_place_details_clear (&self->d);
  return g_steal_pointer (&place);
}

static void
trp_place_init (TrpPlace *self)
{
  trp_place_details_clear (&self->d);
}

static void
trp_place_get_property (GObject *object,
                        guint prop_id,
                        GValue *value,
                        GParamSpec *pspec)
{
  TrpPlace *self = TRP_PLACE (object);

  switch ((Property) prop_id)
    {
    case PROP_ALTITUDE:
      g_value_set_double (value, self->d.altitude);
      break;

    case PROP_AREA:
      g_value_set_string (value, self->d.area);
      break;

    case PROP_COUNTRY:
      g_value_set_string (value, self->d.country);
      break;

    case PROP_FORMATTED_ADDRESS:
      g_value_set_string (value, self->d.formatted_address);
      break;

    case PROP_LATITUDE:
      g_value_set_double (value, self->d.latitude);
      break;

    case PROP_LOCATION_STRING:
      g_value_set_string (value, self->d.location_string);
      break;

    case PROP_LOCALITY:
      g_value_set_string (value, self->d.locality);
      break;

    case PROP_LONGITUDE:
      g_value_set_double (value, self->d.longitude);
      break;

    case PROP_POSTAL_CODE:
      g_value_set_string (value, self->d.postal_code);
      break;

    case PROP_REGION:
      g_value_set_string (value, self->d.region);
      break;

    case PROP_STREET:
      g_value_set_string (value, self->d.street);
      break;

    case PROP_BUILDING:
      g_value_set_string (value, self->d.building);
      break;

    case PROP_UNCERTAINTY:
      g_value_set_double (value, self->d.uncertainty);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_place_set_property (GObject *object,
                        guint prop_id,
                        const GValue *value,
                        GParamSpec *pspec)
{
  TrpPlace *self = TRP_PLACE (object);

  switch ((Property) prop_id)
    {
    case PROP_ALTITUDE:
      self->d.altitude = g_value_get_double (value);

      if (!trp_place_is_altitude (self->d.altitude))
        self->d.altitude = TRP_PLACE_ALTITUDE_UNKNOWN;

      break;

    case PROP_AREA:
      /* construct-only */
      g_assert (self->d.area == NULL);
      self->d.area = g_value_dup_string (value);

      if (self->d.area != NULL && self->d.area[0] == '\0')
        g_clear_pointer (&self->d.area, g_free);

      break;

    case PROP_COUNTRY:
      {
        g_autofree gchar *country = NULL;

        /* construct-only */
        g_assert (self->d.country == NULL);

        country = g_value_dup_string (value);
        g_return_if_fail (country == NULL ||
                          trp_place_is_country_code (country));
        self->d.country = g_steal_pointer (&country);
      }
      break;

    case PROP_FORMATTED_ADDRESS:
      /* construct-only */
      g_assert (self->d.formatted_address == NULL);
      self->d.formatted_address = g_value_dup_string (value);

      if (self->d.formatted_address != NULL &&
          self->d.formatted_address[0] == '\0')
        g_clear_pointer (&self->d.formatted_address, g_free);

      break;

    case PROP_LATITUDE:
      self->d.latitude = g_value_get_double (value);

      if (!trp_place_is_latitude (self->d.latitude))
        self->d.latitude = TRP_PLACE_LATITUDE_UNKNOWN;

      break;

    case PROP_LOCALITY:
      /* construct-only */
      g_assert (self->d.locality == NULL);
      self->d.locality = g_value_dup_string (value);

      if (self->d.locality != NULL && self->d.locality[0] == '\0')
        g_clear_pointer (&self->d.locality, g_free);

      break;

    case PROP_LOCATION_STRING:
      /* construct-only */
      g_assert (self->d.location_string == NULL);
      self->d.location_string = g_value_dup_string (value);

      if (self->d.location_string != NULL &&
          self->d.location_string[0] == '\0')
        g_clear_pointer (&self->d.location_string, g_free);

      break;

    case PROP_LONGITUDE:
      self->d.longitude = g_value_get_double (value);

      if (!trp_place_is_longitude (self->d.longitude))
        self->d.longitude = TRP_PLACE_LONGITUDE_UNKNOWN;

      break;

    case PROP_POSTAL_CODE:
      /* construct-only */
      g_assert (self->d.postal_code == NULL);
      self->d.postal_code = g_value_dup_string (value);

      if (self->d.postal_code != NULL &&
          self->d.postal_code[0] == '\0')
        g_clear_pointer (&self->d.postal_code, g_free);

      break;

    case PROP_REGION:
      /* construct-only */
      g_assert (self->d.region == NULL);
      self->d.region = g_value_dup_string (value);

      if (self->d.region != NULL && self->d.region[0] == '\0')
        g_clear_pointer (&self->d.region, g_free);

      break;

    case PROP_STREET:
      /* construct-only */
      g_assert (self->d.street == NULL);
      self->d.street = g_value_dup_string (value);

      if (self->d.street != NULL && self->d.street[0] == '\0')
        g_clear_pointer (&self->d.street, g_free);

      break;

    case PROP_BUILDING:
      /* construct-only */
      g_assert (self->d.building == NULL);
      self->d.building = g_value_dup_string (value);

      if (self->d.building != NULL && self->d.building[0] == '\0')
        g_clear_pointer (&self->d.building, g_free);

      break;

    case PROP_UNCERTAINTY:
      self->d.uncertainty = g_value_get_double (value);

      if (!trp_place_is_uncertainty (self->d.uncertainty))
        self->d.uncertainty = TRP_PLACE_UNCERTAINTY_UNKNOWN;

      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_place_constructed (GObject *object)
{
  TrpPlace *self = TRP_PLACE (object);

  G_OBJECT_CLASS (trp_place_parent_class)
      ->constructed (object);

  trp_place_details_normalize (&self->d);

  g_return_if_fail ((trp_place_is_latitude (self->d.latitude) &&
                     trp_place_is_longitude (self->d.longitude)) ||
                    self->d.location_string != NULL ||
                    self->d.formatted_address != NULL);
}

static void
trp_place_finalize (GObject *object)
{
  TrpPlace *self = TRP_PLACE (object);

  trp_place_details_clear (&self->d);

  G_OBJECT_CLASS (trp_place_parent_class)
      ->finalize (object);
}

static void
trp_place_class_init (TrpPlaceClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  /**
   * TrpPlace:location-string:
   *
   * A human-readable description of the place, for example a
   * search query entered by the user, or a locale-specific
   * concatenation of various address-related fields.
   *
   * This must be non-%NULL, unless either the #TrpPlace:formatted-address
   * is non-%NULL, or both the latitude and longitude are
   * provided (with values that satisfy trp_place_is_latitude() and
   * trp_place_is_longitude()) instead.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_LOCATION_STRING] =
      g_param_spec_string ("location-string", "Location string",
                           "Search query or human-readable description of place",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:altitude:
   *
   * The altitude of this place in metres above the coordinate reference
   * system's reference, or %TRP_PLACE_ALTITUDE_UNKNOWN if unknown.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_ALTITUDE] =
      g_param_spec_double ("altitude", "Altitude",
                           "Altitude in metres",
                           TRP_PLACE_ALTITUDE_UNKNOWN, G_MAXDOUBLE,
                           TRP_PLACE_ALTITUDE_UNKNOWN,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:latitude:
   *
   * The latitude of this place in degrees north, between -90.0 and 90.0
   * inclusive, or %TRP_PLACE_LATITUDE_UNKNOWN if unknown.
   *
   * Latitudes south of the Equator are represented as negative numbers.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_LATITUDE] =
      g_param_spec_double ("latitude", "Latitude",
                           "Latitude in degrees north",
                           TRP_PLACE_LATITUDE_UNKNOWN, 90.0,
                           TRP_PLACE_LATITUDE_UNKNOWN,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:longitude:
   *
   * The longitude of this place in degrees east, between -180.0 (exclusive)
   * and 180.0 (inclusive), or %TRP_PLACE_LATITUDE_UNKNOWN if unknown.
   *
   * Latitudes west of Greenwich are represented as negative numbers.
   *
   * A longitude of -180.0 degrees is allowed, and is normalized to +180.0
   * degrees when setting this property.
   *
   * At a latitude of +90.0 or -90.0 degrees (the poles), all longitudes are
   * equivalent, and this property is normalized to 0.0.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_LONGITUDE] =
      g_param_spec_double ("longitude", "Longitude",
                           "Longitude in degrees east",
                           TRP_PLACE_LONGITUDE_UNKNOWN, 180.0,
                           TRP_PLACE_LONGITUDE_UNKNOWN,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:uncertainty:
   *
   * The uncertainty of this place's location in metres, or
   * %TRP_PLACE_UNCERTAINTY_UNKNOWN if unknown.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_UNCERTAINTY] =
      g_param_spec_double ("uncertainty", "Uncertainty",
                           "Uncertainty in metres",
                           TRP_PLACE_LONGITUDE_UNKNOWN, G_MAXDOUBLE,
                           TRP_PLACE_LONGITUDE_UNKNOWN,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:postal-code:
   *
   * The postal code of the place.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_POSTAL_CODE] =
      g_param_spec_string ("postal-code", "Postal code",
                           "Postal code of the place",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:country:
   *
   * The place's country, represented as an ISO 3166-1 alpha-2 code.
   * When read, this can be in any case combination; when written,
   * it is upper-case.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_COUNTRY] =
      g_param_spec_string ("country", "Country code",
                           "ISO 3166-1 alpha-2 country code",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:region:
   *
   * A large subdivision of the #TrpPlace:country, for example a state,
   * province or county.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_REGION] =
      g_param_spec_string ("region", "Region",
                           "Region, state, province or county",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:locality:
   *
   * The place's city, town or other locality.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_LOCALITY] =
      g_param_spec_string ("locality", "Locality",
                           "City, town or locality",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:area:
   *
   * A smaller area within the #TrpPlace:locality, for example a village,
   * suburb or neighbourhood.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_AREA] =
      g_param_spec_string ("area", "Area",
                           "Area within the locality",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:street:
   *
   * The name of the street on which the place is situated.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_STREET] =
      g_param_spec_string ("street", "Street",
                           "Street name",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:building:
   *
   * The name or number of the house or building.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_BUILDING] =
      g_param_spec_string ("building", "Building",
                           "House or building name or number",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpPlace:formatted-address:
   *
   * The full postal address formatted in some (probably locale-dependent)
   * canonical form.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_FORMATTED_ADDRESS] =
      g_param_spec_string ("formatted-address", "Formatted address",
                           "Complete postal address",
                           NULL,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  object_class->get_property = trp_place_get_property;
  object_class->set_property = trp_place_set_property;
  object_class->constructed = trp_place_constructed;
  object_class->finalize = trp_place_finalize;
  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}

/**
 * trp_place_new_for_uri:
 * @uri: a URI
 * @flags: Flags influencing how we parse URIs.
 * @error: Return location for a GError, or NULL
 *
 * Return the place corresponding to the specified URI. The syntax has to be
 * understood according to %TrpUriFlags. If the URI cannot be parsed, return
 * %NULL and set error appropriately.
 *
 * Returns: (transfer full): a new #TrpPlace
 *
 * Since: 0.1612.0
 */
TrpPlace *
trp_place_new_for_uri (const gchar *uri,
                       TrpUriFlags flags,
                       GError **error)
{
  g_autoptr (TrpPlaceBuilder) builder = NULL;

  g_return_val_if_fail (uri != NULL, NULL);
  g_return_val_if_fail ((flags & ~TRP_URI_FLAGS_STRICT) == 0, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  builder = trp_place_builder_new (NULL);

  if (!_trp_place_builder_parse_uri (builder, uri, flags, error))
    return NULL;

  return trp_place_builder_end (builder);
}

/**
 * trp_place_to_uri:
 * @self: a place
 * @scheme: the URI scheme to be used, or %TRP_URI_SCHEME_ANY to choose
 *  automatically
 *
 * Return the URI of the given scheme that best represents this place.
 * If there is not enough information to build a URI, return %NULL.
 *
 * It is an error to use a URI scheme that is not suitable for representing
 * a place, such as %TRP_URI_SCHEME_NAV.
 *
 * Since: 0.1612.0
 */
gchar *
trp_place_to_uri (TrpPlace *self,
                  TrpUriScheme scheme)
{
  g_autoptr (GString) string = NULL;
  gchar buffer[G_ASCII_DTOSTR_BUF_SIZE];

  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  switch (scheme)
    {
    case TRP_URI_SCHEME_ANY:
      {
        if (self->d.location_string != NULL ||
            self->d.formatted_address != NULL ||
            self->d.postal_code != NULL ||
            self->d.country != NULL ||
            self->d.region != NULL ||
            self->d.locality != NULL ||
            self->d.area != NULL ||
            self->d.street != NULL ||
            self->d.building != NULL)
          return trp_place_to_uri (self, TRP_URI_SCHEME_PLACE);
        else if (trp_place_is_latitude (self->d.latitude) &&
                 trp_place_is_longitude (self->d.longitude))
          return trp_place_to_uri (self, TRP_URI_SCHEME_GEO);
        else
          return NULL;
      }
      break;

    case TRP_URI_SCHEME_GEO:
      {
        if (!trp_place_is_latitude (self->d.latitude) ||
            !trp_place_is_longitude (self->d.longitude))
          return NULL;

        string = g_string_new ("geo:");
        g_ascii_dtostr (buffer, sizeof (buffer), self->d.latitude);
        g_string_append (string, buffer);
        g_string_append_c (string, ',');
        g_ascii_dtostr (buffer, sizeof (buffer), self->d.longitude);
        g_string_append (string, buffer);

        if (trp_place_is_altitude (self->d.altitude))
          {
            g_string_append_c (string, ',');
            g_ascii_dtostr (buffer, sizeof (buffer), self->d.altitude);
            g_string_append (string, buffer);
          }

        /* The only CRS we support is WGS84, which is the default, so we
           * don't write out the crs parameter. */

        if (self->d.uncertainty >= 0)
          {
            g_ascii_dtostr (buffer, sizeof (buffer), self->d.uncertainty);
            g_string_append (string, ";u=");
            g_string_append (string, buffer);
          }

        return g_string_free (g_steal_pointer (&string), FALSE);
      }
      break;

    case TRP_URI_SCHEME_PLACE:
      {
        g_autofree gchar *geo = trp_place_to_uri (self, TRP_URI_SCHEME_GEO);

        string = g_string_new ("place:");

        if (self->d.location_string != NULL)
          {
            _trp_uri_append_parameter_value (string, self->d.location_string);
          }
        else
          {
            g_autofree gchar *location_string =
                trp_place_force_location_string (self);

            _trp_uri_append_parameter_value (string, location_string);
          }

        /* Order is unimportant, so please sort these alphabetically */

        if (self->d.area != NULL)
          {
            g_string_append (string, ";area=");
            _trp_uri_append_parameter_value (string, self->d.area);
          }

        if (self->d.country != NULL)
          {
            g_string_append (string, ";country=");
            _trp_uri_append_parameter_value (string, self->d.country);
          }

        if (self->d.formatted_address != NULL)
          {
            g_string_append (string, ";formatted-address=");
            _trp_uri_append_parameter_value (string,
                                             self->d.formatted_address);
          }

        if (self->d.locality != NULL)
          {
            g_string_append (string, ";locality=");
            _trp_uri_append_parameter_value (string, self->d.locality);
          }

        if (geo != NULL)
          {
            g_string_append (string, ";location=");
            _trp_uri_append_parameter_value (string, geo);
          }

        if (self->d.postal_code != NULL)
          {
            g_string_append (string, ";postal-code=");
            _trp_uri_append_parameter_value (string, self->d.postal_code);
          }

        if (self->d.region != NULL)
          {
            g_string_append (string, ";region=");
            _trp_uri_append_parameter_value (string, self->d.region);
          }

        if (self->d.street != NULL)
          {
            g_string_append (string, ";street=");
            _trp_uri_append_parameter_value (string, self->d.street);
          }

        if (self->d.building != NULL)
          {
            g_string_append (string, ";building=");
            _trp_uri_append_parameter_value (string, self->d.building);
          }

        return g_string_free (g_steal_pointer (&string), FALSE);
      }
      break;

    case TRP_URI_SCHEME_NAV:
      CRITICAL ("nav: URIs are unsuitable for representing a place");
      return NULL;

    default:
      g_return_val_if_reached (NULL);
    }
}

/**
 * trp_place_get_location_string:
 * @self: a place
 *
 * Return the human-readable location string for the place, or %NULL if
 * none was specified.
 *
 * Returns: (nullable): #TrpPlace:location-string
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_location_string (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.location_string;
}

/**
 * trp_place_force_location_string:
 * @self: a place
 *
 * Return #TrpPlace:location-string, or if that property is %NULL,
 * make up a fallback location string instead.
 *
 * Returns: (transfer full): the human-readable location string (never %NULL
 *  unless @self is not a valid place)
 *
 * Since: 0.1612.0
 */
gchar *
trp_place_force_location_string (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  if (self->d.location_string != NULL)
    return g_strdup (self->d.location_string);

  if (self->d.formatted_address != NULL)
    return g_strdup (self->d.formatted_address);

  if (trp_place_is_latitude (self->d.latitude) &&
      trp_place_is_longitude (self->d.longitude))
    {
      /* Last-resort description for physical geometry:
       * "52.21°N 0.12°E". */
      return g_strdup_printf ("%.2f°%c %.2f°%c",
                              fabs (self->d.latitude),
                              self->d.latitude >= 0 ? 'N' : 'S',
                              fabs (self->d.longitude),
                              self->d.longitude >= 0 ? 'E' : 'W');
    }

  /* We should never get here because we checked during construction */
  g_return_val_if_reached (NULL);
}

/**
 * trp_place_get_latitude:
 * @self: a place
 *
 * Return the latitude of the place in degrees north of the Equator,
 * or %TRP_PLACE_LATITUDE_UNKNOWN if unknown. Negative results indicate
 * a place south of the Equator.
 *
 * Returns: #TrpPlace:latitude
 *
 * Since: 0.1612.0
 */
gdouble
trp_place_get_latitude (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), TRP_PLACE_LATITUDE_UNKNOWN);

  return self->d.latitude;
}

/**
 * trp_place_get_longitude:
 * @self: a place
 *
 * Return the longitude of the place in degrees east of the Greenwich
 * meridian, or %TRP_PLACE_LONGITUDE_UNKNOWN if unknown. Negative results
 * indicate a place west of the Greenwich meridian.
 *
 * Returns: #TrpPlace:longitude
 *
 * Since: 0.1612.0
 */
gdouble
trp_place_get_longitude (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), TRP_PLACE_LONGITUDE_UNKNOWN);

  return self->d.longitude;
}

/**
 * trp_place_get_altitude:
 * @self: a place
 *
 * Return the altitude of the place in metres above the WGS-84 reference
 * geoid, or %TRP_PLACE_ALTITUDE_UNKNOWN if unknown. Negative results
 * indicate a place below the WGS-84 reference geoid.
 *
 * Returns: #TrpPlace:altitude
 *
 * Since: 0.1612.0
 */
gdouble
trp_place_get_altitude (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), TRP_PLACE_ALTITUDE_UNKNOWN);

  return self->d.altitude;
}

/**
 * trp_place_get_uncertainty:
 * @self: a place
 *
 * Return the uncertainty of the place in metres, or
 * %TRP_PLACE_UNCERTAINTY_UNKNOWN if unspecified. An exact point is denoted by
 * an uncertainty of 0, which is not the same as
 * %TRP_PLACE_UNCERTAINTY_UNKNOWN.
 *
 * Returns: #TrpPlace:uncertainty
 *
 * Since: 0.1612.0
 */
gdouble
trp_place_get_uncertainty (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), TRP_PLACE_UNCERTAINTY_UNKNOWN);

  return self->d.uncertainty;
}

/**
 * trp_place_get_postal_code:
 * @self: a place
 *
 * Return the postal code (postcode, zip code) for the place, or %NULL if
 * none was specified.
 *
 * Returns: (nullable): #TrpPlace:postal-code
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_postal_code (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.postal_code;
}

/**
 * trp_place_get_country_code:
 * @self: a place
 *
 * Return the ISO 3166-1 alpha-2 country code for the place, or %NULL if
 * none was specified. The country code is normalized to upper case.
 *
 * #TrpPlace does not check whether the returned code is currently assigned to
 * a real country; reserved and special-purpose codes such as `ZZ`
 * are also accepted.
 *
 * Returns: (nullable): #TrpPlace:country
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_country_code (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.country;
}

/**
 * trp_place_get_region:
 * @self: a place
 *
 * Return the region name for the place, for example a state or province,
 * or %NULL if none was specified.
 *
 * Returns: (nullable): #TrpPlace:region
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_region (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.region;
}

/**
 * trp_place_get_locality:
 * @self: a place
 *
 * Return the locality name for the place, for example a city or town,
 * or %NULL if none was specified.
 *
 * Returns: (nullable): #TrpPlace:locality
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_locality (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.locality;
}

/**
 * trp_place_get_area:
 * @self: a place
 *
 * Return the area name within the #TrpPlace:locality, for example a village
 * or suburb, or %NULL if none was specified.
 *
 * Returns: (nullable): #TrpPlace:area
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_area (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.area;
}

/**
 * trp_place_get_street:
 * @self: a place
 *
 * Return the street name for the place, or %NULL if none was specified.
 *
 * Returns: (nullable): #TrpPlace:street
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_street (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.street;
}

/**
 * trp_place_get_building:
 * @self: a place
 *
 * Return the building name or number for the place, or %NULL if none was
 * specified.
 *
 * Returns: (nullable): #TrpPlace:building
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_building (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.building;
}

/**
 * trp_place_get_formatted_address:
 * @self: a place
 *
 * Return the complete postal address for the place, or %NULL if none was
 * specified. This function does not attempt to construct a formal address
 * from other parameters: applications requiring that functionality should
 * implement it themselves or use a geocoding service.
 *
 * Returns: (nullable): #TrpPlace:formatted-address
 *
 * Since: 0.1612.0
 */
const gchar *
trp_place_get_formatted_address (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self), NULL);

  return self->d.formatted_address;
}

/**
 * trp_place_get_crs:
 * @self: a place
 *
 * Return the coordinate reference system used for the latitude, longitude
 * and altitude of this place.
 *
 * The only supported coordinate reference system in this version of the
 * Traprain API is WGS-84, which is the same coordinate reference system
 * used for the Global Positioning System. See #TrpCoordinateReferenceSystem
 * for details.
 *
 * If a different coordinate reference system is encountered, API users must
 * not assume that the latitude, longitude and altitude in that coordinate
 * reference system will match those reported by GPS.
 *
 * Returns: the coordinate reference system
 *
 * Since: 0.1612.0
 */
TrpCoordinateReferenceSystem
trp_place_get_crs (TrpPlace *self)
{
  g_return_val_if_fail (TRP_IS_PLACE (self),
                        TRP_COORDINATE_REFERENCE_SYSTEM_WGS84);

  /* This is the only CRS we support */
  return TRP_COORDINATE_REFERENCE_SYSTEM_WGS84;
}
