/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "traprain-common/uri-internal.h"

#include <string.h>

#include "traprain-common/glib-derived.h"

/*
 * @p: (inout) (not nullable) (not optional):
 * @value: (out) (not optional):
 */
gboolean
_trp_uri_consume_double (const gchar **p,
                         gdouble *value,
                         GError **error)
{
  gdouble d;
  gchar *endptr;

  d = g_ascii_strtod (*p, &endptr);

  if (*p == endptr)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "Expected a number at the beginning of: %s", *p);
      return FALSE;
    }

  *p = endptr;
  *value = d;
  return TRUE;
}

/*
 * @p: (inout) (not nullable) (not optional):
 * @value: (out) (optional):
 *
 * Consume a RFC5870 labeltext token (a parameter name). On success,
 * advance @p to the first character after the labeltext token, probably
 * '=', ';' or end-of-string.
 */
gboolean
_trp_uri_consume_label_text (const gchar **p,
                             gchar **value,
                             GError **error)
{
  const gchar *tmp;
  gsize n = 0;

  for (tmp = *p; g_ascii_isalnum (*tmp) || *tmp == '-'; tmp++)
    n++;

  if (n == 0)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "Expected RFC 5870 labeltext at the beginning of: %s", *p);
      return FALSE;
    }

  if (value != NULL)
    *value = g_strndup (*p, n);

  *p += n;
  return TRUE;
}

/* U+FFFD REPLACEMENT CHARACTER */
#define UNICODE_REPLACEMENT_CHARACTER ((gunicode) 0xFFFD)

/*
 * @p: (inout):
 * @value: (out) (not optional):
 *
 * Consume a RFC5870 pvalue token (a parameter value). On success, advance
 * @p to the first character after the pvalue token, probably ';' or
 * end-of-string.
 *
 * If %TRP_URI_FLAGS_STRICT is not in @flags, unescaped commas and equals
 * signs are treated as part of the pvalue. In strict mode, parsing
 * follows RFC5870.
 *
 * If %TRP_URI_FLAGS_STRICT is not in @flags, @p may point into an IRI
 * (internationalized resource identifier), and the returned string
 * is coerced into UTF-8 by replacing invalid encodings with U+FFFD.
 * In strict mode, only a URI (which is defined to be ASCII) is accepted,
 * and any percent-encoded text must be valid UTF-8.
 */
gboolean
_trp_uri_consume_parameter_value (const gchar **p,
                                  TrpUriFlags flags,
                                  gchar **value,
                                  GError **error)
{
  const gchar *tmp;
  g_autoptr (GString) buffer = g_string_new ("");
  gboolean strict = ((flags & TRP_URI_FLAGS_STRICT) ? TRUE : FALSE);

  for (tmp = *p; *tmp != '\0'; tmp++)
    {
      if (*tmp == '%')
        {
          /* %XX escape */
          if (g_ascii_isxdigit (tmp[1]) &&
              g_ascii_isxdigit (tmp[2]))
            {
              g_string_append_c (buffer,
                                 ((g_ascii_xdigit_value (tmp[1]) << 4) |
                                  g_ascii_xdigit_value (tmp[2])));
              /* consume the two hex digits */
              tmp += 2;
            }
          else
            {
              g_set_error (error, TRP_PLACE_ERROR,
                           TRP_PLACE_ERROR_INVALID_URI,
                           "Invalid URI %% escape at the beginning of: %s",
                           tmp);
              return FALSE;
            }
        }
      else if (g_ascii_isalnum (*tmp) ||
               strchr (TRP_URI_RFC5870_MARK
                           TRP_URI_RFC5870_P_UNRESERVED,
                       *tmp) != NULL)
        {
          /* Unreserved (in the context of a parameter value) */
          g_string_append_c (buffer, *tmp);
        }
      else if (!strict && (*tmp == ',' || *tmp == '=' || (guchar) *tmp >= 128))
        {
          g_string_append_c (buffer, *tmp);
        }
      else
        {
          break;
        }
    }

  if (tmp == *p && strict)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "Expected RFC 5870 pvalue at the beginning of: %s", *p);
      return FALSE;
    }

  if (g_utf8_validate (buffer->str, -1, NULL))
    {
      *value = g_string_free (g_steal_pointer (&buffer), FALSE);
    }
  else if (strict)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "Expected parameter value to be UTF-8");
      return FALSE;
    }
  else
    {
      *value = _trp_utf8_make_valid (buffer->str);
    }

  *p = tmp;
  return TRUE;
}

void
_trp_uri_append_parameter_value (GString *string,
                                 const gchar *unescaped_value)
{
  g_string_append_uri_escaped (string, unescaped_value,
                               (TRP_URI_RFC5870_MARK
                                    TRP_URI_RFC5870_P_UNRESERVED),
                               FALSE);
}
