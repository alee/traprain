/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "languages-map-internal.h"

/**
 * SECTION: traprain-common/languages-map.h
 * @title: TrpLanguagesMap
 * @short_description: Container used to store strings which can be available in
 * different languages.
 *
 * #TrpLanguagesMap is a containuer used to store strings which can be available in
 * different languages.
 *
 * Languages are stored using language codes in the POSIX locale format with locale identifiers
 * defined by ISO 15897, like fr_BE for example.
 *
 * Use trp_languages_map_lookup() to lookup for translations in a specific language and
 * trp_languages_map_get() to iterate over all the available translations.
 *
 * Since: 0.1.0
 */

typedef struct
{
  const gchar *language; /* interned */
  gchar *string;         /* owned */
} _TrpLanguageString;

static void
clear_language_string (gpointer a)
{
  _TrpLanguageString *s = a;

  g_free (s->string);
}

TrpLanguagesMap *
_trp_languages_map_new (void)
{
  TrpLanguagesMap *map;

  map = g_array_new (FALSE, FALSE, sizeof (_TrpLanguageString));
  g_array_set_clear_func (map, clear_language_string);

  return map;
}

static gboolean
update_string (TrpLanguagesMap *map,
               const gchar *language,
               const gchar *string)
{
  guint i;

  for (i = 0; i < map->len; i++)
    {
      _TrpLanguageString s = g_array_index (map, _TrpLanguageString, i);

      /* We can compare pointers thanks to g_intern_string() */
      if (s.language == language)
        {
          g_free (s.string);
          s.string = g_strdup (string);
          return TRUE;
        }
    }

  return FALSE;
}

void
_trp_languages_map_add (TrpLanguagesMap *map,
                        const gchar *language,
                        const gchar *string)
{
  _TrpLanguageString l;
  const gchar *language_interned;

  language_interned = g_intern_string (language);

  if (update_string (map, language_interned, string))
    return;

  l.language = language_interned;
  l.string = g_strdup (string);
  g_array_append_val (map, l);
}

GVariant *
_trp_languages_map_to_variant (TrpLanguagesMap *map)
{
  GVariantBuilder builder;
  guint i;

  g_variant_builder_init (&builder, G_VARIANT_TYPE ("a{ss}"));
  for (i = 0; i < map->len; i++)
    {
      _TrpLanguageString l = g_array_index (map, _TrpLanguageString, i);

      g_variant_builder_add (&builder, "{ss}", l.language, l.string);
    }

  return g_variant_builder_end (&builder);
}

void
_trp_languages_map_copy_content (TrpLanguagesMap *dest,
                                 TrpLanguagesMap *source)
{
  guint i;

  for (i = 0; i < source->len; i++)
    {
      _TrpLanguageString l = g_array_index (source, _TrpLanguageString, i);

      _trp_languages_map_add (dest, l.language, l.string);
    }
}

/* Assume @a and @b have their elements in the same order */
void
_trp_languages_map_assert_same_content (TrpLanguagesMap *a,
                                        TrpLanguagesMap *b)
{
  guint i;

  g_assert_cmpuint (a->len, ==, b->len);

  for (i = 0; i < a->len; i++)
    {
      _TrpLanguageString str_a, str_b;

      str_a = g_array_index (a, _TrpLanguageString, i);
      str_b = g_array_index (b, _TrpLanguageString, i);

      /* language string is interned so we compare pointers directly */
      g_assert (str_a.language == str_b.language);
      g_assert_cmpstr (str_a.string, ==, str_b.string);
    }
}

TrpLanguagesMap *
_trp_languages_map_new_from_variant (GVariant *v)
{
  TrpLanguagesMap *map;
  g_autoptr (GVariant) _v = NULL;
  g_autoptr (GVariantIter) iter = NULL;
  const gchar *key, *value;

  g_return_val_if_fail (v != NULL, NULL);
  g_return_val_if_fail (g_variant_is_of_type (v, G_VARIANT_TYPE ("a{ss}")), NULL);

  _v = g_variant_ref_sink (v);
  map = _trp_languages_map_new ();

  g_variant_get (_v, "a{ss}", &iter);
  while (g_variant_iter_next (iter, "{&s&s}", &key, &value))
    _trp_languages_map_add (map, key, value);

  return map;
}

/**
 * trp_languages_map_lookup:
 * @map: a #TrpLanguagesMap
 * @language: a language code
 *
 * Look for the translation of the stored string in @language,
 *
 * Returns: (nullable): the string in @language if found, or %NULL
 * Since: 0.1.0
 */
const gchar *
trp_languages_map_lookup (TrpLanguagesMap *map,
                          const gchar *language)
{
  const gchar *language_interned;
  guint i;

  g_return_val_if_fail (map != NULL, NULL);

  /* If the language string has not been interned we have lost already.
   * This prevent garbage strings to be interned and kept in memory forever. */
  if (g_quark_try_string (language) == 0)
    return NULL;

  language_interned = g_intern_string (language);
  for (i = 0; i < map->len; i++)
    {
      _TrpLanguageString s = g_array_index (map, _TrpLanguageString, i);

      if (s.language == language_interned)
        return s.string;
    }

  return NULL;
}

/**
 * trp_languages_map_get:
 * @map: a #TrpLanguagesMap
 * @index: the index of the string to return
 * @language: (out) (optional) (nullable) (transfer none): pointer used to return the language, or %NULL if %FALSE is returned
 * @string: (out) (optional) (nullable) (transfer none): pointer used to return the string, or %NULL if %FALSE is returned
 *
 * Look for the language and translation at index @index in @map.
 *
 * This function can be used to iterate over all the translations available of the string:
 * |[<!-- language="C" -->
 * gsize i;
 * const gchar *lang, *str;
 *
 * for (i = 0; trp_languages_map_get (map, i, &lang, &str); i++)
 *   g_print ("%s -> %s\n", lang, str);
 * ]|
 *
 * Returns: %TRUE if found, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
trp_languages_map_get (TrpLanguagesMap *map,
                       gsize index,
                       const gchar **language,
                       const gchar **string)
{
  _TrpLanguageString s;
  const gchar *_language = NULL, *_string = NULL;
  gboolean result = FALSE;

  g_return_val_if_fail (map != NULL, FALSE);

  if (index >= map->len)
    goto out;

  s = g_array_index (map, _TrpLanguageString, index);
  _language = s.language;
  _string = s.string;
  result = TRUE;

out:
  if (language != NULL)
    *language = _language;
  if (string != NULL)
    *string = _string;

  g_assert ((_language != NULL) == result);
  g_assert ((_string != NULL) == result);
  return result;
}
