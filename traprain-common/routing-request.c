/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "traprain-common/routing-request.h"

#include <math.h>
#include <string.h>

#include <gio/gio.h>

#include "traprain-common/enumtypes.h"
#include "traprain-common/messages-internal.h"
#include "traprain-common/uri-internal.h"

/**
 * SECTION: traprain-common/routing-request.h
 * @title: TrpRoutingRequest
 * @short_description: Object representing a request for navigation routing
 *
 * Routing request objects represent the information that is to be given
 * to a routing engine so that it can calculate a desired route.
 *
 * Since: 0.1612.0
 */

/**
 * TrpRoutingParameter:
 *
 * A data structure representing a routing point or parameter.
 *
 * Routing points can either be *waypoints* (a final or intermediate
 * destination where the route should stop permanently or temporarily),
 * or *via points* (a place or region near which the route should pass
 * without stopping).
 *
 * Other routing parameters include the description of the route.
 *
 * Because future routing parameters (other than routing points) might have a
 * meaning that changes depending where in the sequence of points they
 * appear, they are returned by trp_routing_request_get_parameters()
 * interleaved in sequence with the points.
 *
 * Since: 0.1612.0
 */

struct _TrpRoutingParameter
{
  gsize refcount;
  /* owned */
  gchar *name;
  gchar *value;
  /* owned, nullable */
  TrpPlace *place;
};

G_DEFINE_BOXED_TYPE (TrpRoutingParameter, trp_routing_parameter, trp_routing_parameter_ref, trp_routing_parameter_unref)

/**
 * trp_routing_parameter_is_name:
 * @name: a potential parameter name
 *
 * Return whether @name is a valid parameter name. Valid parameter names
 * consist of one or more ASCII letters, digits and `-` (U+002D HYPHEN-MINUS)
 * characters.
 *
 * Returns: %TRUE if @name is a syntactically valid parameter name
 *
 * Since: 0.1612.0
 */
gboolean
trp_routing_parameter_is_name (const gchar *name)
{
  const gchar *p = name;

  if (name == NULL || *name == '\0')
    return FALSE;

  if (!_trp_uri_consume_label_text (&p, NULL, NULL))
    return FALSE;

  return (*p == '\0');
}

/**
 * trp_routing_parameter_new:
 * @name: the name of the parameter
 * @value: the value of the parameter
 *
 * Create a new routing parameter with the specified name and value.
 * @name is normalised to lower-case.
 *
 * Returns: (transfer full): a routing parameter
 *
 * Since: 0.1612.0
 */
TrpRoutingParameter *
trp_routing_parameter_new (const gchar *name,
                           const gchar *value)
{
  TrpRoutingParameter *self;

  g_return_val_if_fail (trp_routing_parameter_is_name (name), NULL);
  g_return_val_if_fail (g_utf8_validate (value, -1, NULL), NULL);

  self = g_slice_new0 (TrpRoutingParameter);
  self->refcount = 1;
  self->name = g_ascii_strdown (name, -1);
  self->value = g_strdup (value);
  return self;
}

/**
 * trp_routing_parameter_new_for_place:
 * @name: the name of the parameter, typically either
 *  %TRP_ROUTING_PARAMETER_WAYPOINT or %TRP_ROUTING_PARAMETER_VIA
 * @place: (transfer none): the place
 *
 * Create a new routing parameter with the specified name, pointing to the
 * specified place.
 * The name is normalised to lower-case.
 *
 * Returns: (transfer full): a routing parameter
 *
 * Since: 0.1612.0
 */
TrpRoutingParameter *
trp_routing_parameter_new_for_place (const gchar *name,
                                     TrpPlace *place)
{
  TrpRoutingParameter *self;

  g_return_val_if_fail (TRP_IS_PLACE (place), NULL);
  g_return_val_if_fail (trp_routing_parameter_is_name (name), NULL);

  self = g_slice_new0 (TrpRoutingParameter);
  self->refcount = 1;
  self->name = g_ascii_strdown (name, -1);
  self->place = g_object_ref (place);
  return self;
}

/**
 * trp_routing_parameter_unref:
 * @self: (transfer full): the routing parameter
 *
 * Decrement the reference count of @self. If it reaches zero, free @self.
 *
 * Since: 0.1612.0
 */
void
trp_routing_parameter_unref (TrpRoutingParameter *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->name != NULL);
  g_return_if_fail (self->value != NULL || TRP_IS_PLACE (self->place));
  g_return_if_fail (self->refcount >= 1);

  if (--self->refcount == 0)
    {
      g_free (self->name);
      g_free (self->value);
      g_clear_object (&self->place);
      g_slice_free (TrpRoutingParameter, self);
    }
}

/**
 * trp_routing_parameter_ref:
 * @self: a routing parameter
 *
 * Return a new reference to @self.
 *
 * Returns: (transfer full): @self
 *
 * Since: 0.1612.0
 */
TrpRoutingParameter *
trp_routing_parameter_ref (TrpRoutingParameter *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->name != NULL, NULL);
  g_return_val_if_fail (self->value != NULL || TRP_IS_PLACE (self->place), NULL);
  g_return_val_if_fail (self->refcount >= 1, NULL);

  self->refcount++;
  return self;
}

/**
 * trp_routing_parameter_get_name:
 * @self: a routing parameter
 *
 * Return name of routing parameter.
 * It can be one of #TRP_ROUTING_PARAMETER_DESCRIPTION,
 * #TRP_ROUTING_PARAMETER_VIA or #TRP_ROUTING_PARAMETER_WAYPOINT, or another
 * user defined value.
 *
 * Returns: (nullable) (transfer none): name of routing parameter
 *
 * Since: 0.1612.0
 */
const gchar *
trp_routing_parameter_get_name (TrpRoutingParameter *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->name != NULL, NULL);
  g_return_val_if_fail (self->value != NULL || TRP_IS_PLACE (self->place), NULL);
  g_return_val_if_fail (self->refcount >= 1, NULL);

  return self->name;
}

/**
 * trp_routing_parameter_get_place:
 * @self: a routing parameter
 *
 * If this routing parameter could represent a #TrpPlace, return it. Otherwise,
 * return %NULL.
 *
 * This function uses default parameters for URI parsing.
 * If full control over URI parsing and error reporting is required, use
 * trp_routing_parameter_get_value() and trp_place_new_for_uri() instead.
 *
 * Returns: (nullable) (transfer full): the place
 *
 * Since: 0.1612.0
 */
TrpPlace *
trp_routing_parameter_get_place (TrpRoutingParameter *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->name != NULL, NULL);
  g_return_val_if_fail (self->value != NULL || TRP_IS_PLACE (self->place), NULL);
  g_return_val_if_fail (self->refcount >= 1, NULL);

  if (self->place == NULL)
    self->place = trp_place_new_for_uri (self->value, TRP_URI_FLAGS_NONE, NULL);

  if (self->place == NULL)
    return NULL;

  return g_object_ref (self->place);
}

/**
 * trp_routing_parameter_get_value:
 * @self: a routing parameter
 *
 * Return the string value of this routing parameter.
 * It is guaranteed to be UTF-8.
 *
 * Returns: (nullable) (transfer full): the value
 *
 * Since: 0.1612.0
 */
const gchar *
trp_routing_parameter_get_value (TrpRoutingParameter *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->name != NULL, NULL);
  g_return_val_if_fail (self->value != NULL || TRP_IS_PLACE (self->place), NULL);
  g_return_val_if_fail (self->refcount >= 1, NULL);

  if (self->value == NULL)
    self->value = trp_place_to_uri (self->place, TRP_URI_SCHEME_ANY);

  /* Shouldn't happen: all places contain enough information to encode
   * themselves. */
  g_return_val_if_fail (self->value != NULL, NULL);

  return self->value;
}

/**
 * TrpRoutingRequest:
 *
 * Routing request objects represent the information that is to be given
 * to a routing engine so that it can calculate a desired route.
 *
 * Every routing request has exactly one final destination.
 *
 * Routing requests may also have a sequence of via-points and
 * waypoints (intermediate destinations) which the routing engine is
 * expected to take into account.
 *
 * TrpRoutingRequest is an immutable "value object". Use a
 * #TrpRoutingRequestBuilder to build up a #TrpRoutingRequest from parameters.
 *
 * Since: 0.1612.0
 */
struct _TrpRoutingRequest
{
  /*<private>*/
  GObject parent;
  GPtrArray *parameters;
  TrpPlace *destination;
};

/**
 * TrpRoutingRequestClass:
 *
 * #TrpRoutingRequest cannot be subclassed.
 *
 * Since: 0.1612.0
 */
struct _TrpRoutingRequestClass
{
  /*<private>*/
  GObjectClass parent;
};

typedef enum {
  PROP_PARAMETERS = 1,
  PROP_DESTINATION
} Property;

static GParamSpec *property_specs[PROP_DESTINATION + 1] = { NULL };

G_DEFINE_TYPE (TrpRoutingRequest, trp_routing_request, G_TYPE_OBJECT)

/**
 * TrpRoutingRequestBuilder:
 *
 * Mutable object used to build up a #TrpRoutingRequest from parameters.
 *
 * Since: 0.1612.0
 */
struct _TrpRoutingRequestBuilder
{
  /*<private>*/
  gsize refcount;
  GPtrArray *parameters;
  TrpPlace *destination;
};

G_DEFINE_BOXED_TYPE (TrpRoutingRequestBuilder, trp_routing_request_builder, trp_routing_request_builder_ref, trp_routing_request_builder_unref)

/**
 * trp_routing_request_builder_new:
 * @destination: (nullable): if not %NULL, set this as the destination
 *
 * Create a new builder object to build up a routing request with
 * @destination as its destination.
 *
 * Returns: (transfer full): a new builder object
 *
 * Since: 0.1612.0
 */
TrpRoutingRequestBuilder *
trp_routing_request_builder_new (TrpPlace *destination,
                                 const gchar *description)
{
  g_autoptr (TrpRoutingRequestBuilder) self =
      g_slice_new0 (TrpRoutingRequestBuilder);
  g_return_val_if_fail (description == NULL ||
                            g_utf8_validate (description, -1, NULL),
                        NULL);

  g_return_val_if_fail (destination == NULL || TRP_IS_PLACE (destination),
                        NULL);

  self->refcount = 1;
  self->parameters =
      g_ptr_array_new_with_free_func ((GDestroyNotify) trp_routing_parameter_unref);
  self->destination = NULL;

  trp_routing_request_builder_set_description (self, description);

  if (destination != NULL)
    self->destination = g_object_ref (destination);

  return g_steal_pointer (&self);
}

/**
 * trp_routing_request_builder_ref:
 * @self: a routing request builder
 *
 * Increment the reference count on @self.
 *
 * Returns: (transfer full): a new reference to @self
 *
 * Since: 0.1612.0
 */
TrpRoutingRequestBuilder *
trp_routing_request_builder_ref (TrpRoutingRequestBuilder *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);

  self->refcount++;
  return self;
}

/**
 * trp_routing_request_builder_unref:
 * @self: (transfer full): a routing request builder
 *
 * Decrement the reference count on @self, and free it if the reference
 * count reaches zero.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_unref (TrpRoutingRequestBuilder *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);

  if (--self->refcount == 0)
    {
      g_clear_pointer (&self->parameters, g_ptr_array_unref);
      g_clear_object (&self->destination);
      g_slice_free (TrpRoutingRequestBuilder, self);
    }
}

/**
 * trp_routing_request_builder_clear:
 * @self: a routing request builder
 *
 * Remove all parameters from @self, including the destination, description,
 * waypoints and via-points.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_clear (TrpRoutingRequestBuilder *self)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);

  g_clear_pointer (&self->parameters, g_ptr_array_unref);
  self->parameters =
      g_ptr_array_new_with_free_func ((GDestroyNotify) trp_routing_parameter_unref);
  g_clear_object (&self->destination);
}

/**
 * trp_routing_request_builder_set_description:
 * @self: a routing request builder
 * @description: (nullable): the new description in UTF-8, or %NULL to unset
 *
 * Set or unset the human-readable description of the route, for example
 * "Edinburgh via Newcastle".
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_set_description (TrpRoutingRequestBuilder *self,
                                             const gchar *description)
{
  trp_routing_request_builder_set_parameter (self,
                                             TRP_ROUTING_PARAMETER_DESCRIPTION,
                                             description);
}

/**
 * trp_routing_request_builder_set_destination:
 * @self: a routing request builder
 * @destination: (nullable): the new destination, or %NULL to unset
 *
 * Set or clear the final destination for the route.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_set_destination (TrpRoutingRequestBuilder *self,
                                             TrpPlace *destination)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (destination == NULL || TRP_IS_PLACE (destination));

  g_set_object (&self->destination, destination);
}

/**
 * trp_routing_request_builder_add_waypoint:
 * @self: a routing request builder
 * @waypoint: (not nullable): the new waypoint
 *
 * Add a new waypoint (intermediate destination) to the route. The route
 * will temporarily stop here.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_add_waypoint (TrpRoutingRequestBuilder *self,
                                          TrpPlace *waypoint)
{
  trp_routing_request_builder_add_place (self, TRP_ROUTING_PARAMETER_WAYPOINT,
                                         waypoint);
}

/**
 * trp_routing_request_builder_add_via:
 * @self: a routing request builder
 * @via: (not nullable): the new via-point
 *
 * Add a new via-point to the route. The route will pass close to this point
 * without stopping.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_add_via (TrpRoutingRequestBuilder *self,
                                     TrpPlace *via)
{
  trp_routing_request_builder_add_place (self, TRP_ROUTING_PARAMETER_VIA, via);
}

/**
 * trp_routing_request_builder_add_place:
 * @self: a routing request builder
 * @name: (not nullable): the name of the parameter
 * @place: (not nullable): the place
 *
 * Add a new parameter representing a place to the request builder.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_add_place (TrpRoutingRequestBuilder *self,
                                       const gchar *name,
                                       TrpPlace *place)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (TRP_IS_PLACE (place));
  g_return_if_fail (trp_routing_parameter_is_name (name));

  g_ptr_array_add (self->parameters,
                   trp_routing_parameter_new_for_place (name, place));
}

/**
 * trp_routing_request_builder_set_parameter:
 * @self: a routing request builder
 * @name: (not nullable): the name of the parameter
 * @value: (nullable): the value of the parameter, or %NULL to unset
 *
 * Remove all parameters named @name from @self. If @value is not %NULL,
 * add it as a new value of the parameter @name.
 * @name is normalised to lower-case.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_set_parameter (TrpRoutingRequestBuilder *self,
                                           const gchar *name,
                                           const gchar *value)
{
  guint i;
  g_autofree gchar *lower = NULL;

  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_routing_parameter_is_name (name));
  g_return_if_fail (value == NULL || g_utf8_validate (value, -1, NULL));

  lower = g_ascii_strdown (name, -1);

  for (i = 0; i < self->parameters->len; /* nothing */)
    {
      TrpRoutingParameter *p = g_ptr_array_index (self->parameters, i);

      if (strcmp (p->name, lower) == 0)
        g_ptr_array_remove_index (self->parameters, i);
      else
        i++;
    }

  if (value != NULL)
    g_ptr_array_add (self->parameters,
                     trp_routing_parameter_new (name, value));
}

/**
 * trp_routing_request_builder_add_parameter:
 * @self: a routing request builder
 * @name: (not nullable): the name of the parameter
 * @value: (not nullable): the value of the parameter
 *
 * Add a new parameter to the request builder.
 * @name is normalised to lower-case.
 *
 * Since: 0.1612.0
 */
void
trp_routing_request_builder_add_parameter (TrpRoutingRequestBuilder *self,
                                           const gchar *name,
                                           const gchar *value)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_routing_parameter_is_name (name));
  g_return_if_fail (g_utf8_validate (value, -1, NULL));

  g_ptr_array_add (self->parameters,
                   trp_routing_parameter_new (name, value));
}

static gboolean
_trp_routing_request_builder_parse_nav_uri (TrpRoutingRequestBuilder *self,
                                            const gchar *uri,
                                            TrpUriFlags flags,
                                            GError **error)
{
  const gchar *p;
  g_autofree gchar *dest_string = NULL;
  g_autoptr (TrpPlace) destination = NULL;
  gboolean strict = ((flags & TRP_URI_FLAGS_STRICT) ? TRUE : FALSE);

  if (g_ascii_strncasecmp (uri, "nav:", 4) != 0)
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_UNKNOWN_SCHEME,
                   "URI does not start with nav:");
      goto error;
    }

  p = uri + 4;

  if (*p == '/')
    {
      g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                   "nav: URIs are not hierarchical");
      goto error;
    }

  /* The destination is syntactically the same as a parameter value */
  if (!_trp_uri_consume_parameter_value (&p, flags, &dest_string, error))
    goto error;

  destination = trp_place_new_for_uri (dest_string, flags, error);

  if (destination == NULL)
    goto error;

  trp_routing_request_builder_set_destination (self, destination);

  while (*p == ';')
    {
      g_autofree gchar *key = NULL;
      g_autofree gchar *value = NULL;

      p++;

      /* Tolerate multiple ';' */
      if (!strict && *p == ';')
        continue;

      /* Tolerate trailing ';' */
      if (!strict && *p == '\0')
        return TRUE;

      if (!_trp_uri_consume_label_text (&p, &key, error))
        goto error;

      if (*p == '=')
        {
          p++;

          if (!_trp_uri_consume_parameter_value (&p, flags, &value, error))
            goto error;
        }
      /* else leave the value set to NULL */

      if (g_ascii_strcasecmp (key, TRP_ROUTING_PARAMETER_WAYPOINT) == 0 ||
          g_ascii_strcasecmp (key, TRP_ROUTING_PARAMETER_VIA) == 0)
        {
          g_autoptr (TrpPlace) there = NULL;

          if (value == NULL || value[0] == '\0')
            {
              g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
                           "Expected a value for %s parameter", key);
              goto error;
            }

          there = trp_place_new_for_uri (value, flags, error);

          if (there == NULL)
            goto error;

          trp_routing_request_builder_add_place (self, key, there);
        }
      else
        {
          if (g_ascii_strcasecmp (key,
                                  TRP_ROUTING_PARAMETER_DESCRIPTION) == 0 &&
              (value == NULL || value[0] == '\0'))
            {
              if (strict)
                {
                  g_set_error (error, TRP_PLACE_ERROR,
                               TRP_PLACE_ERROR_INVALID_URI,
                               "Expected a value for %s parameter", key);
                  goto error;
                }
              else
                {
                  /* ignore an empty description */
                  continue;
                }
            }

          trp_routing_request_builder_add_parameter (self, key,
                                                     value == NULL ? "" : value);
        }
    }

  if (*p == '\0')
    {
      return TRUE;
    }

  g_set_error (error, TRP_PLACE_ERROR, TRP_PLACE_ERROR_INVALID_URI,
               "Expected a semicolon or end-of-string at: %s", p);

error:
  trp_routing_request_builder_clear (self);
  return FALSE;
}

/**
 * trp_routing_request_builder_set_parameter_boolean:
 * @self: a routing request builder
 * @name: a parameter name
 * @pref: a boolean value
 *
 * Set the boolean routing request parameter associated to @name.
 * Valid values are `0`, `1`, `true` and `false`. The comparison is case
 * insensitive.
 *
 * Since: 0.1612.1
 */
void
trp_routing_request_builder_set_parameter_boolean (TrpRoutingRequestBuilder *self,
                                                   const gchar *name,
                                                   gboolean pref)
{
  g_return_if_fail (self != NULL);
  g_return_if_fail (self->refcount > 0);
  g_return_if_fail (trp_routing_parameter_is_name (name));

  trp_routing_request_builder_set_parameter (self, name, pref ? "1" : "0");
}

/**
 * trp_routing_request_builder_copy:
 * @self: a routing request builder
 *
 * Copy the current parameters from the routing request builder and use
 * them to create a new routing request.
 *
 * The routing request builder must have a destination. All other parameters
 * are optional.
 *
 * Returns: (transfer full): a routing request with the same contents as the
 *  routing request builder
 *
 * Since: 0.1612.0
 */
TrpRoutingRequest *
trp_routing_request_builder_copy (TrpRoutingRequestBuilder *self)
{
  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);
  g_return_val_if_fail (TRP_IS_PLACE (self->destination), NULL);

  return g_object_new (TRP_TYPE_ROUTING_REQUEST,
                       "destination", self->destination,
                       "parameters", self->parameters,
                       NULL);
}

/**
 * trp_routing_request_builder_end:
 * @self: a routing request builder
 *
 * Create a new routing request, and clear the routing request builder.
 *
 * The routing request builder must have a destination. All other parameters
 * are optional.
 *
 * Returns: (transfer full): a routing request with the same contents as the
 *  routing request builder
 *
 * Since: 0.1612.0
 */
TrpRoutingRequest *
trp_routing_request_builder_end (TrpRoutingRequestBuilder *self)
{
  g_autoptr (TrpRoutingRequest) request = NULL;

  g_return_val_if_fail (self != NULL, NULL);
  g_return_val_if_fail (self->refcount > 0, NULL);
  g_return_val_if_fail (TRP_IS_PLACE (self->destination), NULL);

  request = trp_routing_request_builder_copy (self);
  trp_routing_request_builder_clear (self);
  return g_steal_pointer (&request);
}

/**
 * trp_routing_request_new:
 * @destination: a destination
 * @description: a human-readable description of the routing request
 *
 * Create a new routing request.
 *
 * Since: 0.1612.0
 */
TrpRoutingRequest *
trp_routing_request_new (TrpPlace *destination,
                         const gchar *description)
{
  /* not using a free-function because @parameter stays in scope anyway */
  g_autoptr (GPtrArray) parameters = g_ptr_array_sized_new (1);
  g_autoptr (TrpRoutingParameter) parameter = NULL;
  g_return_val_if_fail (TRP_IS_PLACE (destination), NULL);
  g_return_val_if_fail (description == NULL ||
                            g_utf8_validate (description, -1, NULL),
                        NULL);

  if (description != NULL)
    {
      parameter = trp_routing_parameter_new (TRP_ROUTING_PARAMETER_DESCRIPTION,
                                             description);
      g_ptr_array_add (parameters, parameter);
    }

  return g_object_new (TRP_TYPE_ROUTING_REQUEST,
                       "destination", destination,
                       "parameters", parameters,
                       NULL);
}

/**
 * trp_routing_request_get_description:
 * @self: a routing request
 *
 * Return a human-readable description of the routing request, if available.
 *
 * Returns: (nullable): the human-readable description
 *
 * Since: 0.1612.0
 */
const gchar *
trp_routing_request_get_description (TrpRoutingRequest *self)
{
  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);

  return trp_routing_request_get_parameter_value (self,
                                                  TRP_ROUTING_PARAMETER_DESCRIPTION);
}

/**
 * trp_routing_request_get_destination:
 * @self: a routing request
 *
 * Return the final destination of the routing request.
 *
 * Returns: (not nullable) (transfer full): the #TrpRoutingRequest:destination
 *
 * Since: 0.1612.0
 */
TrpPlace *
trp_routing_request_get_destination (TrpRoutingRequest *self)
{
  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);

  return g_object_ref (self->destination);
}

/**
 * trp_routing_request_get_parameters:
 * @self: a routing request
 *
 * Return all parameters of this route, together with all waypoints
 * (intermediate destinations) and via-points. However,
 * the #TrpRoutingRequest:destination is not included.
 *
 * The route that is planned by a routing engine as a result of this routing
 * request should pass through all the via-points and waypoints in the
 * order given.
 *
 * All defined parameters other than waypoints and via-points currently apply
 * to the route as a whole, but parameters that are order-sensitive might
 * be added in future.
 *
 * Returns: (transfer container) (element-type TrpRoutingParameter): the
 *  via-points, waypoints and destination in order
 *
 * Since: 0.1612.0
 */
GPtrArray *
trp_routing_request_get_parameters (TrpRoutingRequest *self)
{
  g_autoptr (GPtrArray) ret = NULL;
  guint i;
  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);

  ret = g_ptr_array_new_full (self->parameters->len,
                              (GDestroyNotify) trp_routing_parameter_unref);

  for (i = 0; i < self->parameters->len; i++)
    {
      TrpRoutingParameter *p = g_ptr_array_index (self->parameters, i);

      g_ptr_array_add (ret, trp_routing_parameter_ref (p));
    }

  return g_steal_pointer (&ret);
}

/**
 * trp_routing_request_get_points:
 * @self: a routing request
 *
 * Return all requested points along the route.
 *
 * The last element of the returned array is always the
 * #TrpRoutingRequest:destination, as a #TrpRoutingParameter whose type is
 * %TRP_ROUTING_PARAMETER_WAYPOINT.
 *
 * Elements before the last element are routing points: either via points
 * (places or regions close to which the route should pass without stopping),
 * or waypoints representing intermediate destinations at which the route
 * should temporarily stop, or some other kind of point added in a future
 * version of this API.
 *
 * The route that is planned by a routing engine as a result of this routing
 * request should pass through all the via-points and waypoints in the
 * order given.
 *
 * Returns: (transfer container) (element-type TrpRoutingParameter): the
 *  via-points, waypoints and destination in order
 *
 * Since: 0.1612.0
 */
GPtrArray *
trp_routing_request_get_points (TrpRoutingRequest *self)
{
  g_autoptr (GPtrArray) points = NULL;
  guint i;

  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);

  points = g_ptr_array_new_full (self->parameters->len + 1,
                                 (GDestroyNotify) trp_routing_parameter_unref);

  for (i = 0; i < self->parameters->len; i++)
    {
      TrpRoutingParameter *p = g_ptr_array_index (self->parameters, i);

      if (g_strcmp0 (p->name, TRP_ROUTING_PARAMETER_VIA) == 0 ||
          g_strcmp0 (p->name, TRP_ROUTING_PARAMETER_WAYPOINT) == 0)
        g_ptr_array_add (points, trp_routing_parameter_ref (p));
    }

  g_ptr_array_add (points,
                   trp_routing_parameter_new_for_place (
                       TRP_ROUTING_PARAMETER_WAYPOINT, self->destination));

  return g_steal_pointer (&points);
}

/**
 * trp_routing_request_get_parameter_value:
 * @self: a routing request
 * @name: a parameter name
 *
 * If there is exactly one parameter of this name, return its associated
 * value. Otherwise (if there are no parameters of this name, or
 * if there is more than one), return %NULL.
 * @name comparison is case insensitive.
 *
 * Returns: (nullable): the unique parameter value for @name, or %NULL
 *
 * Since: 0.1612.0
 */
const gchar *
trp_routing_request_get_parameter_value (TrpRoutingRequest *self,
                                         const gchar *name)
{
  const gchar *ret = NULL;
  g_autofree gchar *lower = NULL;
  guint i;

  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);
  g_return_val_if_fail (trp_routing_parameter_is_name (name), NULL);

  lower = g_ascii_strdown (name, -1);

  for (i = 0; i < self->parameters->len; i++)
    {
      TrpRoutingParameter *p = g_ptr_array_index (self->parameters, i);

      if (strcmp (p->name, lower) == 0)
        {
          if (ret == NULL)
            /* this is the first time we've seen it */
            ret = trp_routing_parameter_get_value (p);
          else
            /* this is the second time we've seen it */
            return NULL;
        }
    }

  return ret;
}

/**
 * trp_routing_request_get_preference:
 * @self: a routing request
 * @name: a parameter name
 * @default_pref: a default value for  name
 *
 * Retrieve the routing request preference associated to @name.
 * If the preference does not appear exactly once in the routing request,
 * return @default_pref instead.
 *
 * Returns: (nullable) (transfer none): preference value for @name or @default_pref
 *
 * Since: 0.1612.1
 */
const gchar *
trp_routing_request_get_preference (TrpRoutingRequest *self,
                                    const gchar *name,
                                    const gchar *default_pref)
{
  const gchar *value;
  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), default_pref);
  g_return_val_if_fail (trp_routing_parameter_is_name (name), default_pref);

  value = trp_routing_request_get_parameter_value (self, name);

  if (value != NULL)
    return value;

  return default_pref;
}

/**
 * trp_routing_request_get_preference_boolean:
 * @self: a routing request
 * @name: a preference name
 * @default_pref: a default boolean value for name
 *
 * Retrieve the routing request preference boolean associated to @name.
 * If the preference does not appear exactly once in the routing request, or
 * if its value is not recognised as a boolean value,
 * return @default_pref instead.
 *
 * Returns: preference value for @name or @default_pref
 *
 * Since: 0.1612.1
 */
gboolean
trp_routing_request_get_preference_boolean (TrpRoutingRequest *self,
                                            const gchar *name,
                                            gboolean default_pref)
{
  const gchar *value;
  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), default_pref);
  g_return_val_if_fail (trp_routing_parameter_is_name (name), default_pref);

  value = trp_routing_request_get_parameter_value (self, name);

  if (value == NULL)
    return default_pref;

  if (g_str_equal (value, "0") ||
      g_ascii_strcasecmp (value, "false") == 0)
    return FALSE;

  if (g_str_equal (value, "1") ||
      g_ascii_strcasecmp (value, "true") == 0)
    return TRUE;

  return default_pref;
}

/**
 * trp_routing_request_get_parameter_values:
 * @self: a routing request
 * @name: a parameter name
 *
 * Return all the values of parameter @name.
 *
 * Returns: (transfer full) (array zero-terminated=1) (not nullable): the
 *  parameter values for @name
 *
 * Since: 0.1612.0
 */
gchar **
trp_routing_request_get_parameter_values (TrpRoutingRequest *self,
                                          const gchar *name)
{
  g_autoptr (GPtrArray) ret = g_ptr_array_new_with_free_func (g_free);
  g_autofree gchar *lower = NULL;
  guint i;

  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);
  g_return_val_if_fail (trp_routing_parameter_is_name (name), NULL);

  lower = g_ascii_strdown (name, -1);

  for (i = 0; i < self->parameters->len; i++)
    {
      TrpRoutingParameter *p = g_ptr_array_index (self->parameters, i);

      if (strcmp (p->name, lower) == 0)
        g_ptr_array_add (ret, g_strdup (trp_routing_parameter_get_value (p)));
    }

  g_ptr_array_add (ret, NULL);

  return (GStrv) g_ptr_array_free (g_steal_pointer (&ret), FALSE);
}

/**
 * trp_routing_request_to_uri:
 * @self: a routing request
 * @scheme: %TRP_URI_SCHEME_NAV
 *
 * Return a URI representing this routing request.
 *
 * It is an error to use a URI scheme that is not suitable for representing
 * a routing request, such as %TRP_URI_SCHEME_GEO.
 *
 * Returns: (transfer full): the URI
 *
 * Since: 0.1612.0
 */
gchar *
trp_routing_request_to_uri (TrpRoutingRequest *self,
                            TrpUriScheme scheme)
{
  g_autoptr (GString) string = g_string_new ("nav:");
  g_autofree gchar *destination = NULL;
  guint i;

  g_return_val_if_fail (TRP_IS_ROUTING_REQUEST (self), NULL);
  g_return_val_if_fail (scheme == TRP_URI_SCHEME_NAV ||
                            scheme == TRP_URI_SCHEME_ANY,
                        NULL);

  destination = trp_place_to_uri (self->destination, TRP_URI_SCHEME_ANY);

  if (destination == NULL)
    /* will already have critical'd */
    return NULL;

  _trp_uri_append_parameter_value (string, destination);

  for (i = 0; i < self->parameters->len; i++)
    {
      TrpRoutingParameter *p = g_ptr_array_index (self->parameters, i);
      const gchar *value = trp_routing_parameter_get_value (p);

      if (p == NULL)
        {
          /* should not happen: non-places have a value by definition, and
           * all TrpPlace objects should contain enough information to avoid
           * conversion to URI failing. We already raised a critical. */
          continue;
        }

      /* If an empty description was added via lower-level APIs,
       * silently do not serialize it */
      if (g_strcmp0 (p->name, TRP_ROUTING_PARAMETER_DESCRIPTION) == 0 &&
          *value == '\0')
        continue;

      g_string_append_c (string, ';');
      g_string_append (string, p->name);

      /* parameters in the URI cannot validly have an empty value, but they
       * can appear without a value */
      if (*value != '\0')
        {
          g_string_append_c (string, '=');
          _trp_uri_append_parameter_value (string, value);
        }
    }

  return g_string_free (g_steal_pointer (&string), FALSE);
}

/**
 * trp_routing_request_new_for_uri:
 * @uri: a URI
 *
 * Return a routing request object with the destination and other parameters
 * given by the URI.
 *
 * Returns: (transfer full): the routing request object, or %NULL on error
 *
 * Since: 0.1612.0
 */
TrpRoutingRequest *
trp_routing_request_new_for_uri (const gchar *uri,
                                 TrpUriFlags flags,
                                 GError **error)
{
  g_autoptr (TrpRoutingRequestBuilder) builder =
      trp_routing_request_builder_new (NULL, NULL);

  g_return_val_if_fail (uri != NULL, NULL);
  g_return_val_if_fail ((flags & ~(TRP_URI_FLAGS_NONE | TRP_URI_FLAGS_STRICT)) == 0, NULL);
  g_return_val_if_fail (error == NULL || *error == NULL, NULL);

  if (!_trp_routing_request_builder_parse_nav_uri (builder, uri, flags, error))
    return NULL;

  return trp_routing_request_builder_end (builder);
}

static void
trp_routing_request_init (TrpRoutingRequest *self)
{
  self->destination = NULL;
  self->parameters =
      g_ptr_array_new_with_free_func ((GDestroyNotify) trp_routing_parameter_unref);
}

static void
trp_routing_request_get_property (GObject *object,
                                  guint prop_id,
                                  GValue *value,
                                  GParamSpec *pspec)
{
  TrpRoutingRequest *self = TRP_ROUTING_REQUEST (object);

  switch ((Property) prop_id)
    {
    case PROP_DESTINATION:
      g_value_set_object (value, self->destination);
      break;

    case PROP_PARAMETERS:
      g_value_set_boxed (value, trp_routing_request_get_parameters (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_routing_request_set_property (GObject *object,
                                  guint prop_id,
                                  const GValue *value,
                                  GParamSpec *pspec)
{
  TrpRoutingRequest *self = TRP_ROUTING_REQUEST (object);

  switch ((Property) prop_id)
    {
    case PROP_DESTINATION:
      /* construct-only */
      g_assert (self->destination == NULL);
      self->destination = g_value_dup_object (value);
      break;

    case PROP_PARAMETERS:
      {
        const GPtrArray *parameters = g_value_get_boxed (value);
        guint i;

        /* construct-only */
        g_assert (self->parameters->len == 0);

        if (parameters == NULL)
          break;

        for (i = 0; i < parameters->len; i++)
          {
            TrpRoutingParameter *p = g_ptr_array_index (parameters, i);

            g_ptr_array_add (self->parameters,
                             trp_routing_parameter_ref (p));
          }
      }
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
    }
}

static void
trp_routing_request_constructed (GObject *object)
{
  TrpRoutingRequest *self = TRP_ROUTING_REQUEST (object);

  G_OBJECT_CLASS (trp_routing_request_parent_class)
      ->constructed (object);

  g_return_if_fail (TRP_IS_PLACE (self->destination));
}

static void
trp_routing_request_dispose (GObject *object)
{
  TrpRoutingRequest *self = TRP_ROUTING_REQUEST (object);

  g_ptr_array_set_size (self->parameters, 0);
  g_clear_object (&self->destination);

  G_OBJECT_CLASS (trp_routing_request_parent_class)
      ->dispose (object);
}

static void
trp_routing_request_finalize (GObject *object)
{
  TrpRoutingRequest *self = TRP_ROUTING_REQUEST (object);

  g_clear_pointer (&self->parameters, g_ptr_array_unref);

  G_OBJECT_CLASS (trp_routing_request_parent_class)
      ->finalize (object);
}

static void
trp_routing_request_class_init (TrpRoutingRequestClass *cls)
{
  GObjectClass *object_class = G_OBJECT_CLASS (cls);

  /**
   * TrpRoutingRequest:destination:
   *
   * The final destination of this route.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_DESTINATION] =
      g_param_spec_object ("destination", "Destination",
                           "The final destination of this route",
                           TRP_TYPE_PLACE,
                           (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                            G_PARAM_STATIC_STRINGS));

  /**
   * TrpRoutingRequest:parameters: (type GPtrArray(TrpRoutingParameters))
   *
   * Points along this route, together with parameters affecting this
   * route.
   *
   * The points seen here include both waypoints (where the route
   * should stop temporarily or permanently) and via-points (where the
   * route should pass close to the requested point without stopping),
   * but unlike trp_routing_request_get_points(), the final destination
   * should not be included.
   *
   * Since: 0.1612.0
   */
  property_specs[PROP_PARAMETERS] =
      g_param_spec_boxed ("parameters", "Parameters",
                          "Waypoints, via-points and other parameters",
                          G_TYPE_PTR_ARRAY,
                          (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY |
                           G_PARAM_STATIC_STRINGS));

  object_class->get_property = trp_routing_request_get_property;
  object_class->set_property = trp_routing_request_set_property;
  object_class->constructed = trp_routing_request_constructed;
  object_class->dispose = trp_routing_request_dispose;
  object_class->finalize = trp_routing_request_finalize;

  g_object_class_install_properties (object_class,
                                     G_N_ELEMENTS (property_specs),
                                     property_specs);
}
