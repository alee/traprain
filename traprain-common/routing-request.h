/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __TRAPRAIN_COMMON_ROUTING_REQUEST_H__
#define __TRAPRAIN_COMMON_ROUTING_REQUEST_H__

#include <glib-object.h>

#include <traprain-common/place.h>

G_BEGIN_DECLS

/**
 * TRP_ROUTING_PARAMETER_DESCRIPTION:
 *
 * Routing parameter used for a human-readable description of the route,
 * intended to be displayed in the UI rather than machine-parsed.
 * This can be used in trp_routing_parameter_get_name()
 * or trp_routing_parameter_new()
 *
 * Since: 0.1612.0
 */
#define TRP_ROUTING_PARAMETER_DESCRIPTION "description"

/**
 * TRP_ROUTING_PARAMETER_VIA:
 *
 * Routing parameter used for defining a via-point.
 * via-points are non-named intermediate routing points, as a place URI (with
 * the `place:` scheme prefix) or as a geo URI (with the `geo:` scheme prefix).
 * These parameters can appear zero or more times and are order-dependent.
 * Way and via places can be interleaved.
 * This can be used in trp_routing_parameter_get_name()
 * or trp_routing_parameter_new()
 *
 * Since: 0.1612.0
 */
#define TRP_ROUTING_PARAMETER_VIA "via"

/**
 * TRP_ROUTING_PARAMETER_WAY:
 *
 * Routing parameter used for defining a waypoint
 * Waypoints are named intermediate destination, as a place URI (with the
 * `place:` scheme prefix) or as a geo URI (with the `geo:` scheme prefix).
 * These parameters can appear zero or more times and are order-dependent.
 * Way and via places can be interleaved.
 * This can be used in trp_routing_parameter_get_name()
 * or trp_routing_parameter_new()
 *
 * Since: 0.1612.0
 */
#define TRP_ROUTING_PARAMETER_WAYPOINT "way"

typedef struct _TrpRoutingParameter TrpRoutingParameter;
#define TRP_TYPE_ROUTING_PARAMETER (trp_routing_parameter_get_type ())
GType trp_routing_parameter_get_type (void);
gboolean trp_routing_parameter_is_name (const gchar *name);
TrpRoutingParameter *trp_routing_parameter_new (const gchar *name,
                                                const gchar *value);
TrpRoutingParameter *trp_routing_parameter_new_for_place (const gchar *name,
                                                          TrpPlace *place);
TrpRoutingParameter *trp_routing_parameter_ref (TrpRoutingParameter *self);
void trp_routing_parameter_unref (TrpRoutingParameter *self);
const gchar *trp_routing_parameter_get_name (TrpRoutingParameter *self);
const gchar *trp_routing_parameter_get_value (TrpRoutingParameter *self);
TrpPlace *trp_routing_parameter_get_place (TrpRoutingParameter *self);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (TrpRoutingParameter, trp_routing_parameter_unref)

#define TRP_TYPE_ROUTING_REQUEST (trp_routing_request_get_type ())
G_DECLARE_FINAL_TYPE (TrpRoutingRequest, trp_routing_request, TRP, ROUTING_REQUEST, GObject)

TrpRoutingRequest *trp_routing_request_new (TrpPlace *destination,
                                            const gchar *description);
TrpRoutingRequest *trp_routing_request_new_for_uri (const gchar *uri,
                                                    TrpUriFlags flags,
                                                    GError **error);

gchar *trp_routing_request_to_uri (TrpRoutingRequest *self,
                                   TrpUriScheme scheme);
const gchar *trp_routing_request_get_description (TrpRoutingRequest *self);
TrpPlace *trp_routing_request_get_destination (TrpRoutingRequest *self);
GPtrArray *trp_routing_request_get_parameters (TrpRoutingRequest *self);
GPtrArray *trp_routing_request_get_points (TrpRoutingRequest *self);
const gchar *trp_routing_request_get_parameter_value (TrpRoutingRequest *self,
                                                      const gchar *name);
gchar **trp_routing_request_get_parameter_values (TrpRoutingRequest *self,
                                                  const gchar *name);

const gchar *trp_routing_request_get_preference (TrpRoutingRequest *self,
                                                 const gchar *name,
                                                 const gchar *default_pref);
gboolean trp_routing_request_get_preference_boolean (TrpRoutingRequest *self,
                                                     const gchar *name,
                                                     gboolean default_pref);
typedef struct _TrpRoutingRequestBuilder TrpRoutingRequestBuilder;

#define TRP_TYPE_ROUTING_REQUEST_BUILDER \
  (trp_routing_request_builder_get_type ())
GType trp_routing_request_builder_get_type (void);

TrpRoutingRequestBuilder *trp_routing_request_builder_new (TrpPlace *destination,
                                                           const gchar *description);
TrpRoutingRequestBuilder *trp_routing_request_builder_ref (TrpRoutingRequestBuilder *self);
void trp_routing_request_builder_unref (TrpRoutingRequestBuilder *self);
G_DEFINE_AUTOPTR_CLEANUP_FUNC (TrpRoutingRequestBuilder, trp_routing_request_builder_unref)

void trp_routing_request_builder_clear (TrpRoutingRequestBuilder *self);
void trp_routing_request_builder_set_destination (TrpRoutingRequestBuilder *self,
                                                  TrpPlace *destination);
void trp_routing_request_builder_set_description (TrpRoutingRequestBuilder *self,
                                                  const gchar *description);
void trp_routing_request_builder_add_waypoint (TrpRoutingRequestBuilder *self,
                                               TrpPlace *waypoint);
void trp_routing_request_builder_add_via (TrpRoutingRequestBuilder *self,
                                          TrpPlace *via);
void trp_routing_request_builder_add_place (TrpRoutingRequestBuilder *self,
                                            const gchar *name,
                                            TrpPlace *place);
void trp_routing_request_builder_add_parameter (TrpRoutingRequestBuilder *self,
                                                const gchar *name,
                                                const gchar *value);
void trp_routing_request_builder_set_parameter (TrpRoutingRequestBuilder *self,
                                                const gchar *name,
                                                const gchar *value);
void trp_routing_request_builder_set_parameter_boolean (TrpRoutingRequestBuilder *self,
                                                        const gchar *name,
                                                        gboolean pref);

TrpRoutingRequest *trp_routing_request_builder_copy (TrpRoutingRequestBuilder *self);
TrpRoutingRequest *trp_routing_request_builder_end (TrpRoutingRequestBuilder *self);

G_END_DECLS

#endif
